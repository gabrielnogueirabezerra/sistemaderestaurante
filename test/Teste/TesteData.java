package Teste;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class TesteData {

    public static void main(String[] args) {
        Date d = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(d);

        DateFormat df = DateFormat.getTimeInstance();
        System.out.println(df.format(c.getTime()));
    }
}
