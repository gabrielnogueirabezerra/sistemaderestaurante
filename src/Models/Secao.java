package Models;

import DAO.SecaoDAO;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Gabriel Nogueira Bezerra
 * @modificado Beatriz Oliveira
 */
public class Secao implements InterfaceManter {

    private int id;
    private String descricao;
    
    public Secao(){
        super();
    }

    public Secao(int id) {
        this();
        if (id > 0) {
            this.id = id;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public void inserir() throws ClassNotFoundException, SQLException {
        if (this.descricao != null) {
            SecaoDAO.getInstancia().inserir(this);
        }
    }

    @Override
    public void alterar() throws ClassNotFoundException, SQLException {
        SecaoDAO.getInstancia().alterar(this);
    }

    @Override
    public void buscar(int codigo) throws ClassNotFoundException, SQLException {
        if (codigo > 0) {
            this.id = codigo;
            SecaoDAO.getInstancia().buscar(this);
        }
    }

    @Override
    public void excluir() throws ClassNotFoundException, SQLException {
        SecaoDAO.getInstancia().excluir(this);
    }

}
