package Models;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class HttpClientProject {

    private final String USER_AGENT = "Mozilla/5.0";

    public void sendPost(String urlPost, String jsonPost) throws Exception {
        String urlParameters
                = URLEncoder.encode(urlPost, "UTF-8");

        URL url;
        HttpURLConnection connection = null;

        //Create connection
        url = new URL(urlPost);
        connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

        connection.setRequestProperty("Content-Length", ""
                + Integer.toString(urlParameters.getBytes().length));
        connection.setRequestProperty("Content-Language", "en-US");

        connection.setUseCaches(false);
        connection.setDoInput(true);
        connection.setDoOutput(true);

        byte array[] = jsonPost.getBytes("UTF-8");
        
        //Send request
        DataOutputStream wr = new DataOutputStream(
                connection.getOutputStream());
        wr.write(array);
        wr.flush();
        wr.close();

        //Get Response	
        InputStream is = connection.getInputStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        String line;
        StringBuffer response = new StringBuffer();
        while ((line = rd.readLine()) != null) {
            response.append(line);
            response.append('\r');
        }
        rd.close();

    }

}
