package Models;

import DAO.ProdutoDAO;
import java.sql.SQLException;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class Produto implements InterfaceManter {

    private int id;
    private Secao secao;
    private String nome;
    private double valor;
    private String descricao;
    private String foto;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Secao getSecao() {
        return secao;
    }

    public void setSecao(Secao secao) {
        this.secao = secao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
    
    @Override
    public void inserir() throws ClassNotFoundException, SQLException {
        if (this.nome != null && this.valor > 0) {
           ProdutoDAO.getInstancia().inserir(this);
        }
    }

    @Override
    public void alterar() throws ClassNotFoundException, SQLException {
        ProdutoDAO.getInstancia().alterar(this);
    }

    @Override
    public void buscar(int codigo) throws ClassNotFoundException, SQLException {
        if (codigo > 0) {
            this.id = codigo;
            ProdutoDAO.getInstancia().buscar(this);
        }
    }

    @Override
    public void excluir() throws ClassNotFoundException, SQLException {
        ProdutoDAO.getInstancia().excluir(this);
    }
}

  
