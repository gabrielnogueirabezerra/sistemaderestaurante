package Models;

import DAO.ItemMesaDAO;
import java.sql.SQLException;
import java.util.Calendar;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class ItemMesa implements InterfaceManter {

    private int id;
    private int ordem;
    private Produto produto;
    private Garcom garcom;
    private double quantidade;
    private double valor;
    private Calendar horaLancamento;
    private int cancelado;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrdem() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem = ordem;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Garcom getGarcom() {
        return garcom;
    }

    public void setGarcom(Garcom garcom) {
        this.garcom = garcom;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Calendar getHoraLancamento() {
        return horaLancamento;
    }

    public void setHoraLancamento(Calendar horaLancamento) {
        this.horaLancamento = horaLancamento;
    }

    public int getCancelado() {
        return cancelado;
    }

    public void setCancelado(int cancelado) {
        this.cancelado = cancelado;
    }

    @Override
    public void inserir() throws ClassNotFoundException, SQLException {
        if (this.quantidade > 0 && this.valor > 0) {
            ItemMesaDAO.getInstancia().inserir(this);
        }
    }

    @Override
    public void alterar() throws ClassNotFoundException, SQLException {
        ItemMesaDAO.getInstancia().alterar(this);
    }

    @Override
    public void buscar(int codigo) throws ClassNotFoundException, SQLException {
        if (codigo > 0) {
            this.id = codigo;
            ItemMesaDAO.getInstancia().buscar(this);
        }
    }

    @Override
    public void excluir() throws ClassNotFoundException, SQLException {
        ItemMesaDAO.getInstancia().excluir(this);
    }

    public double total() {
        return this.quantidade * this.valor;
    }

}
