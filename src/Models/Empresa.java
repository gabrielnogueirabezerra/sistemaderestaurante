package Models;

import DAO.EmpresaDAO;
import java.sql.SQLException;

/**
 *
 * @author Gabriel Nogueira Bezerra
 * @modificado Beatriz Oliveira
 */
public class Empresa implements InterfaceManter {

    private int id;
    private String razaoSocial;
    private String nomeFantasia;
    private String CNPJ;
    private String endereco;
    private String numero;
    private String email;
    private String telefone;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getCNPJ() {
        return CNPJ;
    }

    public void setCNPJ(String cnpj) {
        this.CNPJ = cnpj;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
    @Override
    public void inserir() throws ClassNotFoundException, SQLException {
        if (this.razaoSocial != null && this.CNPJ != null && this.nomeFantasia != null) {
           EmpresaDAO.getInstancia().inserir(this);
        }
    }

    @Override
    public void alterar() throws ClassNotFoundException, SQLException {
        EmpresaDAO.getInstancia().alterar(this);
    }

    @Override
    public void buscar(int codigo) throws ClassNotFoundException, SQLException {
        if (codigo > 0) {
            this.id = codigo;
            EmpresaDAO.getInstancia().buscar(this);
        }
    }

    @Override
    public void excluir() throws ClassNotFoundException, SQLException {
        EmpresaDAO.getInstancia().excluir(this);
    }
}
