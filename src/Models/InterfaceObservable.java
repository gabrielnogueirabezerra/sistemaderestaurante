/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author gabri
 */
public interface InterfaceObservable {
    
    public void incluir(InterfaceObserver observer);
    
    public void excluir(InterfaceObserver observer);
    
    public void avisarObservers();
    
}
