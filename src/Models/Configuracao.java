package Models;

import DAO.EmpresaDAO;
import DAO.FormaPagamentoDAO;
import DAO.GarcomDAO;
import DAO.ItemMesaDAO;
import DAO.MesaDAO;
import DAO.ProdutoDAO;
import DAO.SecaoDAO;
import DAO.UsuarioDAO;
import DAO.VendaDAO;
import DAO.VendaFormaPagamentoDAO;
import Views.FrmPrincipal;
import com.google.gson.Gson;
import com.itextpdf.text.*;
import java.awt.Dimension;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class Configuracao implements InterfaceObservable {

    private ArrayList<InterfaceObserver> observers = new ArrayList<>();
    private JDesktopPane desktop;

    private ArrayList<Garcom> garcons = new ArrayList<>();
    private Garcom garcomSelecionado;

    private ArrayList<Usuario> usuarios = new ArrayList<>();
    private Usuario usuarioSelecionado;

    private Usuario usuario;

    private ArrayList<Secao> secoes = new ArrayList<>();
    private Secao secaoSelecionada;

    private ArrayList<Produto> produtos = new ArrayList<>();
    private Produto produtoSelecionado;

    private ArrayList<Mesa> mesas = new ArrayList<>();
    private Mesa mesaSelecionada;
    private int numeroMesa;

    private ArrayList<FormaPagamento> formas = new ArrayList<>();
    private FormaPagamento formaPagamentoSelecionada;

    private ArrayList<Empresa> empresas = new ArrayList<>();
    private Empresa EmpresaSelecionada;

    private Venda vendaCriada;

    private String caminhoFoto = "";

    @Override
    public void incluir(InterfaceObserver observer) {
        if (observer != null) {
            this.observers.add(observer);
        }
    }

    @Override
    public void excluir(InterfaceObserver observer) {
        if (observer != null) {
            this.observers.remove(observer);
        }
    }

    @Override
    public void avisarObservers() {
        for (InterfaceObserver o : this.observers) {
            if (o != null) {
                o.alterar();
            }
        }
    }

    public void setDesktop(JDesktopPane desktop) {
        this.desktop = desktop;
    }

    public void abreTelaPrincipal() {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new FrmPrincipal(new Configuracao()).setVisible(true);
            }
        });
    }

    public void abreTela(String nomeTela) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, IllegalArgumentException, InvocationTargetException {

        JInternalFrame tela = (JInternalFrame) Class.forName("Views." + nomeTela).
                getConstructor(Configuracao.class).newInstance(this);

        tela.setVisible(true);
        desktop.add(tela);
        Dimension dimensao = desktop.getSize();
        tela.setLocation((dimensao.width - tela.getSize().width) / 2,
                (dimensao.height - tela.getSize().height) / 2);
        tela.moveToFront();

    }

    public void abreMesa(int numero) throws ClassNotFoundException, SQLException {
        Mesa mesa = new Mesa();
        mesa.setNumero(numero);
        mesa.setDataHoraAbertura(Calendar.getInstance());
        mesa.inserir();
    }

    public void logar(String usuario, String senha) throws SQLException, ClassNotFoundException, Exception {
        this.usuario = new Usuario();
        this.usuario.setLogin(usuario);
        this.usuario.setSenha(Criptografia.encrypt(senha));
        this.usuario.logar();

        if (this.usuario.getId() == 0) {
            this.usuario = null;
        }
    }

    public Garcom inserirGarcom(String nome) throws ClassNotFoundException, SQLException {
        Garcom garcom = new Garcom();
        garcom.setNome(nome);
        garcom.inserir();

        return garcom;
    }

    public void excluirGarcom(int id) throws ClassNotFoundException, SQLException {
        Garcom garcom = new Garcom();
        garcom.buscar(id);

        garcom.excluir();
    }

    public void alterarGarcom(int id, String nome) throws ClassNotFoundException, SQLException {
        Garcom garcom = new Garcom();
        garcom.buscar(id);

        garcom.setNome(nome);
        garcom.alterar();
    }

    public void buscarTodosGarcons() throws SQLException, ClassNotFoundException {
        this.garcons = GarcomDAO.getInstancia().buscarTodos();
    }

    public Garcom buscarGarcom(int id) throws ClassNotFoundException, SQLException {
        this.garcons = new ArrayList<>();
        Garcom garcom = new Garcom();
        garcom.buscar(id);

        if (garcom.getNome() != null) {
            garcons.add(garcom);

        }

        return garcom;
    }

    public void buscarGarcom(String nome) throws ClassNotFoundException, SQLException {
        this.garcons = GarcomDAO.getInstancia().buscaTodosPorNome(nome);
    }

    public Usuario inserirUsuario(String nome, String senha) throws ClassNotFoundException, SQLException, Exception {
        Usuario u = new Usuario();
        u.setLogin(nome);
        u.setSenha(Criptografia.encrypt(senha));
        u.inserir();

        return u;
    }

    public void excluirUsuario(int id) throws ClassNotFoundException, SQLException {
        Usuario u = new Usuario();
        u.buscar(id);

        u.excluir();
    }

    public void alterarUsuario(int id, String nome, String senha) throws ClassNotFoundException, SQLException, Exception {
        Usuario u = new Usuario();
        u.buscar(id);

        u.setLogin(nome);
        u.setSenha(Criptografia.encrypt(senha));
        u.alterar();
    }

    public void buscarTodosUsuarios() throws SQLException, ClassNotFoundException, Exception {
        this.usuarios = UsuarioDAO.getInstancia().buscarTodos();
    }

    public Usuario buscarUsuario(int id) throws ClassNotFoundException, SQLException {
        this.usuarios = new ArrayList<>();
        Usuario u = new Usuario();
        u.buscar(id);

        if (u.getLogin() != null) {
            this.usuarios.add(u);

        }

        return u;
    }

    public void buscarUsuario(String nome) throws ClassNotFoundException, SQLException {
        this.garcons = GarcomDAO.getInstancia().buscaTodosPorNome(nome);
    }

    public Secao inserirSecao(String descricao) throws ClassNotFoundException, SQLException {
        Secao secao = new Secao();
        secao.setDescricao(descricao);
        secao.inserir();

        return secao;
    }

    public void excluirSecao(int id) throws ClassNotFoundException, SQLException {
        Secao secao = new Secao();
        secao.buscar(id);

        secao.excluir();
    }

    public void alterarSecao(int id, String descricao) throws ClassNotFoundException, SQLException {
        Secao secao = new Secao();
        secao.buscar(id);

        secao.setDescricao(descricao);
        secao.alterar();
    }

    public void buscarTodosSecao() throws SQLException, ClassNotFoundException {
        this.secoes = SecaoDAO.getInstancia().buscarTodos();
    }

    public Secao buscarSecao(int id) throws ClassNotFoundException, SQLException {
        this.secoes = new ArrayList<>();
        Secao secao = new Secao();
        secao.buscar(id);

        if (secao.getDescricao() != null) {
            secoes.add(secao);
        }
        return secao;
    }

    public void buscarSecao(String descricao) throws ClassNotFoundException, SQLException {
        this.secoes = SecaoDAO.getInstancia().buscaTodosPorDescricao(descricao);
    }

    public Empresa inserirEmpresa(String razaoSocial, String nomeFantasia, String CNPJ,
            String endereco, String numero, String email, String telefone) throws ClassNotFoundException, SQLException {
        Empresa empresa = new Empresa();
        empresa.setRazaoSocial(razaoSocial);
        empresa.setNomeFantasia(nomeFantasia);
        empresa.setCNPJ(CNPJ);
        empresa.setEndereco(endereco);
        empresa.setNumero(numero);
        empresa.setEmail(email);
        empresa.setTelefone(telefone);
        empresa.inserir();

        return empresa;
    }

    public void excluirEmpresa(int id) throws ClassNotFoundException, SQLException {
        Empresa empresa = new Empresa();
        empresa.buscar(id);

        empresa.excluir();
    }

    public void alterarEmpresa(int id, String razaoSocial, String nomeFantasia, String CNPJ,
            String endereco, String numero, String email, String telefone) throws ClassNotFoundException, SQLException {

        Empresa empresa = new Empresa();
        empresa.buscar(id);

        empresa.setRazaoSocial(razaoSocial);
        empresa.setNomeFantasia(nomeFantasia);
        empresa.setCNPJ(CNPJ);
        empresa.setEndereco(endereco);
        empresa.setNumero(numero);
        empresa.setEmail(email);
        empresa.setTelefone(telefone);
        empresa.alterar();
    }

    public void buscarTodosEmpresa() throws SQLException, ClassNotFoundException {
        this.empresas = EmpresaDAO.getInstancia().buscarTodos();
    }

    public Empresa buscarEmpresa(int id) throws ClassNotFoundException, SQLException {
        this.empresas = new ArrayList<>();
        Empresa empresa = new Empresa();
        empresa.buscar(id);

        if (empresa.getNomeFantasia() != null) {
            empresas.add(empresa);
        }
        return empresa;
    }

    public void buscarEmpresa(String nomeFantasia) throws ClassNotFoundException, SQLException {
        this.empresas = EmpresaDAO.getInstancia().buscaTodosPorNomeFantasia(nomeFantasia);
    }

    private void calculosVenda() throws ClassNotFoundException, SQLException {
        this.vendaCriada.getVendaFormasPagamento().forEach((VendaFormaPagamento vendaFormaPagamento) -> {
            vendaFormaPagamento.setVenda(null);
        });

        this.vendaCriada.setMesa(null);

        Empresa empresa = new Empresa();
        empresa.buscar(1);
        this.vendaCriada.setEmpresa(empresa);

        this.vendaCriada.calcTotal();
    }

    public void imprimirVenda() throws Exception {

        this.calculosVenda();
        
        String server = "http://localhost:8080/datasnap/rest/TControllerImpressao/ImprimirComprovante";

        Gson gson = new Gson();

        HttpClientProject http = new HttpClientProject();
        http.sendPost(server, gson.toJson(this.vendaCriada));

        this.vendaCriada = null;

    }

    public Produto inserirProduto(String nome, double valor, String caminhoFoto, int CodigoSecao, String descricao) throws ClassNotFoundException, SQLException {
        Produto produto = new Produto();
        produto.setNome(nome);
        produto.setSecao(this.buscarSecao(CodigoSecao));
        produto.setValor(valor);
        produto.setFoto(caminhoFoto);
        produto.setDescricao(descricao);

        produto.inserir();

        this.produtos = new ArrayList<>();

        return produto;
    }

    public void excluirProduto(int id) throws ClassNotFoundException, SQLException {
        Produto produto = new Produto();
        produto.buscar(id);

        produto.excluir();
    }

    public void alterarProduto(int id, String nome, double valor, String caminhoFoto, int CodigoSecao, String descricao) throws ClassNotFoundException, SQLException {
        Produto produto = new Produto();

        produto.setId(id);

        produto.setNome(nome);
        produto.setSecao(this.buscarSecao(CodigoSecao));
        produto.setValor(valor);
        produto.setFoto(caminhoFoto);
        produto.setDescricao(descricao);

        produto.alterar();
    }

    public void buscarTodosProdutos() throws SQLException, ClassNotFoundException {
        this.produtos = ProdutoDAO.getInstancia().buscarTodos();
    }

    public Produto buscarProduto(int id) throws ClassNotFoundException, SQLException {
        this.produtos = new ArrayList<>();
        Produto produto = new Produto();
        produto.buscar(id);

        if (produto.getNome() != null) {
            produtos.add(produto);
        }
        return produto;
    }

    public void buscarProduto(String Nome) throws ClassNotFoundException, SQLException {
        this.produtos = ProdutoDAO.getInstancia().buscaTodosPorNome(Nome);
    }

    public void buscarProduto(Secao secao) throws ClassNotFoundException, SQLException {
        this.produtos = ProdutoDAO.getInstancia().buscaTodos(secao);
    }

    public boolean existeMesaAberta(int numeroMesa) throws ClassNotFoundException, SQLException {
        Mesa mesa = new Mesa();
        mesa.setNumero(numeroMesa);
        mesa.buscaPorNumero();

        return (mesa.getId() != 0);
    }

    public void buscarMesas(boolean abertas, boolean canceladas, boolean fechadas) throws SQLException, ClassNotFoundException {
        this.mesas = MesaDAO.getInstancia().buscar(abertas, canceladas, fechadas);
    }

    public void buscarMesas(boolean abertas, boolean canceladas, boolean fechadas, String dataInicial, String dataFinal) throws SQLException, ClassNotFoundException {
        this.mesas = MesaDAO.getInstancia().buscar(abertas, canceladas, fechadas, formatDataBD(dataInicial) + " 00:00:00", formatDataBD(dataFinal) + " 23:59:59");
    }

    private String formatDataBD(String data) {

        String[] retorno = data.split("/");

        return retorno[2] + "-" + retorno[1] + "-" + retorno[0];
    }

    public Mesa buscaMesa(int numero) throws SQLException, ClassNotFoundException {
        Mesa mesa = new Mesa();
        mesa.setNumero(numero);
        mesa.buscaPorNumero();

        return mesa;
    }

    public void excluirFormaPagamento(int id) throws ClassNotFoundException, SQLException {
        FormaPagamento formaPagamento = new FormaPagamento();
        formaPagamento.buscar(id);

        formaPagamento.excluir();
    }

    public void alterarFormaPagamento(int id, String descricao) throws ClassNotFoundException, SQLException {
        FormaPagamento formaPagamento = new FormaPagamento();
        formaPagamento.buscar(id);

        formaPagamento.setDescricao(descricao);
        formaPagamento.alterar();
    }

    public void buscarTodasFormasPagamento() throws SQLException, ClassNotFoundException {
        this.formas = FormaPagamentoDAO.getInstancia().buscarTodos();
    }

    public FormaPagamento inserirFormaPagamento(String descricao) throws ClassNotFoundException, SQLException {
        FormaPagamento formaPagamento = new FormaPagamento();
        formaPagamento.setDescricao(descricao);
        formaPagamento.inserir();

        return formaPagamento;
    }

    public FormaPagamento buscarFormaPagamento(int id) throws ClassNotFoundException, SQLException {
        this.formas = new ArrayList<>();
        FormaPagamento formaPagamento = new FormaPagamento();
        formaPagamento.buscar(id);

        if (formaPagamento.getDescricao() != null) {
            formas.add(formaPagamento);

        }

        return formaPagamento;
    }

    public void buscarFormaPagamento(String descricao) throws ClassNotFoundException, SQLException {
        this.formas = FormaPagamentoDAO.getInstancia().buscaTodosPorDescricao(descricao);
    }

    public void criaNovaVenda() throws Exception {
        Venda venda = new Venda();
        venda.setDataHora(Calendar.getInstance());
        venda.setMesa(mesaSelecionada);
        int i = 1;
        for (ItemMesa itemMesa : mesaSelecionada.getItensMesa()) {
            if (itemMesa != null) {
                if (itemMesa.getCancelado() == 0) {
                    ItemVenda itemVenda = new ItemVenda();
                    itemVenda.setOrdem(i);
                    itemVenda.setProduto(itemMesa.getProduto());
                    itemVenda.setQuantidade(itemMesa.getQuantidade());
                    itemVenda.setValor(itemMesa.getValor());
                    venda.getItensVenda().add(itemVenda);
                    i++;
                }
            }
        }

        this.vendaCriada = venda;
    }

    public Venda getVendaCriada() {
        return vendaCriada;
    }

    public void setVendaCriada(Venda vendaCriada) {
        this.vendaCriada = vendaCriada;
    }

    public ArrayList<FormaPagamento> getFormas() {
        return formas;
    }

    public void setFormas(ArrayList<FormaPagamento> formas) {
        this.formas = formas;
    }

    public FormaPagamento getFormaPagamentoSelecionada() {
        return formaPagamentoSelecionada;
    }

    public void setFormaPagamentoSelecionada(FormaPagamento formaPagamentoSelecionada) {
        this.formaPagamentoSelecionada = formaPagamentoSelecionada;
    }

    public JDesktopPane getDesktop() {
        return this.desktop;
    }

    public ArrayList<Garcom> getGarcons() {
        return garcons;
    }

    public void setGarcons(ArrayList<Garcom> garcons) {
        this.garcons = garcons;
    }

    public ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(ArrayList<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public Garcom getGarcomSelecionado() {
        return garcomSelecionado;
    }

    public void setGarcomSelecionado(Garcom garcomSelecionado) {
        this.garcomSelecionado = garcomSelecionado;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public Usuario getUsuarioSelecionado() {
        return usuarioSelecionado;
    }

    public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
        this.usuarioSelecionado = usuarioSelecionado;
    }

    public ArrayList<Secao> getSecoes() {
        return secoes;
    }

    public void setSecoes(ArrayList<Secao> secoes) {
        this.secoes = secoes;
    }

    public Secao getSecaoSelecionada() {
        return secaoSelecionada;
    }

    public void setSecaoSelecionada(Secao secaoSelecionada) {
        this.secaoSelecionada = secaoSelecionada;
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }

    public Produto getProdutoSelecionado() {
        return produtoSelecionado;
    }

    public void setProdutoSelecionado(Produto produtoSelecionado) {
        this.produtoSelecionado = produtoSelecionado;
    }

    public ArrayList<Empresa> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(ArrayList<Empresa> empresas) {
        this.empresas = empresas;
    }

    public Empresa getEmpresaSelecionada() {
        return EmpresaSelecionada;
    }

    public void setEmpresaSelecionada(Empresa EmpresaSelecionada) {
        this.EmpresaSelecionada = EmpresaSelecionada;
    }

    public ArrayList<Mesa> getMesas() {
        return mesas;
    }

    public void setMesas(ArrayList<Mesa> mesas) {
        this.mesas = mesas;
    }

    public Mesa getMesaSelecionada() {
        return mesaSelecionada;
    }

    public void setMesaSelecionada(Mesa mesaSelecionada) {
        this.mesaSelecionada = mesaSelecionada;
    }

    public int getNumeroMesa() {
        return numeroMesa;
    }

    public void setNumeroMesa(int numeroMesa) {
        this.numeroMesa = numeroMesa;
    }

    public void inserirItemMesa(int idProduto, int idGarcom, double quantidade, double valor) throws ClassNotFoundException, SQLException {
        if (mesaSelecionada != null) {
            ItemMesa itemMesa = new ItemMesa();
            itemMesa.setGarcom(buscarGarcom(idGarcom));
            itemMesa.setProduto(buscarProduto(idProduto));
            itemMesa.setHoraLancamento(Calendar.getInstance());
            itemMesa.setId(mesaSelecionada.getId());
            itemMesa.setOrdem(mesaSelecionada.getItensMesa().size() + 1);
            itemMesa.setQuantidade(quantidade);
            itemMesa.setValor(valor);
            itemMesa.setCancelado(0);

            ItemMesaDAO.getInstancia().inserir(itemMesa);
            mesaSelecionada.getItensMesa().add(itemMesa);
        }
    }

    public void cancelarMesa(int codigo) throws ClassNotFoundException, SQLException {
        Mesa mesa = new Mesa();
        mesa.buscar(codigo);

        mesa.setStatus(2);
        mesa.setDataHoraCancelamento(Calendar.getInstance());

        mesa.alterar();
    }

    public void cancelaItemMesa(int codigo, int ordem) throws ClassNotFoundException, SQLException {
        ItemMesa itemMesa = new ItemMesa();
        itemMesa.setOrdem(ordem);
        itemMesa.buscar(codigo);

        itemMesa.setCancelado(1);
        itemMesa.alterar();
    }

    public void adicionaFormaPagamentoVenda(int codigoFormaPagamento, double valor) throws ClassNotFoundException, SQLException {

        FormaPagamento formaPagamento = new FormaPagamento();
        formaPagamento.buscar(codigoFormaPagamento);
        VendaFormaPagamento vendaFormaPagamento = new VendaFormaPagamento();
        vendaFormaPagamento.setFormaPagamento(formaPagamento);
        vendaFormaPagamento.setValor(valor);
        vendaFormaPagamento.setVenda(vendaCriada);

        vendaCriada.getVendaFormasPagamento().add(vendaFormaPagamento);

    }

    public void excluirFormaPagamentoVenda(int posicao) throws Exception {
        vendaCriada.getVendaFormasPagamento().remove(posicao);
    }

    public void gravaVenda(double acrescimo, double desconto, String nomeCliente) throws ClassNotFoundException, SQLException {

        vendaCriada.setAcrescimo(acrescimo);
        vendaCriada.setDesconto(desconto);
        vendaCriada.setNomeCliente(nomeCliente);
        VendaDAO.getInstancia().inserir(vendaCriada);

        for (ItemVenda itemVenda : vendaCriada.getItensVenda()) {
            itemVenda.setId(vendaCriada.getId());
            itemVenda.inserir();
        }

        for (VendaFormaPagamento vendaFormaPagamento : vendaCriada.getVendaFormasPagamento()) {
            VendaFormaPagamentoDAO.getInstancia().inserir(vendaFormaPagamento);
        }

    }

    public void concluiMesaSelecionada() throws ClassNotFoundException, SQLException {
        if (this.getMesaSelecionada() != null) {
            this.mesaSelecionada.setStatus(1);
            this.mesaSelecionada.setDataHoraFechamento(Calendar.getInstance());
            this.mesaSelecionada.alterar();
        }
    }

    public String getCaminhoFoto() {
        return caminhoFoto;
    }

    public void setCaminhoFoto(String caminhoFoto) {
        this.caminhoFoto = caminhoFoto;
    }

    public ArrayList<InterfaceObserver> getObservers() {
        return observers;
    }

    public void buscaVenda(int codigo) throws ClassNotFoundException, SQLException {
        this.vendaCriada = new Venda();
        Mesa mesa = new Mesa();
        mesa.buscar(codigo);
        this.vendaCriada.setMesa(mesa);
        VendaDAO.getInstancia().buscarVendaMesa(this.vendaCriada);

    }

    public void geraRelatorioVendas(String dataInicial, String dataFinal) throws ClassNotFoundException, SQLException, FileNotFoundException, DocumentException, IOException {
        TemplateRelatorio relatorio = new RelatorioVenda(VendaDAO.getInstancia().buscaTodos(formatDataBD(dataInicial) + " 00:00:00", formatDataBD(dataFinal) + " 23:59:59"));
        relatorio.geraRelatorio();
    }

    public void setObservers(ArrayList<InterfaceObserver> observers) {
        this.observers = observers;
    }

}
