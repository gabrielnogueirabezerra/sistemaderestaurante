package Models;

import DAO.ItemVendaDAO;
import java.sql.SQLException;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class ItemVenda implements InterfaceManter{

    private int id;
    private int ordem;
    private Produto produto;
    private double quantidade;
    private double valor;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrdem() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem = ordem;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
    
    @Override
    public void inserir() throws ClassNotFoundException, SQLException {
        if (this.quantidade > 0 && this.valor > 0) {
           ItemVendaDAO.getInstancia().inserir(this);
        }
    }

    @Override
    public void alterar() throws ClassNotFoundException, SQLException {
        ItemVendaDAO.getInstancia().alterar(this);
    }

    @Override
    public void buscar(int codigo) throws ClassNotFoundException, SQLException {
        if (codigo > 0) {
            this.id = codigo;
            ItemVendaDAO.getInstancia().buscar(this);
        }
    }

    @Override
    public void excluir() throws ClassNotFoundException, SQLException {
        ItemVendaDAO.getInstancia().excluir(this);
    }
    
    public double total(){
        return this.quantidade * this.valor;
    }
}
