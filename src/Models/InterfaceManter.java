/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Beatriz Oliveiira
 */
public interface InterfaceManter {
    
    public void inserir() throws ClassNotFoundException, SQLException;

    public void alterar() throws ClassNotFoundException, SQLException;

    public void buscar(int codigo) throws ClassNotFoundException, SQLException;

    public void excluir() throws ClassNotFoundException, SQLException;
}
