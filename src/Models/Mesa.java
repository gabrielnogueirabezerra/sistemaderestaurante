package Models;

import DAO.MesaDAO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class Mesa {

    private int id;
    private int numero;
    private String descricao;
    private int totalPessoas;
    private Calendar dataHoraAbertura;
    private Calendar dataHoraFechamento;
    private Calendar dataHoraCancelamento;
    private int status;
    private ArrayList<ItemMesa> itensMesa = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getTotalPessoas() {
        return totalPessoas;
    }

    public void setTotalPessoas(int totalPessoas) {
        this.totalPessoas = totalPessoas;
    }

    public Calendar getDataHoraAbertura() {
        return dataHoraAbertura;
    }

    public void setDataHoraAbertura(Calendar dataHoraAbertura) {
        this.dataHoraAbertura = dataHoraAbertura;
    }

    public Calendar getDataHoraFechamento() {
        return dataHoraFechamento;
    }

    public void setDataHoraFechamento(Calendar dataHoraFechamento) {
        this.dataHoraFechamento = dataHoraFechamento;
    }

    public Calendar getDataHoraCancelamento() {
        return dataHoraCancelamento;
    }

    public void setDataHoraCancelamento(Calendar dataHoraCancelamento) {
        this.dataHoraCancelamento = dataHoraCancelamento;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<ItemMesa> getItensMesa() {
        return itensMesa;
    }

    public void setItensMesa(ArrayList<ItemMesa> itensMesa) {
        this.itensMesa = itensMesa;
    }

    public void buscaPorNumero() throws ClassNotFoundException, SQLException {
        if (this.numero > 0) {
            MesaDAO.getInstancia().buscaPorNumero(this);
        }
    }

    public void buscar(int id) throws ClassNotFoundException, SQLException {
        if (id > 0) {
            this.id = id;
            MesaDAO.getInstancia().buscar(this);
        }
    }

    public void alterar() throws ClassNotFoundException, SQLException {
        if (this.id > 0 && this.dataHoraAbertura != null && this.numero > 0) {
            MesaDAO.getInstancia().alterar(this);
        }
    }

    public void inserir() throws ClassNotFoundException, SQLException {
        if (this.numero > 0) {
            MesaDAO.getInstancia().inserir(this);
        }
    }

    public double total() {

        double total = 0;

        for (ItemMesa itemMesa : itensMesa) {
            if (itemMesa.getCancelado() == 0) {
                total = total + itemMesa.total();
            }
        }

        return total;
    }

}
