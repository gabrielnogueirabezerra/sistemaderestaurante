package Models;

import DAO.GarcomDAO;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @modificado Beatriz Oliveira
 * @author Gabriel Nogueira Bezerra
 */
public class Garcom implements InterfaceManter {

    private int id;
    private String nome;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Garcom() {
        super();
    }

    public Garcom(String nome) {
        this.setNome(nome);
    }

    public Garcom(int id, String nome) {
        this.setId(id);
        this.setNome(nome);
    }

    @Override
    public void inserir() throws ClassNotFoundException, SQLException {
        if (this.nome != null) {

            GarcomDAO.getInstancia().inserir(this);

        }
    }

    @Override
    public void alterar() throws ClassNotFoundException, SQLException {
        GarcomDAO.getInstancia().alterar(this);
    }

    @Override
    public void buscar(int codigo) throws ClassNotFoundException, SQLException {
        if (codigo > 0) {
            this.id = codigo;
            GarcomDAO.getInstancia().buscar(this);
        }
    }

    @Override
    public void excluir() throws ClassNotFoundException, SQLException {
        GarcomDAO.getInstancia().excluir(this);
    }

}
