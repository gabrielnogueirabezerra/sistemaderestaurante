package Models;

import DAO.UsuarioDAO;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class Usuario implements InterfaceManter {

    private int id;
    private String login;
    private String senha;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public void logar() throws SQLException, ClassNotFoundException {
        if (this.login != null && this.senha != null) {
            UsuarioDAO.getInstancia().logar(this);
        }
    }

    @Override
    public void buscar(int id) throws SQLException, ClassNotFoundException {
        if (id > 0) {
            this.id = id;
            try {
                UsuarioDAO.getInstancia().buscar(this);
            } catch (Exception ex) {
                Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void inserir() throws ClassNotFoundException, SQLException {
        if (this.login != null && this.senha != null) {
            UsuarioDAO.getInstancia().inserir(this);
        }
    }

    @Override
    public void alterar() throws ClassNotFoundException, SQLException {
        if (this.id > 0 && this.login != null && this.senha != null) {
            UsuarioDAO.getInstancia().alterar(this);
        }
    }

    @Override
    public void excluir() throws ClassNotFoundException, SQLException {
        if (this.id > 0) {
            UsuarioDAO.getInstancia().excluir(this);
        }
    }

}
