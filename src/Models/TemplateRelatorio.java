package Models;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public abstract class TemplateRelatorio {

    protected String localRelatorio;
    protected Document documento;

    public void geraRelatorio() throws FileNotFoundException, DocumentException, IOException {
        this.criaRelatorio();
        this.preparaRelatorio();
        this.encerraRelatorio();

        Desktop.getDesktop().open(new File(this.localRelatorio));
    }

    protected abstract void criaRelatorio() throws FileNotFoundException, DocumentException;

    protected abstract void preparaRelatorio() throws DocumentException;

    protected abstract void encerraRelatorio();

}
