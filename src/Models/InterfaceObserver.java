package Models;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public interface InterfaceObserver {
    
    public void alterar();
    
}
