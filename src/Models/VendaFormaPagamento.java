package Models;

import DAO.VendaFormaPagamentoDAO;
import java.sql.SQLException;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class VendaFormaPagamento implements InterfaceManter {

    private int id;
    private Venda venda;
    private FormaPagamento formaPagamento;
    private double valor;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Venda getVenda() {
        return venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

     @Override
    public void inserir() throws ClassNotFoundException, SQLException {
        if (this.valor > 0 && formaPagamento != null && venda != null) {

            VendaFormaPagamentoDAO.getInstancia().inserir(this);

        }
    }

    @Override
    public void alterar() throws ClassNotFoundException, SQLException {
        VendaFormaPagamentoDAO.getInstancia().alterar(this);
    }

    @Override
    public void buscar(int codigo) throws ClassNotFoundException, SQLException {
        if (codigo > 0) {
            this.id = codigo;
            VendaFormaPagamentoDAO.getInstancia().buscar(this);
        }
    }

    @Override
    public void excluir() throws ClassNotFoundException, SQLException {
        VendaFormaPagamentoDAO.getInstancia().excluir(this);
    }
    
}
