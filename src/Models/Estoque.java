package Models;

import DAO.EstoqueDAO;
import java.sql.SQLException;

/**
 *@modificado Beatriz Oliveira
 * @author Gabriel Nogueira Bezerra
 */
public class Estoque implements InterfaceManter {

    private int id;
    private Produto produto;
    private double quantidade;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }
    
    @Override
    public void inserir() throws ClassNotFoundException, SQLException {
        if (this.quantidade > 0) {
           EstoqueDAO.getInstancia().inserir(this);
        }
    }

    @Override
    public void alterar() throws ClassNotFoundException, SQLException {
        EstoqueDAO.getInstancia().alterar(this);
    }

    @Override
    public void buscar(int codigo) throws ClassNotFoundException, SQLException {
        if (codigo > 0) {
            this.id = codigo;
            EstoqueDAO.getInstancia().buscar(this);
        }
    }

    @Override
    public void excluir() throws ClassNotFoundException, SQLException {
        EstoqueDAO.getInstancia().excluir(this);
    }
}
