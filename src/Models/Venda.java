package Models;

import com.itextpdf.text.pdf.PdfPCell;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class Venda {

    private int id;
    private Mesa mesa;
    private Calendar dataHora;
    private String nomeCliente;
    private double desconto;
    private double acrescimo;
    private ArrayList<ItemVenda> itensVenda = new ArrayList<>();
    private ArrayList<VendaFormaPagamento> vendaFormasPagamento = new ArrayList<>();
    private Empresa empresa;
    private double total;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Mesa getMesa() {
        return mesa;
    }

    public void setMesa(Mesa mesa) {
        this.mesa = mesa;
    }

    public Calendar getDataHora() {
        return dataHora;
    }

    public void setDataHora(Calendar dataHora) {
        this.dataHora = dataHora;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public double getAcrescimo() {
        return acrescimo;
    }

    public void setAcrescimo(double acrescimo) {
        this.acrescimo = acrescimo;
    }

    public ArrayList<ItemVenda> getItensVenda() {
        return itensVenda;
    }

    public void setItensVenda(ArrayList<ItemVenda> itensVenda) {
        this.itensVenda = itensVenda;
    }

    public ArrayList<VendaFormaPagamento> getVendaFormasPagamento() {
        return vendaFormasPagamento;
    }

    public void setVendaFormasPagamento(ArrayList<VendaFormaPagamento> vendaFormasPagamento) {
        this.vendaFormasPagamento = vendaFormasPagamento;
    }

    public double total() {
        double total = 0;

        for (ItemVenda itemVenda : this.itensVenda) {
            total = total + itemVenda.total();
        }

        for (VendaFormaPagamento vendaFormaPagamento : this.vendaFormasPagamento) {
            total = total - vendaFormaPagamento.getValor();
        }

        total = total + this.acrescimo;

        total = total - this.desconto;

        return total;
    }

    public void calcTotal() {
        this.total = 0;

        for (ItemVenda itemVenda : this.itensVenda) {
            this.total = this.total + itemVenda.total();
        }

        this.total = this.total + this.acrescimo;
        this.total = this.total - this.desconto;

    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String formasPagamento() {
        String resultado = "";
        for (VendaFormaPagamento vendaF : this.vendaFormasPagamento) {
            resultado += vendaF.getFormaPagamento().getDescricao() + " | ";
        }

        resultado = resultado.substring(0, resultado.length() - 3);
        
        return resultado;
    }

}
