package Models;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class RelatorioVenda extends TemplateRelatorio {

    private ArrayList<Venda> vendas = new ArrayList<>();

    public RelatorioVenda(ArrayList<Venda> vendas) {
        if (vendas != null) {
            super.localRelatorio = "C:\\Users\\gabri\\OneDrive\\Document"
                    + "os\\Engenharia de Software\\6º Semestre - 2019.1\\Manutenção de Software\\S"
                    + "istema\\RelatorioVendas.pdf";
            this.vendas = vendas;
        }
    }

    /**
     *
     * @throws FileNotFoundException
     * @throws DocumentException
     */
    @Override
    protected void criaRelatorio() throws FileNotFoundException, DocumentException {
        super.documento = new Document();
        PdfWriter.getInstance(super.documento, new FileOutputStream(super.localRelatorio));
        super.documento.open();
    }

    /**
     *
     * @throws DocumentException
     */
    @Override
    protected void preparaRelatorio() throws DocumentException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY", new Locale("pt", "BR"));

        Paragraph titulo
                = new Paragraph(new Phrase(20F, "Relatório de vendas por período", FontFactory.getFont(FontFactory.HELVETICA, 18F)));

        titulo.setAlignment(Element.ALIGN_CENTER);

        super.documento.add(titulo);

        Font fontDeLink = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK);

        Paragraph enter = new Paragraph(new Phrase(14F, "", fontDeLink));

        super.documento.add(Chunk.NEWLINE);
        super.documento.add(Chunk.NEWLINE);

        Paragraph dataEmissao = new Paragraph(new Phrase(14F, "Emiss"
                + "ão: " + sdf.format(Calendar.getInstance().getTime()), fontDeLink));

        dataEmissao.setAlignment(Element.ALIGN_LEFT);

        super.documento.add(dataEmissao);

        super.documento.add(Chunk.NEWLINE);

        Font f = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE);

        PdfPTable table = new PdfPTable(4);

        Paragraph pColuna1 = new Paragraph("CÓDIGO", f);
        pColuna1.setAlignment(Element.ALIGN_CENTER);

        PdfPCell coluna1 = new PdfPCell(pColuna1);
        coluna1.setBackgroundColor(BaseColor.GRAY);

        Paragraph pColuna2 = new Paragraph("DATA/HORA", f);
        pColuna2.setAlignment(Element.ALIGN_CENTER);

        PdfPCell coluna2 = new PdfPCell(pColuna2);
        coluna2.setBackgroundColor(BaseColor.GRAY);

        Paragraph pColuna3 = new Paragraph("TOTAL", f);
        pColuna3.setAlignment(Element.ALIGN_CENTER);

        PdfPCell coluna3 = new PdfPCell(pColuna3);
        coluna3.setBackgroundColor(BaseColor.GRAY);

        Paragraph pColuna4 = new Paragraph("FORMA DE PAGAMENTO", f);
        pColuna4.setAlignment(Element.ALIGN_CENTER);

        PdfPCell coluna4 = new PdfPCell(pColuna4);
        coluna4.setBackgroundColor(BaseColor.GRAY);

        table.setWidths(new int[]{300, 300, 350, 300});
        table.addCell(coluna1);
        table.addCell(coluna2);
        table.addCell(coluna3);
        table.addCell(coluna4);

        sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        double totalVendas = 0;

        for (Venda venda : vendas) {

            venda.calcTotal();

            table.addCell(String.valueOf(venda.getId()));
            table.addCell(sdf.format(venda.getDataHora().getTime()));
            table.addCell(String.valueOf(venda.getTotal()));
            table.addCell(venda.formasPagamento());

            totalVendas += venda.getTotal();

        }

        super.documento.add(table);
        super.documento.add(Chunk.NEWLINE);

        Paragraph pTotalVendas = new Paragraph(new Phrase(20F, "Total: " + String.valueOf(totalVendas), fontDeLink));

        pTotalVendas.setAlignment(Element.ALIGN_RIGHT);

        super.documento.add(pTotalVendas);
    }

    @Override
    protected void encerraRelatorio() {
        super.documento.close();
    }

}
