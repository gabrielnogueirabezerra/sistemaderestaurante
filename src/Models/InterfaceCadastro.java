/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import javax.swing.JButton;

/**
 *
 * @author gabri
 */
public interface InterfaceCadastro {

    public void botoes(char opcao);

    public JButton getBtnNovo();

    public JButton getBtnGravar();

    public JButton getBtnAlterar();

    public JButton getBtnCancelar();

    public JButton getBtnConsultar();

    public JButton getBtnExcluir();

    public JButton getBtnSair();

}
