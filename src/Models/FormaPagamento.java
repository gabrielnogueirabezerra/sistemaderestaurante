package Models;

import DAO.FormaPagamentoDAO;
import java.sql.SQLException;

/**
 *
 * @author Gabriel Nogueira Bezerra
 * @modificado Beatriz Oliveira
 */
public class FormaPagamento implements InterfaceManter{

    private int id;
    private String descricao;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public void inserir() throws ClassNotFoundException, SQLException {
        if (this.descricao != null) {

            FormaPagamentoDAO.getInstancia().inserir(this);

        }
    }

    @Override
    public void alterar() throws ClassNotFoundException, SQLException {
        FormaPagamentoDAO.getInstancia().alterar(this);
    }

    @Override
    public void buscar(int codigo) throws ClassNotFoundException, SQLException {
        if (codigo > 0) {
            this.id = codigo;
            FormaPagamentoDAO.getInstancia().buscar(this);
        }
    }

    @Override
    public void excluir() throws ClassNotFoundException, SQLException {
        FormaPagamentoDAO.getInstancia().excluir(this);
    }

}
