/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Controllers.FrmCadastroEmpresaController;
import Models.Configuracao;
import Models.InterfaceCadastro;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Beatriz Oliveiira
 */
public class FrmCadastroEmpresa extends javax.swing.JInternalFrame implements InterfaceCadastro {
    
    private FrmCadastroEmpresaController controller;
    
    public FrmCadastroEmpresa() {
        initComponents();
    }
    
    public JTextField getCNPJ() {
        return this.EdtCNPJ;
    }
    
    public JTextField getId() {
        return this.EdtCodigo;
    }
    
    public JTextField getEmail() {
        return this.EdtEmail;
    }
    
    public JTextField getEndereco() {
        return this.EdtEndereco;
    }
    
    public JTextField getNomeFantasia() {
        return this.EdtNomeFantasia;
    }
    
    public JTextField getNumero() {
        return this.EdtNumero;
    }
    
    public JTextField getRazaoSocial() {
        return this.EdtRazaoSocial;
    }
    
    public JTextField getTelefone() {
        return this.EdtTelefone;
    }
    
    @Override
    public JButton getBtnNovo() {
        return this.btnNovo;
    }
    
    @Override
    public JButton getBtnGravar() {
        return this.btnGravar;
    }
    
    @Override
    public JButton getBtnAlterar() {
        return this.btnAlterar;
    }
    
    @Override
    public JButton getBtnCancelar() {
        return this.btnCancelar;
    }
    
    @Override
    public JButton getBtnConsultar() {
        return this.btnConsultar;
    }
    
    @Override
    public JButton getBtnExcluir() {
        return this.btnExcluir;
    }
    
    @Override
    public JButton getBtnSair() {
        return this.btnSair;
    }
    
    @Override
    public void botoes(char opcao) {
        switch (opcao) {
            case 'N':
                this.getBtnNovo().setEnabled(false);
                this.getBtnGravar().setEnabled(true);
                this.getBtnAlterar().setEnabled(false);
                this.getBtnCancelar().setEnabled(true);
                this.getBtnConsultar().setEnabled(false);
                this.getBtnExcluir().setEnabled(false);
                this.getBtnSair().setEnabled(true);
                
                this.EdtNomeFantasia.setEnabled(true);
                this.EdtCNPJ.setEnabled(true);
                this.EdtRazaoSocial.setEnabled(true);
                this.EdtEndereco.setEnabled(true);
                this.EdtNumero.setEnabled(true);
                this.EdtEmail.setEnabled(true);
                this.EdtTelefone.setEnabled(true);
                
                break;
            case 'G':
                this.getBtnNovo().setEnabled(true);
                this.getBtnGravar().setEnabled(false);
                this.getBtnAlterar().setEnabled(true);
                this.getBtnCancelar().setEnabled(false);
                this.getBtnConsultar().setEnabled(true);
                this.getBtnExcluir().setEnabled(true);
                this.getBtnSair().setEnabled(true);
                
                this.EdtNomeFantasia.setEnabled(false);
                this.EdtCNPJ.setEnabled(false);
                this.EdtRazaoSocial.setEnabled(false);
                this.EdtEndereco.setEnabled(false);
                this.EdtNumero.setEnabled(false);
                this.EdtEmail.setEnabled(false);
                this.EdtTelefone.setEnabled(false);
                break;
            case 'A':
                this.botoes('N');
                break;
            case 'C':
                this.getBtnNovo().setEnabled(true);
                this.getBtnGravar().setEnabled(false);
                this.getBtnAlterar().setEnabled(false);
                this.getBtnCancelar().setEnabled(false);
                this.getBtnConsultar().setEnabled(true);
                this.getBtnExcluir().setEnabled(false);
                this.getBtnSair().setEnabled(true);
                
                this.EdtNomeFantasia.setEnabled(false);
                this.EdtCNPJ.setEnabled(false);
                this.EdtRazaoSocial.setEnabled(false);
                this.EdtEndereco.setEnabled(false);
                this.EdtNumero.setEnabled(false);
                this.EdtEmail.setEnabled(false);
                this.EdtTelefone.setEnabled(false);
                break;
            case 'E':
                this.botoes('N');
                break;
            default:
                break;
        }
    }
    
    public FrmCadastroEmpresa(Configuracao model) {
        this();
        if (model != null) {
            this.controller = new FrmCadastroEmpresaController(this, model);
        }
    }
    
    public boolean validaCampos() {
        if (this.EdtRazaoSocial.getText().trim().equals("")) {
            this.EdtRazaoSocial.requestFocus();
            this.mostraMensagem("Informe a razão social da empresa.");
            return false;
        }
        
        if (this.EdtCNPJ.getText().trim().equals("")) {
            this.EdtCNPJ.requestFocus();
            this.mostraMensagem("Informe o CNPJ da empresa.");
            return false;
        }
        
        if (this.EdtNomeFantasia.getText().trim().equals("")) {
            this.EdtNomeFantasia.requestFocus();
            this.mostraMensagem("Informe o nome fantasia da empresa.");
            return false;
        }
        
        if (this.EdtEndereco.getText().trim().equals("")) {
            this.EdtEndereco.requestFocus();
            this.mostraMensagem("Informe o endereço da empresa.");
            return false;
        }
        
        if (this.EdtNumero.getText().trim().equals("")) {
            this.EdtNumero.requestFocus();
            this.mostraMensagem("Informe o numero do endereço da empresa.");
            return false;
        }
        
        if (this.EdtEmail.getText().trim().equals("")) {
            this.EdtEmail.requestFocus();
            this.mostraMensagem("Informe o email da empresa.");
            return false;
        }
        
        if (this.EdtTelefone.getText().trim().equals("")) {
            this.EdtTelefone.requestFocus();
            this.mostraMensagem("Informe o telefone da empresa.");
            return false;
        }
        
        return true;
    }
    
    public void mostraMensagem(String mensagem) {
        if (mensagem != null) {
            JOptionPane.showMessageDialog(this, mensagem);
        }
    }
    
    public boolean mensagemVerificacao(String mensagem) {
        if (mensagem != null) {
            int resposta = JOptionPane.showConfirmDialog(this, mensagem, "Atenção!", JOptionPane.YES_NO_OPTION);
            
            if (resposta == JOptionPane.YES_OPTION) {
                //Usuário clicou em Sim. Executar o código correspondente.
                return true;
            }
        }
        
        return false;
    }
    
    public void limpaCampos() {
        this.EdtNomeFantasia.setText("");
        this.EdtCodigo.setText("");
        this.EdtRazaoSocial.setText("");
        this.EdtCNPJ.setText("");
        this.EdtEndereco.setText("");
        this.EdtNumero.setText("");
        this.EdtEmail.setText("");
        this.EdtTelefone.setText("");
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        EdtRazaoSocial = new javax.swing.JTextField();
        EdtCodigo = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        EdtNomeFantasia = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        PanelBotoes = new javax.swing.JPanel();
        btnNovo = new javax.swing.JButton();
        btnGravar = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        btnConsultar = new javax.swing.JButton();
        btnSair = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        EdtEndereco = new javax.swing.JTextField();
        EdtNumero = new javax.swing.JTextField();
        EdtTelefone = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        EdtCNPJ = new javax.swing.JTextField();
        EdtEmail = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setTitle("Cadastro de Empresa");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosed(evt);
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        EdtRazaoSocial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EdtRazaoSocialActionPerformed(evt);
            }
        });

        EdtCodigo.setEditable(false);
        EdtCodigo.setBackground(new java.awt.Color(255, 255, 0));

        jLabel1.setText("Codigo:");

        jLabel6.setText("Email:");

        jLabel2.setText("Razão social:");

        jLabel3.setText("CNPJ:");

        EdtNomeFantasia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EdtNomeFantasiaActionPerformed(evt);
            }
        });
        EdtNomeFantasia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                EdtNomeFantasiaKeyTyped(evt);
            }
        });

        jLabel4.setText("Nome Fantasia:");

        jLabel5.setText("Endereço:");

        PanelBotoes.setBackground(new java.awt.Color(204, 204, 204));

        btnNovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/plus.png"))); // NOI18N
        btnNovo.setText("Novo");
        btnNovo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnNovo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });

        btnGravar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/save.png"))); // NOI18N
        btnGravar.setText("Gravar");
        btnGravar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnGravar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGravarActionPerformed(evt);
            }
        });

        btnAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/refresh.png"))); // NOI18N
        btnAlterar.setText("Alterar");
        btnAlterar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAlterar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });

        btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/garbage.png"))); // NOI18N
        btnExcluir.setText("Excluir");
        btnExcluir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnExcluir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        btnConsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/lens.png"))); // NOI18N
        btnConsultar.setText("Consultar");
        btnConsultar.setBorder(null);
        btnConsultar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnConsultar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarActionPerformed(evt);
            }
        });

        btnSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/opened-door-aperture.png"))); // NOI18N
        btnSair.setText("Sair");
        btnSair.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSair.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/x-button.png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.setBorder(null);
        btnCancelar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCancelar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelBotoesLayout = new javax.swing.GroupLayout(PanelBotoes);
        PanelBotoes.setLayout(PanelBotoesLayout);
        PanelBotoesLayout.setHorizontalGroup(
            PanelBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelBotoesLayout.createSequentialGroup()
                .addComponent(btnNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 40, Short.MAX_VALUE))
        );
        PanelBotoesLayout.setVerticalGroup(
            PanelBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnAlterar, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
            .addComponent(btnExcluir, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnConsultar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnSair, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnNovo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnGravar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        EdtEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EdtEnderecoActionPerformed(evt);
            }
        });

        EdtNumero.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                EdtNumeroFocusLost(evt);
            }
        });
        EdtNumero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EdtNumeroActionPerformed(evt);
            }
        });

        EdtTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EdtTelefoneActionPerformed(evt);
            }
        });
        EdtTelefone.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                EdtTelefoneKeyTyped(evt);
            }
        });

        jLabel7.setText("Número:");

        EdtCNPJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EdtCNPJActionPerformed(evt);
            }
        });
        EdtCNPJ.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                EdtCNPJKeyTyped(evt);
            }
        });

        EdtEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EdtEmailActionPerformed(evt);
            }
        });
        EdtEmail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                EdtEmailKeyTyped(evt);
            }
        });

        jLabel8.setText("Telefone:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(PanelBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(39, 39, 39)
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(EdtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(EdtRazaoSocial, javax.swing.GroupLayout.PREFERRED_SIZE, 443, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(161, 161, 161)
                        .addComponent(jLabel4))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(EdtCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(EdtNomeFantasia, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(370, 370, 370)
                        .addComponent(jLabel7))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(EdtEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(EdtNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(320, 320, 320)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(EdtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(EdtTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(EdtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(EdtRazaoSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(EdtCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(EdtNomeFantasia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel7))
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(EdtEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(EdtNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel8))
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(EdtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(EdtTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(PanelBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void EdtRazaoSocialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EdtRazaoSocialActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EdtRazaoSocialActionPerformed

    private void EdtNomeFantasiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EdtNomeFantasiaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EdtNomeFantasiaActionPerformed

    private void EdtNomeFantasiaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EdtNomeFantasiaKeyTyped
        
    }//GEN-LAST:event_EdtNomeFantasiaKeyTyped

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        this.controller.evento("btnNovo");
    }//GEN-LAST:event_btnNovoActionPerformed

    private void btnGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGravarActionPerformed
        this.controller.evento("btnGravar");
    }//GEN-LAST:event_btnGravarActionPerformed

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed
        this.controller.evento("btnAlterar");
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        this.controller.evento("btnExcluir");
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarActionPerformed
        this.controller.evento("btnConsultar");
    }//GEN-LAST:event_btnConsultarActionPerformed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        this.controller.evento("btnSair");
    }//GEN-LAST:event_btnSairActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        this.controller.evento("btnCancelar");
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void EdtEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EdtEnderecoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EdtEnderecoActionPerformed

    private void EdtNumeroFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_EdtNumeroFocusLost
        this.controller.evento("EdtCodigo");
    }//GEN-LAST:event_EdtNumeroFocusLost

    private void EdtNumeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EdtNumeroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EdtNumeroActionPerformed

    private void EdtTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EdtTelefoneActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EdtTelefoneActionPerformed

    private void EdtTelefoneKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EdtTelefoneKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_EdtTelefoneKeyTyped

    private void EdtCNPJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EdtCNPJActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EdtCNPJActionPerformed

    private void EdtCNPJKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EdtCNPJKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_EdtCNPJKeyTyped

    private void EdtEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EdtEmailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EdtEmailActionPerformed

    private void EdtEmailKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EdtEmailKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_EdtEmailKeyTyped

    private void formInternalFrameClosed(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosed
        this.controller.evento("FormClose");
    }//GEN-LAST:event_formInternalFrameClosed

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        this.controller.evento("FormShow");
    }//GEN-LAST:event_formInternalFrameOpened

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        // TODO add your handling code here:
    }//GEN-LAST:event_formInternalFrameActivated


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField EdtCNPJ;
    private javax.swing.JTextField EdtCodigo;
    private javax.swing.JTextField EdtEmail;
    private javax.swing.JTextField EdtEndereco;
    private javax.swing.JTextField EdtNomeFantasia;
    private javax.swing.JTextField EdtNumero;
    private javax.swing.JTextField EdtRazaoSocial;
    private javax.swing.JTextField EdtTelefone;
    private javax.swing.JPanel PanelBotoes;
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnConsultar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnGravar;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnSair;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    // End of variables declaration//GEN-END:variables

}
