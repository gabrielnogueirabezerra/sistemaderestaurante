/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Controllers.FrmCadastroProdutoController;
import Models.Configuracao;
import Models.InterfaceCadastro;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author gabri
 */
public class FrmCadastroProduto extends javax.swing.JInternalFrame implements InterfaceCadastro {

    private FrmCadastroProdutoController controller;

    /**
     * Creates new form FrmCadastroProduto
     */
    private FrmCadastroProduto() {
        initComponents();
    }

    public boolean validaCampos() {
        if (this.EdtNome.getText().trim().equals("")) {
            this.EdtNome.requestFocus();
            this.mostraMensagem("Informe o nome do produto.");
            return false;
        }

        if (this.EdtValor.getText().trim().equals("")) {
            this.EdtValor.requestFocus();
            this.mostraMensagem("Informe o valor do produto.");
            return false;
        }

        if (this.EdtDescricaoSecao.getText().trim().equals("")) {
            this.EdtCodigoSecao.requestFocus();
            this.mostraMensagem("Informe a seção do produto.");
            return false;
        }

        return true;
    }

    public void limpaCampos() {
        this.EdtNome.setText("");
        this.EdtCodigo.setText("");
        this.EdtValor.setText("");
        this.EdtCaminhoFoto.setText("Nenhuma Foto");
        this.EdtCaminhoFoto.setIcon(null);
        this.EdtCodigoSecao.setText("");
        this.EdtDescricaoSecao.setText("");
        this.EdtDescricao.setText("");
    }

    public void mostraMensagem(String mensagem) {
        if (mensagem != null) {
            JOptionPane.showMessageDialog(this, mensagem);
        }
    }

    public boolean mensagemVerificacao(String mensagem) {
        if (mensagem != null) {
            int resposta = JOptionPane.showConfirmDialog(this, mensagem, "Atenção!", JOptionPane.YES_NO_OPTION);

            if (resposta == JOptionPane.YES_OPTION) {
                //Usuário clicou em Sim. Executar o código correspondente.
                return true;
            }
        }

        return false;
    }

    public FrmCadastroProduto(Configuracao model) {
        this();
        if (model != null) {
            this.controller = new FrmCadastroProdutoController(this, model);
        }
    }

    public JTextField getNome() {
        return this.EdtNome;
    }

    public JTextField getId() {
        return this.EdtCodigo;
    }

    public JTextField getValor() {
        return this.EdtValor;
    }

    public JLabel getCaminhoFoto() {
        return this.EdtCaminhoFoto;
    }

    public JTextField getCodigoSecao() {
        return this.EdtCodigoSecao;
    }

    public JTextField getDescricaoSecao() {
        return this.EdtDescricaoSecao;
    }

    public JTextArea getDescricao() {
        return this.EdtDescricao;
    }

    @Override
    public JButton getBtnNovo() {
        return this.btnNovo;
    }

    @Override
    public JButton getBtnGravar() {
        return this.btnGravar;
    }

    @Override
    public JButton getBtnAlterar() {
        return this.btnAlterar;
    }

    @Override
    public JButton getBtnCancelar() {
        return this.btnCancelar;
    }

    @Override
    public JButton getBtnConsultar() {
        return this.btnConsultar;
    }

    @Override
    public JButton getBtnExcluir() {
        return this.btnExcluir;
    }

    @Override
    public JButton getBtnSair() {
        return this.btnSair;
    }

    @Override
    public void botoes(char opcao) {
        switch (opcao) {
            case 'N':
                this.getBtnNovo().setEnabled(false);
                this.getBtnGravar().setEnabled(true);
                this.getBtnAlterar().setEnabled(false);
                this.getBtnCancelar().setEnabled(true);
                this.getBtnConsultar().setEnabled(false);
                this.getBtnExcluir().setEnabled(false);
                this.getBtnSair().setEnabled(true);

                this.EdtNome.setEnabled(true);
                this.EdtValor.setEnabled(true);
                this.btnSelecionarFoto.setEnabled(true);
                this.EdtCodigoSecao.setEnabled(true);
                this.BtnPesquisarSecao.setEnabled(true);
                this.BtnCadastrarSecao.setEnabled(true);
                this.EdtDescricao.setEnabled(true);

                break;
            case 'G':
                this.getBtnNovo().setEnabled(true);
                this.getBtnGravar().setEnabled(false);
                this.getBtnAlterar().setEnabled(true);
                this.getBtnCancelar().setEnabled(false);
                this.getBtnConsultar().setEnabled(true);
                this.getBtnExcluir().setEnabled(true);
                this.getBtnSair().setEnabled(true);

                this.EdtNome.setEnabled(false);
                this.EdtValor.setEnabled(false);
                this.btnSelecionarFoto.setEnabled(false);
                this.EdtCodigoSecao.setEnabled(false);
                this.BtnPesquisarSecao.setEnabled(false);
                this.BtnCadastrarSecao.setEnabled(false);
                this.EdtDescricao.setEnabled(false);

                break;
            case 'A':
                this.botoes('N');
                break;
            case 'C':
                this.getBtnNovo().setEnabled(true);
                this.getBtnGravar().setEnabled(false);
                this.getBtnAlterar().setEnabled(false);
                this.getBtnCancelar().setEnabled(false);
                this.getBtnConsultar().setEnabled(true);
                this.getBtnExcluir().setEnabled(false);
                this.getBtnSair().setEnabled(true);

                this.EdtNome.setEnabled(false);
                this.EdtValor.setEnabled(false);
                this.btnSelecionarFoto.setEnabled(false);
                this.EdtCodigoSecao.setEnabled(false);
                this.BtnPesquisarSecao.setEnabled(false);
                this.BtnCadastrarSecao.setEnabled(false);
                this.EdtDescricao.setEnabled(false);
                break;
            case 'E':
                this.botoes('N');
                break;
            default:
                break;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PanelBotoes = new javax.swing.JPanel();
        btnNovo = new javax.swing.JButton();
        btnGravar = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        btnConsultar = new javax.swing.JButton();
        btnSair = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        EdtNome = new javax.swing.JTextField();
        EdtCodigo = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        EdtValor = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        btnSelecionarFoto = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        EdtDescricaoSecao = new javax.swing.JTextField();
        EdtCodigoSecao = new javax.swing.JTextField();
        BtnPesquisarSecao = new javax.swing.JButton();
        BtnCadastrarSecao = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        EdtDescricao = new javax.swing.JTextArea();
        EdtCaminhoFoto = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setTitle("Cadastro de Produto");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosed(evt);
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        PanelBotoes.setBackground(new java.awt.Color(204, 204, 204));

        btnNovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/plus.png"))); // NOI18N
        btnNovo.setText("Novo");
        btnNovo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnNovo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });

        btnGravar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/save.png"))); // NOI18N
        btnGravar.setText("Gravar");
        btnGravar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnGravar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGravarActionPerformed(evt);
            }
        });

        btnAlterar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/refresh.png"))); // NOI18N
        btnAlterar.setText("Alterar");
        btnAlterar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAlterar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });

        btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/garbage.png"))); // NOI18N
        btnExcluir.setText("Excluir");
        btnExcluir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnExcluir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        btnConsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/lens.png"))); // NOI18N
        btnConsultar.setText("Consultar");
        btnConsultar.setBorder(null);
        btnConsultar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnConsultar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarActionPerformed(evt);
            }
        });

        btnSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/opened-door-aperture.png"))); // NOI18N
        btnSair.setText("Sair");
        btnSair.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSair.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/x-button.png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.setBorder(null);
        btnCancelar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCancelar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelBotoesLayout = new javax.swing.GroupLayout(PanelBotoes);
        PanelBotoes.setLayout(PanelBotoesLayout);
        PanelBotoesLayout.setHorizontalGroup(
            PanelBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelBotoesLayout.createSequentialGroup()
                .addComponent(btnNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 10, Short.MAX_VALUE))
        );
        PanelBotoesLayout.setVerticalGroup(
            PanelBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnAlterar, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
            .addComponent(btnExcluir, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
            .addComponent(btnConsultar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
            .addComponent(btnSair, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
            .addComponent(btnNovo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
            .addComponent(btnCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
            .addComponent(btnGravar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        EdtNome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EdtNomeActionPerformed(evt);
            }
        });

        EdtCodigo.setEditable(false);
        EdtCodigo.setBackground(new java.awt.Color(255, 255, 0));

        jLabel1.setText("Codigo:");

        jLabel2.setText("Nome:");

        jLabel3.setText("Valor:");

        EdtValor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EdtValorActionPerformed(evt);
            }
        });
        EdtValor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                EdtValorKeyTyped(evt);
            }
        });

        jLabel4.setText("Foto:");

        btnSelecionarFoto.setText("Selecionar");
        btnSelecionarFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelecionarFotoActionPerformed(evt);
            }
        });

        jLabel5.setText("Seção");

        EdtDescricaoSecao.setEditable(false);
        EdtDescricaoSecao.setBackground(new java.awt.Color(255, 255, 255));
        EdtDescricaoSecao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EdtDescricaoSecaoActionPerformed(evt);
            }
        });

        EdtCodigoSecao.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                EdtCodigoSecaoFocusLost(evt);
            }
        });
        EdtCodigoSecao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EdtCodigoSecaoActionPerformed(evt);
            }
        });

        BtnPesquisarSecao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/find.png"))); // NOI18N
        BtnPesquisarSecao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPesquisarSecaoActionPerformed(evt);
            }
        });

        BtnCadastrarSecao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/add.png"))); // NOI18N
        BtnCadastrarSecao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCadastrarSecaoActionPerformed(evt);
            }
        });

        jLabel6.setText("Descrição:");

        EdtDescricao.setColumns(20);
        EdtDescricao.setRows(5);
        jScrollPane1.setViewportView(EdtDescricao);

        EdtCaminhoFoto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        EdtCaminhoFoto.setText("Nenhuma Foto");
        EdtCaminhoFoto.setFocusable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(EdtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(EdtNome)))
                    .addComponent(jLabel5)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(EdtCodigoSecao, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BtnPesquisarSecao, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(BtnCadastrarSecao, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(EdtDescricaoSecao, javax.swing.GroupLayout.PREFERRED_SIZE, 403, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(EdtValor, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(EdtCaminhoFoto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSelecionarFoto))))
                    .addComponent(jLabel6)
                    .addComponent(PanelBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(EdtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(EdtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(EdtValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSelecionarFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(EdtCaminhoFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addGap(4, 4, 4)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BtnPesquisarSecao, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(EdtDescricaoSecao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(EdtCodigoSecao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(BtnCadastrarSecao, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(PanelBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        this.controller.evento("btnNovo");
    }//GEN-LAST:event_btnNovoActionPerformed

    private void btnGravarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGravarActionPerformed
        this.controller.evento("btnGravar");
    }//GEN-LAST:event_btnGravarActionPerformed

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed
        this.controller.evento("btnAlterar");
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        this.controller.evento("btnExcluir");
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarActionPerformed
        this.controller.evento("btnConsultar");
    }//GEN-LAST:event_btnConsultarActionPerformed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        this.controller.evento("btnSair");
    }//GEN-LAST:event_btnSairActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        this.controller.evento("btnCancelar");
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void EdtNomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EdtNomeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EdtNomeActionPerformed

    private void formInternalFrameClosed(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosed
        this.controller.evento("FormClose");
    }//GEN-LAST:event_formInternalFrameClosed

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        this.controller.evento("FormShow");
    }//GEN-LAST:event_formInternalFrameOpened

    private void EdtValorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EdtValorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EdtValorActionPerformed

    private void btnSelecionarFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelecionarFotoActionPerformed
        this.controller.evento("btnSelecionarFoto");
    }//GEN-LAST:event_btnSelecionarFotoActionPerformed

    private void EdtDescricaoSecaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EdtDescricaoSecaoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EdtDescricaoSecaoActionPerformed

    private void EdtCodigoSecaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EdtCodigoSecaoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EdtCodigoSecaoActionPerformed

    private void BtnPesquisarSecaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPesquisarSecaoActionPerformed
        this.controller.evento("btnPesquisarSecao");
    }//GEN-LAST:event_BtnPesquisarSecaoActionPerformed

    private void BtnCadastrarSecaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCadastrarSecaoActionPerformed
        this.controller.evento("btnCadastrarSecao");
    }//GEN-LAST:event_BtnCadastrarSecaoActionPerformed

    private void EdtCodigoSecaoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_EdtCodigoSecaoFocusLost
        this.controller.evento("EdtCodigoSecaoSaida");
    }//GEN-LAST:event_EdtCodigoSecaoFocusLost

    private void EdtValorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EdtValorKeyTyped
        this.controller.evento(evt);
    }//GEN-LAST:event_EdtValorKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCadastrarSecao;
    private javax.swing.JButton BtnPesquisarSecao;
    private javax.swing.JLabel EdtCaminhoFoto;
    private javax.swing.JTextField EdtCodigo;
    private javax.swing.JTextField EdtCodigoSecao;
    private javax.swing.JTextArea EdtDescricao;
    private javax.swing.JTextField EdtDescricaoSecao;
    private javax.swing.JTextField EdtNome;
    private javax.swing.JTextField EdtValor;
    private javax.swing.JPanel PanelBotoes;
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnConsultar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnGravar;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnSair;
    private javax.swing.JButton btnSelecionarFoto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
