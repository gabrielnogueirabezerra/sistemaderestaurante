unit uDM;

interface

uses
  System.SysUtils, System.Classes, ACBrBase, ACBrPosPrinter;

type
  TDM = class(TDataModule)
    FACBrPosPrinter: TACBrPosPrinter;
  private
    { Private declarations }
  public
    { Public declarations }

    function FormataStringE(Valor, Tamanho, Complemento: string): string;
  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}
{$R *.dfm}
{ TDM }

function TDM.FormataStringE(Valor, Tamanho, Complemento: string): string;

var
  X, Y: Integer;
begin
  Y := Length(Valor);
  for X := Y to StrToInt(Tamanho) do
  begin
    if (X <> StrToInt(Tamanho)) then
      Valor := Valor + Complemento
    else
      Valor := Valor + '';
  end;
  Result := Valor;
end;

end.
