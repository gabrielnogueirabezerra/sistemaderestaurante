object FrmPrincipal: TFrmPrincipal
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'WebService - Gerenciamento de Impress'#227'o'
  ClientHeight = 105
  ClientWidth = 378
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 34
    Height = 13
    Caption = 'Modelo'
  end
  object Label2: TLabel
    Left = 191
    Top = 8
    Width = 26
    Height = 13
    Caption = 'Porta'
  end
  object Label3: TLabel
    Left = 191
    Top = 54
    Width = 38
    Height = 13
    Caption = 'Colunas'
  end
  object Label4: TLabel
    Left = 8
    Top = 54
    Width = 83
    Height = 13
    Caption = 'P'#225'gina de C'#243'digo'
  end
  object cbxModelo: TComboBox
    Left = 8
    Top = 24
    Width = 177
    Height = 21
    Style = csDropDownList
    TabOrder = 0
  end
  object cbxPorta: TComboBox
    Left = 191
    Top = 24
    Width = 177
    Height = 21
    TabOrder = 1
  end
  object EdtNumeroColunas: TSpinEdit
    Left = 191
    Top = 69
    Width = 177
    Height = 22
    MaxValue = 60
    MinValue = 0
    TabOrder = 2
    Value = 48
  end
  object cbXPagCodigo: TComboBox
    Left = 8
    Top = 69
    Width = 177
    Height = 21
    Style = csDropDownList
    TabOrder = 3
  end
end
