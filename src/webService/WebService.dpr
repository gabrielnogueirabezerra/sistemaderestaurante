program WebService;

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  uFrmPrincipal in 'uFrmPrincipal.pas' {FrmPrincipal},
  Controller.Impressao in 'Controller.Impressao.pas',
  Server.Container in 'Server.Container.pas' {SC: TDataModule},
  uDM in 'uDM.pas' {DM: TDataModule},
  uComprovante in 'uComprovante.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmPrincipal, FrmPrincipal);
  Application.CreateForm(TSC, SC);
  Application.CreateForm(TDM, DM);
  Application.Run;
end.

