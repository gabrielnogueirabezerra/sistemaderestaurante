unit Controller.Impressao;

interface

uses System.Classes, Datasnap.DSServer, Datasnap.DSAuth, uDM;

type
{$METHODINFO ON}
  TControllerImpressao = class(TComponent)
  private
    { Private declarations }
  public
    { Public declarations }
    function EchoString(Value: string): string;
    function ReverseString(Value: string): string;
    function updateImprimirComprovante(Value: String): Integer;
  end;
{$METHODINFO OFF}

implementation

uses System.StrUtils, System.JSON, uComprovante, uFrmPrincipal,
  System.SysUtils, Vcl.Dialogs;

function TControllerImpressao.EchoString(Value: string): string;
begin
  Result := Value;
end;

function TControllerImpressao.ReverseString(Value: string): string;
begin
  Result := System.StrUtils.ReverseString(Value);
end;

function TControllerImpressao.updateImprimirComprovante(Value: String): Integer;
var
  comprovante: TComprovante;
  RazaoSocial, Endereco, Numero, Cnpj, Telefone, DataHora: String;
  NomeProduto, QuantidadeProduto, ValorProduto, TotalProduto: String;
  NomeForma, ValorForma: String;
  jsonObj, jSubObj, lJsonObj: TJSONObject;
  jv, lJsonVal, jvItem: TJSONValue;
  J: Integer;
  lJsonArray: TJSONArray;
  lJsonPair: TJSONPair;
  item: TJSONObject;
begin
  jsonObj := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(Value), 0)
    as TJSONObject;

  jv := jsonObj.Get('empresa').JsonValue;
  jSubObj := jv as TJSONObject;

  RazaoSocial := jSubObj.Get('razaoSocial').JsonValue.Value;
  Endereco := jSubObj.Get('endereco').JsonValue.Value;
  Numero := jSubObj.Get('numero').JsonValue.Value;
  Cnpj := jSubObj.Get('cnpj').JsonValue.Value;
  Telefone := jSubObj.Get('telefone').JsonValue.Value;

  comprovante := TComprovante.Create;
  comprovante.titulo := 'Comprovante de Venda';
  comprovante.abreComprovante(inttostr(FrmPrincipal.cbxModelo.ItemIndex),
    inttostr(FrmPrincipal.cbXPagCodigo.ItemIndex), FrmPrincipal.cbxPorta.Text,
    '', FrmPrincipal.EdtNumeroColunas.Text, '0', true, true, RazaoSocial,
    Endereco, Numero, Cnpj, Telefone);

  jv := jsonObj.Get('dataHora').JsonValue;
  jSubObj := jv as TJSONObject;

  DataHora := jSubObj.Get('dayOfMonth').JsonValue.Value + '/' +
    jSubObj.Get('month').JsonValue.Value + '/' + jSubObj.Get('year')
    .JsonValue.Value + ' ' + jSubObj.Get('hourOfDay').JsonValue.Value + ':' +
    jSubObj.Get('minute').JsonValue.Value + ':' + jSubObj.Get('second')
    .JsonValue.Value;

  comprovante.imprimeTextoComprovanteValor('DATA/HORA', DataHora);

  if jsonObj.Get('nomeCliente').JsonValue.Value <> '' then
    comprovante.imprimeTextoComprovante('CLIENTE: ' + jsonObj.Get('nomeCliente')
      .JsonValue.Value, 'E', true);

  comprovante.imprimeTextoComprovante(comprovante.linhaSimples);
  comprovante.imprimeTextoComprovanteEspaco('PRODUTO', 'QTD  VALOR  TOTAL');
  comprovante.imprimeTextoComprovante(comprovante.linhaSimples);

  jv := jsonObj.Get('itensVenda').JsonValue;

  lJsonArray := jv as TJSONArray;

  for J := 0 to lJsonArray.Count - 1 do
  begin
    item := lJsonArray.Items[J] as TJSONObject;
    QuantidadeProduto := item.GetValue('quantidade').Value;
    ValorProduto := item.GetValue('valor').Value;
    TotalProduto := currtostr(strtocurr(item.GetValue('quantidade').Value) *
      strtocurr(item.GetValue('valor').Value));
    jvItem := item.GetValue('produto');
    item := jvItem as TJSONObject;
    NomeProduto := item.GetValue('nome').Value;

    comprovante.imprimeTextoComprovante(NomeProduto, 'E', false);

    comprovante.imprimeTextoComprovanteValor('',
      DM.FormataStringE(QuantidadeProduto, '4', ' ') + ' ' +
      DM.FormataStringE(ValorProduto, '9', ' ') + ' ' +
      DM.FormataStringE(TotalProduto, '0', ' '));
  end;

  comprovante.imprimeTextoComprovante(comprovante.linhaSimples);

  comprovante.imprimeTextoComprovante('FORMAS DE PAGAMENTO', 'C', false);

  comprovante.imprimeTextoComprovante(comprovante.linhaSimples);
  comprovante.imprimeTextoComprovanteEspaco('DESCRICAO', 'VALOR');
  comprovante.imprimeTextoComprovante(comprovante.linhaSimples);

  jv := jsonObj.Get('vendaFormasPagamento').JsonValue;

  lJsonArray := jv as TJSONArray;

  for J := 0 to lJsonArray.Count - 1 do
  begin
    item := lJsonArray.Items[J] as TJSONObject;
    ValorForma := item.GetValue('valor').Value;
    jvItem := item.GetValue('formaPagamento');
    item := jvItem as TJSONObject;
    NomeForma := item.GetValue('descricao').Value;

    comprovante.imprimeTextoComprovanteValor(NomeForma, ValorForma);
  end;
  comprovante.imprimeTextoComprovante(comprovante.linhaSimples);

  jv := jsonObj.Get('acrescimo').JsonValue;
  comprovante.imprimeTextoComprovanteValor('ACRESCIMO', jv.Value);

  jv := jsonObj.Get('desconto').JsonValue;
  comprovante.imprimeTextoComprovanteValor('DESCONTO', jv.Value);

  jv := jsonObj.Get('total').JsonValue;
  comprovante.imprimeTextoComprovanteValor('TOTAL', jv.Value);

  comprovante.fechaComprovante(true, false, 8, true);

  Result := 200;
end;

end.
