unit uFrmPrincipal;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  IPPeerServer,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.Samples.Spin;

type
  TFrmPrincipal = class(TForm)
    Label1: TLabel;
    cbxModelo: TComboBox;
    Label2: TLabel;
    cbxPorta: TComboBox;
    Label3: TLabel;
    EdtNumeroColunas: TSpinEdit;
    Label4: TLabel;
    cbXPagCodigo: TComboBox;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmPrincipal: TFrmPrincipal;

implementation

uses
  ACBrPosPrinter, System.TypInfo, Vcl.Printers, uDM;

{$R *.dfm}

procedure TFrmPrincipal.FormShow(Sender: TObject);
var
  I: TACBrPosPrinterModelo;
  J: TACBrPosPaginaCodigo;
  K: Integer;
begin
  cbxModelo.Items.Clear;
  For I := Low(TACBrPosPrinterModelo) to High(TACBrPosPrinterModelo) do
    cbxModelo.Items.Add(GetEnumName(TypeInfo(TACBrPosPrinterModelo),
      Integer(I)));

  cbXPagCodigo.Items.Clear;
  For J := Low(TACBrPosPaginaCodigo) to High(TACBrPosPaginaCodigo) do
    cbXPagCodigo.Items.Add(GetEnumName(TypeInfo(TACBrPosPaginaCodigo),
      Integer(J)));

  cbxPorta.Items.Clear;
  DM.FACBrPosPrinter.Device.AcharPortasSeriais(cbxPorta.Items);
  cbxPorta.Items.Add('C:\Users\gabri\OneDrive\Documentos\Engenharia ' +
    'de Software\6º Semestre - 2019.1\Manutenção de Software\Siste' +
    'ma\webService\comprovante.txt');
  cbxPorta.Items.Add('LPT1');
  cbxPorta.Items.Add('LPT2');
  cbxPorta.Items.Add('\\localhost\Epson');
  cbxPorta.Items.Add('c:\temp\ecf.txt');
  cbxPorta.Items.Add('TCP:192.168.0.31:9100');

  For K := 0 to Printer.Printers.Count - 1 do
    cbxPorta.Items.Add('RAW:' + Printer.Printers[K]);

  cbxPorta.Items.Add('/dev/ttyS0');
  cbxPorta.Items.Add('/dev/ttyS1');
  cbxPorta.Items.Add('/dev/ttyUSB0');
  cbxPorta.Items.Add('/dev/ttyUSB1');
  cbxPorta.Items.Add('/tmp/ecf.txt');

  cbxPorta.ItemIndex := 0;
  cbxModelo.ItemIndex := 0;
  cbXPagCodigo.ItemIndex := 3;
end;

end.
