package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;

import Conexao.Conexao;
import Models.Secao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

/**
 *
 * @author Beatriz Oliveiira
 */
public class SecaoDAO extends DAO<Secao> {

    private Conexao dao = Conexao.getInstanciaDaConexao();
    private static SecaoDAO instancia;

    public static SecaoDAO getInstancia() {
        if (instancia == null) {
            instancia = new SecaoDAO();
        }
        return instancia;
    }

    @Override
    public void inserir(Secao secao) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;

        try {
            stmt = conexao.prepareStatement("INSERT INTO `secao` (`id_secao`,`descricao`) VALUES (?, ?)");
            stmt.setInt(1, secao.getId());
            stmt.setString(2, secao.getDescricao());

            stmt.executeUpdate();

            secao.setId(this.find());

        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void alterar(Secao secao) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;

        try {
            stmt = conexao.prepareStatement("UPDATE `secao` SET `descricao` = ? WHERE `id_secao` = ?");
            stmt.setString(1, secao.getDescricao());
            stmt.setInt(2, secao.getId());

            stmt.executeUpdate();

        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void excluir(Secao secao) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;

        try {
            stmt = conexao.prepareStatement("DELETE FROM SECAO WHERE `id_secao` = ?");
            stmt.setInt(1, secao.getId());

            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void buscar(Secao secao) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = conexao.prepareStatement("SELECT `descricao` FROM `secao` WHERE `id_secao` = ?");
            stmt.setInt(1, secao.getId());

            result = stmt.executeQuery();

            while (result.next()) {
                secao.setDescricao(result.getString("descricao"));
            }

        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
        }
    }

    @Override
    public ArrayList<Secao> buscarTodos() throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<Secao> secoes = new ArrayList<>();

        try {
            stmt = conexao.prepareStatement("SELECT `id_secao`, `descricao` FROM `secao` order by id_secao");
            result = stmt.executeQuery();

            while (result.next()) {
                Secao secao = new Secao();
                secao.setId(result.getInt("id_secao"));
                secao.setDescricao(result.getString("descricao"));
                secoes.add(secao);
            }

        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return secoes;
        }
    }

    private int find() throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        int resultado = 0;

        try {
            stmt = conexao.prepareStatement("SELECT AUTO_INCREMENT as id FROM information_schema.tables WHERE table_name = 'secao' AND table_schema = 'banco_restaurante'");
            result = stmt.executeQuery();

            while (result.next()) {
                resultado = result.getInt("id");
            }

        } finally {
            Conexao.fecharConexao(conexao, stmt);
            return resultado - 1;
        }
    }

    public ArrayList<Secao> buscaTodosPorDescricao(String descricao) {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<Secao> secoes = new ArrayList<>();
        try {
            stmt = conexao.prepareStatement("SELECT * FROM SECAO where descricao like ? ORDER BY ID_SECAO");
            stmt.setString(1, "%" + descricao + "%");
            result = stmt.executeQuery();

            while (result.next()) {
                Secao secao = new Secao();
                secao.setId(result.getInt("id_secao"));
                secao.setDescricao(result.getString("descricao"));

                secoes.add(secao);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return secoes;
        }

    }

}
