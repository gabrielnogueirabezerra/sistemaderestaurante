package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;

import Conexao.Conexao;
import Models.Criptografia;
import Models.Usuario;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Beatriz Oliveiira
 */
public class UsuarioDAO extends DAO<Usuario> {

    private Conexao dao = Conexao.getInstanciaDaConexao();
    private static UsuarioDAO instancia;

    public static UsuarioDAO getInstancia() {
        if (instancia == null) {
            instancia = new UsuarioDAO();
        }
        return instancia;
    }

    @Override
    public void inserir(Usuario usuario) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("INSERT INTO `usuario`(`id_usuario`, `login`, `senha`) VALUES (?, ?, ?)");
            stmt.setInt(1, usuario.getId());
            stmt.setString(2, usuario.getLogin());
            stmt.setString(3, usuario.getSenha());

            stmt.executeUpdate();

            usuario.setId(this.find());
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void alterar(Usuario usuario) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("UPDATE `usuario` SET `login` = ?, `senha` = ? "
                    + "WHERE `id_usuario` = ?");
            stmt.setString(1, usuario.getLogin());
            stmt.setString(2, usuario.getSenha());
            stmt.setInt(3, usuario.getId());

            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void buscar(Usuario usuario) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = conexao.prepareStatement("SELECT `id_usuario`, `login`, `senha`"
                    + " FROM `usuario` WHERE `id_usuario` = ? ");
            stmt.setInt(1, usuario.getId());
            result = stmt.executeQuery();

            while (result.next()) {
                usuario.setId(result.getInt("id_usuario"));
                usuario.setLogin(result.getString("login"));
                try {
                    usuario.setSenha(Criptografia.decrypt(result.getString("senha")));
                } catch (GeneralSecurityException | IOException ex) {
                    Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
        }
    }

    @Override
    public ArrayList<Usuario> buscarTodos() throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<Usuario> usuarios = new ArrayList<>();

        try {
            stmt = conexao.prepareStatement("SELECT `id_usuario`, `login`, `senha` FROM `usuario` ORDER BY id_usuario");
            result = stmt.executeQuery();

            while (result.next()) {
                Usuario usuario = new Usuario();
                usuario.setId(result.getInt("id_usuario"));
                usuario.setLogin(result.getString("login"));
                usuario.setSenha(Criptografia.decrypt(result.getString("senha")));

                usuarios.add(usuario);
            }

        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return usuarios;
        }
    }

    public void logar(Usuario usuario) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = conexao.prepareStatement("SELECT `id_usuario`, `login`, `senha`"
                    + " FROM `usuario` WHERE `login` = ? and `senha` = ? ");
            stmt.setString(1, usuario.getLogin());
            stmt.setString(2, usuario.getSenha());
            result = stmt.executeQuery();

            while (result.next()) {
                usuario.setId(result.getInt("id_usuario"));
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
        }
    }

    public void excluir(Usuario usuario) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;

        try {
            stmt = conexao.prepareStatement("DELETE FROM USUARIO WHERE `id_usuario` = ?");
            stmt.setInt(1, usuario.getId());

            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    private int find() throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        int resultado = 0;

        try {
            stmt = conexao.prepareStatement("SELECT AUTO_INCREMENT as id FROM information_schema.tables "
                    + "WHERE table_name = 'usuario' AND table_schema = 'banco_restaurante'");
            result = stmt.executeQuery();

            while (result.next()) {
                resultado = result.getInt("id");
            }

        } finally {
            Conexao.fecharConexao(conexao, stmt);
            return resultado - 1;
        }
    }

}
