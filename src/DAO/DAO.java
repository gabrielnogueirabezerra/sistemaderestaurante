package DAO;

import Conexao.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Gabriel Nogueira Bezerra
 * @param <Object>
 */
public abstract class DAO<Object> {

    public abstract void inserir(Object objeto) throws SQLException, ClassNotFoundException;

    public abstract void alterar(Object objeto) throws SQLException, ClassNotFoundException;

    public abstract void buscar(Object objeto) throws SQLException, ClassNotFoundException;

    public abstract void excluir(Object objeto) throws SQLException, ClassNotFoundException;

    public abstract ArrayList<Object> buscarTodos() throws SQLException, ClassNotFoundException;
}
