package DAO;

import Conexao.Conexao;
import Models.Mesa;
import Models.Venda;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Beatriz Oliveiira
 */
public class VendaDAO extends DAO<Venda> {

    private Conexao dao = Conexao.getInstanciaDaConexao();
    private static VendaDAO instancia;

    public static VendaDAO getInstancia() {
        if (instancia == null) {
            instancia = new VendaDAO();
        }

        return instancia;
    }

    @Override
    public void inserir(Venda venda) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("INSERT INTO `venda`(`id_venda`, `id_mesa`, `data_hora`, `nome_cliente`, "
                    + "`desconto`, `acrescimo`) VALUES (?, ?, ?, ?, ?, ?)");
            stmt.setInt(1, venda.getId());
            stmt.setInt(2, venda.getMesa().getId());

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            stmt.setString(3, sdf.format(venda.getDataHora().getTime()));
            stmt.setString(4, venda.getNomeCliente());
            stmt.setDouble(5, venda.getDesconto());
            stmt.setDouble(6, venda.getAcrescimo());

            stmt.executeUpdate();

            venda.setId(this.find());
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void alterar(Venda venda) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("UPDATE `venda` SET `id_mesa` = ?,`data_hora` = ?, `nome_cliente` = ?, "
                    + "`desconto` = ?, `acrescimo` = ? WHERE `id_venda` = ?");
            stmt.setInt(1, venda.getMesa().getId());

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            stmt.setString(2, sdf.format(venda.getDataHora().getTime()));

            stmt.setString(3, venda.getNomeCliente());
            stmt.setDouble(4, venda.getDesconto());
            stmt.setDouble(5, venda.getAcrescimo());
            stmt.setInt(6, venda.getId());
            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void excluir(Venda venda) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {

            stmt = conexao.prepareStatement("DELETE FROM VENDA WHERE ID_VENDA = ?");
            stmt.setInt(1, venda.getId());
            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void buscar(Venda venda) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = conexao.prepareStatement("SELECT `id_mesa`, `data_hora`, `nome_cliente`, "
                    + "`desconto`, `acrescimo` FROM `venda` WHERE `id_venda` = ?");
            stmt.setInt(1, venda.getId());
            result = stmt.executeQuery();

            while (result.next()) {
                venda.setAcrescimo(result.getDouble("acrescimo"));
                venda.setDesconto(result.getDouble("desconto"));
                venda.setItensVenda(ItemVendaDAO.getInstancia().buscarPorVenda(venda));
                venda.setNomeCliente(result.getString("nome_cliente"));
                venda.setVendaFormasPagamento(VendaFormaPagamentoDAO.getInstancia().buscarPorVenda(venda));

                Mesa mesa = new Mesa();
                mesa.setId(result.getInt("id_mesa"));
                MesaDAO.getInstancia().buscar(mesa);
                venda.setMesa(mesa);

                Calendar calendarioDataHora = Calendar.getInstance();
                calendarioDataHora.setTime(result.getTimestamp("data_hora"));
                venda.setDataHora(calendarioDataHora);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
        }
    }

    public void buscarVendaMesa(Venda venda) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = conexao.prepareStatement("SELECT `id_venda`, `id_mesa`, `data_hora`, `nome_cliente`, "
                    + "`desconto`, `acrescimo` FROM `venda` WHERE `id_mesa` = ?");
            stmt.setInt(1, venda.getMesa().getId());
            result = stmt.executeQuery();

            while (result.next()) {
                venda.setId(result.getInt("id_venda"));
                venda.setAcrescimo(result.getDouble("acrescimo"));
                venda.setDesconto(result.getDouble("desconto"));
                venda.setItensVenda(ItemVendaDAO.getInstancia().buscarPorVenda(venda));
                venda.setNomeCliente(result.getString("nome_cliente"));
                venda.setVendaFormasPagamento(VendaFormaPagamentoDAO.getInstancia().buscarPorVenda(venda));

                Calendar calendarioDataHora = Calendar.getInstance();
                calendarioDataHora.setTime(result.getTimestamp("data_hora"));
                venda.setDataHora(calendarioDataHora);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
        }
    }

    @Override
    public ArrayList<Venda> buscarTodos() throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<Venda> vendas = new ArrayList<>();
        try {
            stmt = conexao.prepareStatement("SELECT *  FROM `venda` order by id_venda");
            result = stmt.executeQuery();

            while (result.next()) {
                Venda venda = new Venda();
                venda.setAcrescimo(result.getDouble("acrescimo"));
                venda.setDesconto(result.getDouble("desconto"));
                venda.setItensVenda(ItemVendaDAO.getInstancia().buscarPorVenda(venda));
                venda.setNomeCliente(result.getString("nome_cliente"));
                venda.setVendaFormasPagamento(VendaFormaPagamentoDAO.getInstancia().buscarPorVenda(venda));

                Mesa mesa = new Mesa();
                mesa.setId(result.getInt("id_mesa"));
                MesaDAO.getInstancia().buscar(mesa);
                venda.setMesa(mesa);

                Calendar calendarioDataHora = Calendar.getInstance();
                calendarioDataHora.setTime(result.getTimestamp("data_hora"));
                venda.setDataHora(calendarioDataHora);

                vendas.add(venda);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return vendas;
        }
    }

    public ArrayList<Venda> buscaTodos(String dataInicial, String dataFinal) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<Venda> vendas = new ArrayList<>();
        try {
            stmt = conexao.prepareStatement("SELECT *  FROM `venda` where data_hora between ? and ? order by id_venda");
            stmt.setString(1, dataInicial);
            stmt.setString(2, dataFinal);
            result = stmt.executeQuery();

            while (result.next()) {
                Venda venda = new Venda();
                venda.setId(result.getInt("id_venda"));
                venda.setAcrescimo(result.getDouble("acrescimo"));
                venda.setDesconto(result.getDouble("desconto"));
                venda.setItensVenda(ItemVendaDAO.getInstancia().buscarPorVenda(venda));
                venda.setNomeCliente(result.getString("nome_cliente"));
                venda.setVendaFormasPagamento(VendaFormaPagamentoDAO.getInstancia().buscarPorVenda(venda));

                Mesa mesa = new Mesa();
                mesa.setId(result.getInt("id_mesa"));
                MesaDAO.getInstancia().buscar(mesa);
                venda.setMesa(mesa);

                Calendar calendarioDataHora = Calendar.getInstance();
                calendarioDataHora.setTime(result.getTimestamp("data_hora"));
                venda.setDataHora(calendarioDataHora);

                vendas.add(venda);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return vendas;
        }
    }

    private int find() throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        int resultado = 0;

        try {
            stmt = conexao.prepareStatement("SELECT AUTO_INCREMENT as id FROM information_schema.tables WHERE "
                    + "table_name = 'venda' AND table_schema = 'banco_restaurante'");
            result = stmt.executeQuery();

            while (result.next()) {
                resultado = result.getInt("id");
            }

        } finally {
            Conexao.fecharConexao(conexao, stmt);
            return resultado - 1;
        }
    }

}
