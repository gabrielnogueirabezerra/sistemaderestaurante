package DAO;

import Conexao.Conexao;
import Models.Garcom;
import Models.ItemMesa;
import Models.Mesa;
import Models.Produto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Beatriz Oliveiira
 */
public class ItemMesaDAO extends DAO<ItemMesa> {

    private Conexao dao = Conexao.getInstanciaDaConexao();
    private static ItemMesaDAO instancia;

    public static ItemMesaDAO getInstancia() {
        if (instancia == null) {
            instancia = new ItemMesaDAO();
        }

        return instancia;
    }

    @Override
    public void inserir(ItemMesa itemMesa) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("INSERT INTO `item_mesa`(`id_item_mesa`, `ordem`, `id_produto`,"
                    + " `id_garcom`, `quantidade`, `valor`, `hora_lancamento`, `cancelado`  ) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            stmt.setInt(1, itemMesa.getId());
            stmt.setInt(2, itemMesa.getOrdem());
            stmt.setInt(3, itemMesa.getProduto().getId());
            stmt.setInt(4, itemMesa.getGarcom().getId());
            stmt.setDouble(5, itemMesa.getQuantidade());
            stmt.setDouble(6, itemMesa.getValor());

            DateFormat df = DateFormat.getTimeInstance();

            stmt.setString(7, df.format(itemMesa.getHoraLancamento().getTime()));
            stmt.setInt(8, itemMesa.getCancelado());
            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void alterar(ItemMesa itemMesa) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("UPDATE `item_mesa` SET `id_produto` = ?, `id_garcom` = ?,"
                    + " `quantidade` = ?,`valor` = ?, `hora_lancamento` = ?, `cancelado` = ? "
                    + " WHERE `id_item_mesa` = ? and `ordem` = ?");
            stmt.setInt(1, itemMesa.getProduto().getId());
            stmt.setInt(2, itemMesa.getGarcom().getId());
            stmt.setDouble(3, itemMesa.getQuantidade());
            stmt.setDouble(4, itemMesa.getValor());

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            stmt.setString(5, sdf.format(itemMesa.getHoraLancamento().getTime()));

            stmt.setInt(6, itemMesa.getCancelado());
            stmt.setInt(7, itemMesa.getId());
            stmt.setInt(8, itemMesa.getOrdem());
            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void excluir(ItemMesa itemMesa) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("DELETE FROM ITEM_MESA WHERE ID_ITEM_MESA = ? and ORDEM = ?");
            stmt.setInt(1, itemMesa.getId());
            stmt.setInt(2, itemMesa.getOrdem());
            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    public ArrayList<ItemMesa> buscarPorMesa(Mesa mesa) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<ItemMesa> itensMesa = new ArrayList<>();
        try {
            stmt = conexao.prepareStatement("SELECT `id_item_mesa`, `ordem`, `id_produto`, `id_garcom`, `quantidade`, `valor`,"
                    + " `hora_lancamento`, `cancelado` FROM `item_mesa` WHERE "
                    + "`id_item_mesa` = ?");
            stmt.setInt(1, mesa.getId());
            result = stmt.executeQuery();

            while (result.next()) {
                ItemMesa itemMesa = new ItemMesa();
                itemMesa.setId(result.getInt("id_item_mesa"));
                itemMesa.setOrdem(result.getInt("ordem"));
                itemMesa.setQuantidade(result.getDouble("quantidade"));
                itemMesa.setValor(result.getDouble("valor"));
                itemMesa.setCancelado(result.getInt("cancelado"));

                Calendar calendarioDataHoraAbertura = Calendar.getInstance();
                calendarioDataHoraAbertura.setTime(result.getTimestamp("hora_lancamento"));
                itemMesa.setHoraLancamento(calendarioDataHoraAbertura);

                Produto produto = new Produto();
                produto.buscar(result.getInt("id_produto"));
                itemMesa.setProduto(produto);

                Garcom garcom = new Garcom();
                garcom.buscar(result.getInt("id_garcom"));
                itemMesa.setGarcom(garcom);

                itensMesa.add(itemMesa);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return itensMesa;
        }
    }

    @Override
    public void buscar(ItemMesa itemMesa) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = conexao.prepareStatement("SELECT `id_produto`, `id_garcom`, `quantidade`, `valor`,"
                    + " `hora_lancamento`, `cancelado` FROM `item_mesa` WHERE "
                    + "`id_item_mesa` = ? and `ordem` = ? ");
            stmt.setInt(1, itemMesa.getId());
            stmt.setInt(2, itemMesa.getOrdem());
            result = stmt.executeQuery();

            while (result.next()) {
                itemMesa.setQuantidade(result.getDouble("quantidade"));
                itemMesa.setValor(result.getDouble("valor"));
                itemMesa.setCancelado(result.getInt("cancelado"));

                if (!result.getString("hora_lancamento").equals("00:00:00")) {
                    Calendar calendarioDataHoraFechamento = Calendar.getInstance();
                    calendarioDataHoraFechamento.setTime(result.getTimestamp("hora_lancamento"));
                    itemMesa.setHoraLancamento(calendarioDataHoraFechamento);
                }

                Produto produto = new Produto();
                produto.buscar(result.getInt("id_produto"));
                itemMesa.setProduto(produto);

                Garcom garcom = new Garcom();
                garcom.buscar(result.getInt("id_garcom"));
                itemMesa.setGarcom(garcom);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
        }
    }

    @Override
    public ArrayList<ItemMesa> buscarTodos() throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
