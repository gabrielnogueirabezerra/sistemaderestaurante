package DAO;

import Conexao.Conexao;
import Models.Mesa;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Beatriz Oliveiira
 */
public class MesaDAO extends DAO<Mesa> {

    private Conexao dao = Conexao.getInstanciaDaConexao();
    private static MesaDAO instancia;

    public static MesaDAO getInstancia() {
        if (instancia == null) {
            instancia = new MesaDAO();
        }

        return instancia;
    }

    @Override
    public void inserir(Mesa mesa) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("INSERT INTO `mesa`(`id_mesa`, `numero`, `descricao`, `total_pessoas`,"
                    + " `data_hora_abertura`, `data_hora_fechamento`, `data_hora_cancelamento`, `status`)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            stmt.setInt(1, mesa.getId());
            stmt.setInt(2, mesa.getNumero());
            stmt.setString(3, mesa.getDescricao());
            stmt.setInt(4, mesa.getTotalPessoas());

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String s = sdf.format(mesa.getDataHoraAbertura().getTime());

            stmt.setString(5, sdf.format(mesa.getDataHoraAbertura().getTime()));
            if (mesa.getDataHoraFechamento() != null) {
                stmt.setString(6, sdf.format(mesa.getDataHoraFechamento().getTime()));
            } else {
                stmt.setString(6, "0000-00-00 00:00:00");
            }
            if (mesa.getDataHoraCancelamento() != null) {
                stmt.setString(7, sdf.format(mesa.getDataHoraCancelamento().getTime()));
            } else {
                stmt.setString(7, "0000-00-00 00:00:00");
            }
            stmt.setInt(8, mesa.getStatus());

            stmt.executeUpdate();

            mesa.setId(this.find());
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void alterar(Mesa mesa) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("UPDATE `mesa` SET `numero` = ?, `descricao` = ?, `total_pessoas` = ?,"
                    + " `data_hora_abertura` = ?, `data_hora_fechamento` = ?, `data_hora_cancelamento` = ?,"
                    + " `status` = ? WHERE `id_mesa` = ?");

            stmt.setInt(1, mesa.getNumero());
            stmt.setString(2, mesa.getDescricao());
            stmt.setInt(3, mesa.getTotalPessoas());

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            stmt.setString(4, sdf.format(mesa.getDataHoraAbertura().getTime()));
            if (mesa.getDataHoraFechamento() != null) {
                stmt.setString(5, sdf.format(mesa.getDataHoraFechamento().getTime()));
            } else {
                stmt.setString(5, "0000-00-00 00:00:00");
            }
            if (mesa.getDataHoraCancelamento() != null) {
                stmt.setString(6, sdf.format(mesa.getDataHoraCancelamento().getTime()));
            } else {
                stmt.setString(6, "0000-00-00 00:00:00");
            }

            stmt.setInt(7, mesa.getStatus());
            stmt.setInt(8, mesa.getId());

            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void excluir(Mesa mesa) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("DELETE FROM MESA WHERE ID_MESA = ?");
            stmt.setInt(1, mesa.getId());
            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void buscar(Mesa mesa) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = conexao.prepareStatement("SELECT `numero`, `descricao`, `total_pessoas`, `data_hora_abertura`,"
                    + " `data_hora_fechamento`, `data_hora_cancelamento`, `status` FROM `mesa` WHERE `id_mesa` = ?");
            stmt.setInt(1, mesa.getId());
            result = stmt.executeQuery();

            while (result.next()) {
                mesa.setNumero(result.getInt("numero"));
                mesa.setDescricao(result.getString("descricao"));
                mesa.setTotalPessoas(result.getInt("total_pessoas"));
                mesa.setStatus(result.getInt("status"));

                Calendar calendarioDataHoraAbertura = Calendar.getInstance();
                calendarioDataHoraAbertura.setTime(result.getTimestamp("data_hora_abertura"));
                mesa.setDataHoraAbertura(calendarioDataHoraAbertura);

                if (!result.getString("data_hora_fechamento").equals("0000-00-00 00:00:00")) {
                    Calendar calendarioDataHoraFechamento = Calendar.getInstance();
                    calendarioDataHoraFechamento.setTime(result.getTimestamp("data_hora_fechamento"));
                    mesa.setDataHoraFechamento(calendarioDataHoraFechamento);
                }

                if (!result.getString("data_hora_cancelamento").equals("0000-00-00 00:00:00")) {
                    Calendar calendarioDataHoraCancelamento = Calendar.getInstance();
                    calendarioDataHoraCancelamento.setTime(result.getTimestamp("data_hora_cancelamento"));
                    mesa.setDataHoraCancelamento(calendarioDataHoraCancelamento);
                }

                mesa.setItensMesa(ItemMesaDAO.getInstancia().buscarPorMesa(mesa));

            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
        }
    }

    private int find() throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        int resultado = 0;

        try {
            stmt = conexao.prepareStatement("SELECT AUTO_INCREMENT as id FROM information_schema.tables WHERE "
                    + "table_name = 'mesa' AND table_schema = 'banco_restaurante'");
            result = stmt.executeQuery();

            while (result.next()) {
                resultado = result.getInt("id");
            }

        } finally {
            Conexao.fecharConexao(conexao, stmt);
            return resultado - 1;
        }
    }

    public void buscaPorNumero(Mesa mesa) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = conexao.prepareStatement("SELECT `id_mesa`, `numero`, `descricao`, `total_pessoas`, `data_hora_abertura`,"
                    + " `data_hora_fechamento`, `data_hora_cancelamento`, `status` FROM `mesa` WHERE `numero` = ? and `status` = 0");
            stmt.setInt(1, mesa.getNumero());
            result = stmt.executeQuery();

            while (result.next()) {
                mesa.setId(result.getInt("id_mesa"));
                mesa.setNumero(result.getInt("numero"));
                mesa.setDescricao(result.getString("descricao"));
                mesa.setTotalPessoas(result.getInt("total_pessoas"));
                mesa.setStatus(result.getInt("status"));

                Calendar calendarioDataHoraAbertura = Calendar.getInstance();
                calendarioDataHoraAbertura.setTime(result.getTimestamp("data_hora_abertura"));
                mesa.setDataHoraAbertura(calendarioDataHoraAbertura);

                if (!result.getString("data_hora_fechamento").equals("0000-00-00 00:00:00")) {
                    Calendar calendarioDataHoraFechamento = Calendar.getInstance();
                    calendarioDataHoraFechamento.setTime(result.getTimestamp("data_hora_fechamento"));
                    mesa.setDataHoraFechamento(calendarioDataHoraFechamento);
                }

                if (!result.getString("data_hora_cancelamento").equals("0000-00-00 00:00:00")) {
                    Calendar calendarioDataHoraCancelamento = Calendar.getInstance();
                    calendarioDataHoraCancelamento.setTime(result.getTimestamp("data_hora_cancelamento"));
                    mesa.setDataHoraCancelamento(calendarioDataHoraCancelamento);
                }

                mesa.setItensMesa(ItemMesaDAO.getInstancia().buscarPorMesa(mesa));
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
        }
    }

    public ArrayList<Mesa> buscar(boolean abertas, boolean canceladas, boolean fechadas) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<Mesa> mesas = new ArrayList<>();
        try {

            String status;

            if (abertas && fechadas && canceladas) {
                status = "0,1,2";
            } else if (abertas && fechadas && !canceladas) {
                status = "0,1";
            } else if (abertas && !fechadas && !canceladas) {
                status = "0";
            } else if (abertas && !fechadas && canceladas) {
                status = "0,2";
            } else if (!abertas && fechadas && canceladas) {
                status = "1,2";
            } else if (!abertas && !fechadas && canceladas) {
                status = "2";
            } else if (!abertas && fechadas && !canceladas) {
                status = "1";
            } else {
                status = "-1";
            }

            stmt = conexao.prepareStatement("SELECT `id_mesa`, `numero`, `descricao`, `total_pessoas`, `data_hora_abertura`,"
                    + " `data_hora_fechamento`, `data_hora_cancelamento`, `status` FROM `mesa` WHERE  `status` in (" + status + ")");

            result = stmt.executeQuery();

            while (result.next()) {
                Mesa mesa = new Mesa();
                mesa.setId(result.getInt("id_mesa"));
                mesa.setNumero(result.getInt("numero"));
                mesa.setDescricao(result.getString("descricao"));
                mesa.setTotalPessoas(result.getInt("total_pessoas"));
                mesa.setStatus(result.getInt("status"));

                Calendar calendarioDataHoraAbertura = Calendar.getInstance();
                calendarioDataHoraAbertura.setTime(result.getTimestamp("data_hora_abertura"));
                mesa.setDataHoraAbertura(calendarioDataHoraAbertura);

                if (!result.getString("data_hora_fechamento").equals("0000-00-00 00:00:00")) {
                    Calendar calendarioDataHoraFechamento = Calendar.getInstance();
                    calendarioDataHoraFechamento.setTime(result.getTimestamp("data_hora_fechamento"));
                    mesa.setDataHoraFechamento(calendarioDataHoraFechamento);
                }

                if (!result.getString("data_hora_cancelamento").equals("0000-00-00 00:00:00")) {
                    Calendar calendarioDataHoraCancelamento = Calendar.getInstance();
                    calendarioDataHoraCancelamento.setTime(result.getTimestamp("data_hora_cancelamento"));
                    mesa.setDataHoraCancelamento(calendarioDataHoraCancelamento);
                }

                mesa.setItensMesa(ItemMesaDAO.getInstancia().buscarPorMesa(mesa));

                mesas.add(mesa);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return mesas;
        }
    }

    public ArrayList<Mesa> buscar(boolean abertas, boolean canceladas, boolean fechadas, String dataInicial, String dataFinal) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<Mesa> mesas = new ArrayList<>();
        try {

            String sql = "SELECT `id_mesa`, `numero`, `descricao`, `total_pessoas`, `data_hora_abertura`,"
                    + " `data_hora_fechamento`, `data_hora_cancelamento`, `status` FROM `mesa` WHERE  (`status` = -1";

            if (abertas) {
                sql += " or (`status` = 0) ";
            }

            if (canceladas) {
                sql += " or (`data_hora_cancelamento` between ? and ? and `status` = 2)";
            }

            if (fechadas) {
                sql += " or (`data_hora_fechamento` between ? and ? and `status` = 1)";
            }

            sql += ") order by `id_mesa`";
            stmt = conexao.prepareStatement(sql);

            if (fechadas && canceladas) {
                stmt.setString(1, dataInicial);
                stmt.setString(2, dataFinal);
                stmt.setString(3, dataInicial);
                stmt.setString(4, dataFinal);
            }

            if (!fechadas && canceladas) {
                stmt.setString(1, dataInicial);
                stmt.setString(2, dataFinal);
            }

            if (fechadas && !canceladas) {
                stmt.setString(1, dataInicial);
                stmt.setString(2, dataFinal);
            }

            result = stmt.executeQuery();

            while (result.next()) {
                Mesa mesa = new Mesa();
                mesa.setId(result.getInt("id_mesa"));
                mesa.setNumero(result.getInt("numero"));
                mesa.setDescricao(result.getString("descricao"));
                mesa.setTotalPessoas(result.getInt("total_pessoas"));
                mesa.setStatus(result.getInt("status"));

                Calendar calendarioDataHoraAbertura = Calendar.getInstance();
                calendarioDataHoraAbertura.setTime(result.getTimestamp("data_hora_abertura"));
                mesa.setDataHoraAbertura(calendarioDataHoraAbertura);

                if (!result.getString("data_hora_fechamento").equals("0000-00-00 00:00:00")) {
                    Calendar calendarioDataHoraFechamento = Calendar.getInstance();
                    calendarioDataHoraFechamento.setTime(result.getTimestamp("data_hora_fechamento"));
                    mesa.setDataHoraFechamento(calendarioDataHoraFechamento);
                }

                if (!result.getString("data_hora_cancelamento").equals("0000-00-00 00:00:00")) {
                    Calendar calendarioDataHoraCancelamento = Calendar.getInstance();
                    calendarioDataHoraCancelamento.setTime(result.getTimestamp("data_hora_cancelamento"));
                    mesa.setDataHoraCancelamento(calendarioDataHoraCancelamento);
                }

                mesa.setItensMesa(ItemMesaDAO.getInstancia().buscarPorMesa(mesa));

                mesas.add(mesa);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return mesas;
        }
    }

    @Override
    public ArrayList<Mesa> buscarTodos() throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
