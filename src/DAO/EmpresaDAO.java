package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;

import Conexao.Conexao;
import Models.Empresa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

/**
 *
 * @author Beatriz Oliveiira
 */
public class EmpresaDAO extends DAO<Empresa> {

    private Conexao dao = Conexao.getInstanciaDaConexao();
    private static EmpresaDAO instancia;

    public static EmpresaDAO getInstancia() {
        if (instancia == null) {
            instancia = new EmpresaDAO();
        }
        return instancia;
    }

    @Override
    public void inserir(Empresa empresa) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("INSERT INTO `empresa`(`id_empresa`, `razao_social`, `cnpj`, `nome_fantasia`, `endereco`, `numero`, `email`, `telefone`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            stmt.setInt(1, empresa.getId());
            stmt.setString(2, empresa.getRazaoSocial());
            stmt.setString(3, empresa.getCNPJ());
            stmt.setString(4, empresa.getNomeFantasia());
            stmt.setString(5, empresa.getEndereco());
            stmt.setString(6, empresa.getNumero());
            stmt.setString(7, empresa.getEmail());
            stmt.setString(8, empresa.getTelefone());

            stmt.executeUpdate();

            empresa.setId(this.find());
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void alterar(Empresa empresa) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("UPDATE `empresa` SET `razao_social` = ?, `cnpj` = ?, "
                    + "`nome_fantasia` = ?, `endereco` = ?, `numero` = ?, `email` = ?, `telefone` = ? "
                    + "WHERE `id_empresa` = ?");
            stmt.setString(1, empresa.getRazaoSocial());
            stmt.setString(2, empresa.getCNPJ());
            stmt.setString(3, empresa.getNomeFantasia());
            stmt.setString(4, empresa.getEndereco());
            stmt.setString(5, empresa.getNumero());
            stmt.setString(6, empresa.getEmail());
            stmt.setString(7, empresa.getTelefone());
            stmt.setInt(8, empresa.getId());

            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void buscar(Empresa empresa) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = conexao.prepareStatement("SELECT `razao_social`, `cnpj`, `nome_fantasia`, `endereco`, "
                    + "`numero`, `email`, `telefone` FROM `empresa` WHERE `id_empresa` = ?");
            stmt.setInt(1, empresa.getId());
            result = stmt.executeQuery();

            while (result.next()) {
                empresa.setRazaoSocial(result.getString("razao_social"));
                empresa.setCNPJ(result.getString("cnpj"));
                empresa.setNomeFantasia(result.getString("nome_fantasia"));
                empresa.setEndereco(result.getString("endereco"));
                empresa.setNumero(result.getString("numero"));
                empresa.setEmail(result.getString("email"));
                empresa.setTelefone(result.getString("telefone"));
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
        }
    }

    @Override
    public ArrayList<Empresa> buscarTodos() throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<Empresa> empresas = new ArrayList<>();

        try {
            stmt = conexao.prepareStatement("SELECT `id_empresa`, `razao_social`, `nome_fantasia`, `cnpj`, `cnpj`, `endereco`, `numero`, `email`, `telefone` FROM `empresa` order by id_empresa");
            result = stmt.executeQuery();

            while (result.next()) {
                Empresa empresa = new Empresa();
                empresa.setId(result.getInt("id_empresa"));
                empresa.setNomeFantasia(result.getString("razao_social"));
                empresa.setNomeFantasia(result.getString("nome_fantasia"));
                empresa.setNomeFantasia(result.getString("cnpj"));
                empresa.setEndereco(result.getString("endereco"));
                empresa.setNumero(result.getString("numero"));
                empresa.setEmail(result.getString("email"));
                empresa.setTelefone(result.getString("telefone"));

                empresas.add(empresa);
            }

        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return empresas;
        }
    }

    public ArrayList<Empresa> buscaTodosPorNomeFantasia(String nomeFantasia) {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<Empresa> empresas = new ArrayList<>();
        try {
            stmt = conexao.prepareStatement("SELECT * FROM EMPRESA where nome_fantasia like ? ORDER BY ID_EMPRESA");
            stmt.setString(1, "%" + nomeFantasia + "%");
            result = stmt.executeQuery();

            while (result.next()) {
                Empresa empresa = new Empresa();
                empresa.setId(result.getInt("id_empresa"));
                empresa.setRazaoSocial(result.getString("razao_social"));
                empresa.setNomeFantasia(result.getString("nome_fantasia"));
                empresa.setCNPJ(result.getString("cnpj"));
                empresa.setEndereco(result.getString("endereco"));
                empresa.setNumero(result.getString("numero"));
                empresa.setEmail(result.getString("email"));
                empresa.setTelefone(result.getString("telefone"));

                empresas.add(empresa);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return empresas;
        }
    }

    @Override
    public void excluir(Empresa empresa) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;

        try {
            stmt = conexao.prepareStatement("DELETE FROM EMPRESA WHERE `id_empresa` = ?");
            stmt.setInt(1, empresa.getId());

            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    private int find() throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        int resultado = 0;

        try {
            stmt = conexao.prepareStatement("SELECT AUTO_INCREMENT as id FROM information_schema.tables "
                    + "WHERE table_name = 'empresa' AND table_schema = 'banco_restaurante'");
            result = stmt.executeQuery();

            while (result.next()) {
                resultado = result.getInt("id");
            }

        } finally {
            Conexao.fecharConexao(conexao, stmt);
            return resultado - 1;
        }
    }

}
