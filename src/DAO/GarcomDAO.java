package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;

import Conexao.Conexao;
import Models.Garcom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

/**
 *
 * @author Beatriz Oliveiira
 */
public class GarcomDAO extends DAO<Garcom> {

    private Conexao dao = Conexao.getInstanciaDaConexao();
    private static GarcomDAO instancia;

    public static GarcomDAO getInstancia() {
        if (instancia == null) {
            instancia = new GarcomDAO();
        }

        return instancia;
    }

    @Override
    public void inserir(Garcom garcom) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("INSERT INTO `garcom`(`id_garcom`, "
                    + "`nome`) VALUES (?, ?)");
            stmt.setInt(1, garcom.getId());
            stmt.setString(2, garcom.getNome());

            stmt.executeUpdate();

            garcom.setId(this.find());
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void alterar(Garcom garcom) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("UPDATE `garcom` SET `nome` = ? WHERE `id_garcom` = ?");
            stmt.setString(1, garcom.getNome());
            stmt.setInt(2, garcom.getId());

            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void excluir(Garcom garcom) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("DELETE FROM GARCOM WHERE ID_GARCOM = ?");
            stmt.setInt(1, garcom.getId());
            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void buscar(Garcom garcom) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = conexao.prepareStatement("SELECT `nome` FROM `garcom` WHERE `id_garcom` = ?");
            stmt.setInt(1, garcom.getId());
            result = stmt.executeQuery();

            while (result.next()) {
                garcom.setNome(result.getString("nome"));
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
        }
    }

    public ArrayList<Garcom> buscaTodosPorNome(String nome) {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<Garcom> garcons = new ArrayList<>();
        try {
            stmt = conexao.prepareStatement("SELECT * FROM GARCOM where nome like ? ORDER BY ID_GARCOM");
            stmt.setString(1, "%" + nome + "%");
            result = stmt.executeQuery();

            while (result.next()) {
                Garcom garcom = new Garcom();
                garcom.setId(result.getInt("id_garcom"));
                garcom.setNome(result.getString("nome"));

                garcons.add(garcom);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return garcons;
        }
    }

    @Override
    public ArrayList<Garcom> buscarTodos() throws SQLException, ClassNotFoundException {

        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<Garcom> garcons = new ArrayList<>();
        try {
            stmt = conexao.prepareStatement("SELECT * FROM GARCOM ORDER BY ID_GARCOM");
            result = stmt.executeQuery();

            while (result.next()) {
                Garcom garcom = new Garcom();
                garcom.setId(result.getInt("id_garcom"));
                garcom.setNome(result.getString("nome"));

                garcons.add(garcom);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return garcons;
        }
    }

    private int find() throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        int resultado = 0;

        try {
            stmt = conexao.prepareStatement("SELECT AUTO_INCREMENT as id FROM information_schema.tables WHERE table_name = 'garcom' AND table_schema = 'banco_restaurante'");
            result = stmt.executeQuery();

            while (result.next()) {
                resultado = result.getInt("id");
            }

        } finally {
            Conexao.fecharConexao(conexao, stmt);
            return resultado - 1;
        }
    }

}
