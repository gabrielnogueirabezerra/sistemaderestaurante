package DAO;

import Conexao.Conexao;
import Models.ItemVenda;
import Models.Produto;
import Models.Venda;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Beatriz Oliveiira
 */
public class ItemVendaDAO extends DAO<ItemVenda> {

    private Conexao dao = Conexao.getInstanciaDaConexao();
    private static ItemVendaDAO instancia;

    public static ItemVendaDAO getInstancia() {
        if (instancia == null) {
            instancia = new ItemVendaDAO();
        }

        return instancia;
    }

    @Override
    public void inserir(ItemVenda itemVenda) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("INSERT INTO `item_venda`(`id_item_venda`, `ordem`, `id_produto`, "
                    + "`quantidade`, `valor` ) VALUES (?, ?, ?, ?, ?)");
            stmt.setInt(1, itemVenda.getId());
            stmt.setInt(2, itemVenda.getOrdem());
            stmt.setInt(3, itemVenda.getProduto().getId());
            stmt.setDouble(4, itemVenda.getQuantidade());
            stmt.setDouble(5, itemVenda.getValor());
            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void alterar(ItemVenda itemVenda) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("UPDATE `item_venda` SET `id_produto` = ?, `quantidade` = ?,`valor` = ? "
                    + " WHERE `id_item_venda` = ? and `ordem` = ?");
            stmt.setInt(1, itemVenda.getProduto().getId());
            stmt.setDouble(2, itemVenda.getQuantidade());
            stmt.setDouble(3, itemVenda.getValor());
            stmt.setInt(4, itemVenda.getId());
            stmt.setInt(5, itemVenda.getOrdem());
            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void excluir(ItemVenda itemVenda) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("DELETE FROM ITEM_VENDA WHERE ID_ITEM_VENDA = ? and ORDEM = ?");
            stmt.setInt(1, itemVenda.getId());
            stmt.setInt(2, itemVenda.getOrdem());
            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void buscar(ItemVenda itemVenda) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = conexao.prepareStatement("SELECT `id_produto`, `quantidade`, `valor` "
                    + "FROM `item_venda` WHERE `id_item_venda` = ?, `ordem` = ? ");
            stmt.setInt(1, itemVenda.getId());
            stmt.setInt(2, itemVenda.getOrdem());
            result = stmt.executeQuery();

            while (result.next()) {
                itemVenda.setQuantidade(result.getDouble("quantidade"));
                itemVenda.setValor(result.getDouble("valor"));

                Produto produto = new Produto();
                produto.buscar(result.getInt("id_produto"));
                itemVenda.setProduto(produto);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
        }
    }

    public ArrayList<ItemVenda> buscarPorVenda(Venda venda) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<ItemVenda> itensVenda = new ArrayList<>();
        try {
            stmt = conexao.prepareStatement("SELECT `id_produto`, `quantidade`, `valor`, `ordem` "
                    + "FROM `item_venda` WHERE `id_item_venda` = ?");
            stmt.setInt(1, venda.getId());
            result = stmt.executeQuery();

            while (result.next()) {
                ItemVenda itemVenda = new ItemVenda();
                itemVenda.setId(venda.getId());
                itemVenda.setOrdem(result.getInt("ordem"));
                itemVenda.setQuantidade(result.getDouble("quantidade"));
                itemVenda.setValor(result.getDouble("valor"));
                itemVenda.setOrdem(result.getInt("ordem"));

                Produto produto = new Produto();
                produto.buscar(result.getInt("id_produto"));
                itemVenda.setProduto(produto);

                itensVenda.add(itemVenda);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return itensVenda;
        }
    }

    @Override
    public ArrayList<ItemVenda> buscarTodos() throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
