package DAO;

import Conexao.Conexao;
import Models.FormaPagamento;
import Models.Venda;
import Models.VendaFormaPagamento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Beatriz Oliveiira
 */
public class VendaFormaPagamentoDAO extends DAO<VendaFormaPagamento> {

    private Conexao dao = Conexao.getInstanciaDaConexao();
    private static VendaFormaPagamentoDAO instancia;

    public static VendaFormaPagamentoDAO getInstancia() {
        if (instancia == null) {
            instancia = new VendaFormaPagamentoDAO();
        }

        return instancia;
    }

    @Override
    public void inserir(VendaFormaPagamento vendaFormaPagamento) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("INSERT INTO `venda_forma_pagamento`(`id_venda_forma_pagamento`, `id_venda`, "
                    + "`id_forma_pagamento`, `valor`) VALUES (?, ?, ?, ?)");
            stmt.setInt(1, vendaFormaPagamento.getId());
            stmt.setInt(2, vendaFormaPagamento.getVenda().getId());
            stmt.setInt(3, vendaFormaPagamento.getFormaPagamento().getId());
            stmt.setDouble(4, vendaFormaPagamento.getValor());
            stmt.executeUpdate();

            vendaFormaPagamento.setId(this.find());

        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void alterar(VendaFormaPagamento vendaFormaPagamento) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("UPDATE `venda_forma_pagamento` SET `id_venda = ?,`id_forma_pagamento` = ?, `valor` = ?"
                    + " WHERE `id_venda_forma_pagamento` = ?");
            stmt.setInt(1, vendaFormaPagamento.getVenda().getId());
            stmt.setInt(2, vendaFormaPagamento.getFormaPagamento().getId());
            stmt.setDouble(3, vendaFormaPagamento.getValor());
            stmt.setInt(4, vendaFormaPagamento.getId());
            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void excluir(VendaFormaPagamento vendaFormaPagamento) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("DELETE FROM VENDA_FORMA_PAGAMENTO WHERE ID_VENDA_FORMA_PAGAMENTO = ?");
            stmt.setInt(1, vendaFormaPagamento.getId());
            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void buscar(VendaFormaPagamento vendaFormaPagamento) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = conexao.prepareStatement("SELECT `id_venda`, `id_forma_pagamento`, `valor` "
                    + "FROM `venda_forma_pagamento` WHERE `id_venda_forma_pagamento` = ?");
            stmt.setInt(1, vendaFormaPagamento.getId());
            result = stmt.executeQuery();

            while (result.next()) {
                vendaFormaPagamento.setValor(result.getDouble("valor"));

                Venda venda = new Venda();
                venda.setId(result.getInt("id_venda"));
                vendaFormaPagamento.setVenda(venda);

                FormaPagamento formaPagamento = new FormaPagamento();
                formaPagamento.buscar(result.getInt("id_Forma_pagamento"));
                vendaFormaPagamento.setFormaPagamento(formaPagamento);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
        }
    }

    public ArrayList<VendaFormaPagamento> buscarPorVenda(Venda venda) {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<VendaFormaPagamento> vendasFormaPagamento = new ArrayList<>();
        try {
            stmt = conexao.prepareStatement("SELECT * FROM venda_forma_pagamento WHERE `id_venda` = ?");
            stmt.setInt(1, venda.getId());
            result = stmt.executeQuery();

            while (result.next()) {
                VendaFormaPagamento vendaFormaPagamento = new VendaFormaPagamento();
                vendaFormaPagamento.setValor(result.getDouble("valor"));

                vendaFormaPagamento.setVenda(venda);

                FormaPagamento formaPagamento = new FormaPagamento();
                formaPagamento.buscar(result.getInt("id_Forma_pagamento"));
                vendaFormaPagamento.setFormaPagamento(formaPagamento);

                vendasFormaPagamento.add(vendaFormaPagamento);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return vendasFormaPagamento;
        }
    }

    private int find() throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        int resultado = 0;

        try {
            stmt = conexao.prepareStatement("SELECT AUTO_INCREMENT as id FROM information_schema.tables WHERE "
                    + "table_name = 'venda_forma_pagamento' AND table_schema = 'banco_restaurante'");
            result = stmt.executeQuery();

            while (result.next()) {
                resultado = result.getInt("id");
            }

        } finally {
            Conexao.fecharConexao(conexao, stmt);
            return resultado - 1;
        }
    }

    @Override
    public ArrayList<VendaFormaPagamento> buscarTodos() throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
