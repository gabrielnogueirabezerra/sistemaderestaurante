package DAO;

import Conexao.Conexao;
import Models.FormaPagamento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Beatriz Oliveiira
 */
public class FormaPagamentoDAO extends DAO<FormaPagamento> {

    private Conexao dao = Conexao.getInstanciaDaConexao();
    private static FormaPagamentoDAO instancia;

    public static FormaPagamentoDAO getInstancia() {
        if (instancia == null) {
            instancia = new FormaPagamentoDAO();
        }

        return instancia;
    }

    @Override
    public void inserir(FormaPagamento formaPagamento) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("INSERT INTO `forma_pagamento`(`id_forma_pagamento`, "
                    + "`descricao`) VALUES (?, ?)");
            stmt.setInt(1, formaPagamento.getId());
            stmt.setString(2, formaPagamento.getDescricao());

            stmt.executeUpdate();

            formaPagamento.setId(this.find());
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void alterar(FormaPagamento formaPagamento) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("UPDATE `forma_pagamento` SET `descricao` = ?"
                    + " WHERE `id_forma_pagamento` = ?");
            stmt.setString(1, formaPagamento.getDescricao());
            stmt.setInt(2, formaPagamento.getId());

            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void excluir(FormaPagamento formaPagamento) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("DELETE FROM FORMA_PAGAMENTO WHERE ID_FORMA_PAGAMENTO = ?");
            stmt.setInt(1, formaPagamento.getId());
            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void buscar(FormaPagamento formaPagamento) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = conexao.prepareStatement("SELECT `descricao` FROM `forma_pagamento` "
                    + "WHERE `id_forma_pagamento` = ?");
            stmt.setInt(1, formaPagamento.getId());
            result = stmt.executeQuery();

            while (result.next()) {
                formaPagamento.setDescricao(result.getString("descricao"));

            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
        }
    }

    @Override
    public ArrayList<FormaPagamento> buscarTodos() throws SQLException, ClassNotFoundException {

        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<FormaPagamento> formasPagamentos = new ArrayList<>();
        try {
            stmt = conexao.prepareStatement("SELECT * FROM FORMA_PAGAMENTO ORDER BY ID_FORMA_PAGAMENTO");
            result = stmt.executeQuery();

            while (result.next()) {
                FormaPagamento pagamento = new FormaPagamento();
                pagamento.setId(result.getInt("id_forma_pagamento"));
                pagamento.setDescricao(result.getString("descricao"));

                formasPagamentos.add(pagamento);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return formasPagamentos;
        }
    }

    public ArrayList<FormaPagamento> buscaTodosPorDescricao(String descricao) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<FormaPagamento> formas = new ArrayList<>();
        try {
            stmt = conexao.prepareStatement("SELECT * FROM `forma_pagamento` WHERE `descricao` like ? order by `id_forma_pagamento`");
            stmt.setString(1, "%" + descricao + "%");
            result = stmt.executeQuery();

            while (result.next()) {
                FormaPagamento formaPagamento = new FormaPagamento();
                formaPagamento.setId(result.getInt("id_forma_pagamento"));
                formaPagamento.setDescricao(result.getString("descricao"));

                formas.add(formaPagamento);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return formas;
        }
    }

    private int find() throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        int resultado = 0;

        try {
            stmt = conexao.prepareStatement("SELECT AUTO_INCREMENT as id FROM information_schema.tables "
                    + "WHERE table_name = 'forma_pagamento' AND table_schema = 'banco_restaurante'");
            result = stmt.executeQuery();

            while (result.next()) {
                resultado = result.getInt("id");
            }

        } finally {
            Conexao.fecharConexao(conexao, stmt);
            return resultado - 1;
        }
    }

}
