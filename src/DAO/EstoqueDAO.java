package DAO;

import Conexao.Conexao;
import Models.Estoque;
import Models.Produto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Beatriz Oliveiira
 */
public class EstoqueDAO extends DAO<Estoque> {

    private Conexao dao = Conexao.getInstanciaDaConexao();
    private static EstoqueDAO instancia;

    public static EstoqueDAO getInstancia() {
        if (instancia == null) {
            instancia = new EstoqueDAO();
        }

        return instancia;
    }

    @Override
    public void inserir(Estoque estoque) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("INSERT INTO `estoque`(`id_estoque`, `id_produto`,"
                    + " `quantidade`) VALUES (?, ?, ?)");
            stmt.setInt(1, estoque.getId());
            stmt.setInt(2, estoque.getProduto().getId());
            stmt.setDouble(3, estoque.getQuantidade());
            stmt.executeUpdate();

            estoque.setId(this.find());
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void alterar(Estoque estoque) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("UPDATE `estoque` SET `id_produto` = ?, `quantidade` = ? "
                    + "WHERE `id_estoque` = ?");
            stmt.setInt(1, estoque.getProduto().getId());
            stmt.setDouble(2, estoque.getQuantidade());
            stmt.setInt(3, estoque.getId());
            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void excluir(Estoque estoque) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("DELETE FROM ESTOQUE WHERE ID_ESTOQUE = ?");
            stmt.setInt(1, estoque.getId());
            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void buscar(Estoque estoque) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = conexao.prepareStatement("SELECT `id_produto`, `quantidade` FROM `estoque` "
                    + "WHERE `id_estoque` = ?");
            stmt.setInt(1, estoque.getId());
            result = stmt.executeQuery();

            while (result.next()) {
                estoque.setQuantidade(result.getDouble("quantidade"));

                Produto produto = new Produto();
                produto.buscar(result.getInt("id_produto"));
                estoque.setProduto(produto);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
        }
    }

    private int find() throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        int resultado = 0;

        try {
            stmt = conexao.prepareStatement("SELECT AUTO_INCREMENT as id FROM "
                    + "information_schema.tables WHERE table_name = 'estoque'"
                    + " AND table_schema = 'banco_restaurante'");
            result = stmt.executeQuery();

            while (result.next()) {
                resultado = result.getInt("id");
            }

        } finally {
            Conexao.fecharConexao(conexao, stmt);
            return resultado - 1;
        }
    }

    @Override
    public ArrayList<Estoque> buscarTodos() throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
