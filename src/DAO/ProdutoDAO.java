package DAO;

import Conexao.Conexao;
import Models.Produto;
import Models.Secao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Beatriz Oliveiira
 */
public class ProdutoDAO extends DAO<Produto> {

    private Conexao dao = Conexao.getInstanciaDaConexao();
    private static ProdutoDAO instancia;

    public static ProdutoDAO getInstancia() {
        if (instancia == null) {
            instancia = new ProdutoDAO();
        }

        return instancia;
    }

    @Override
    public void inserir(Produto produto) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("INSERT INTO `produto`(`id_produto`, `id_secao`, `nome`, `valor`, "
                    + "`descricao`, `foto`) VALUES (?, ?, ?, ?, ?, ?)");
            stmt.setInt(1, produto.getId());
            stmt.setInt(2, produto.getSecao().getId());
            stmt.setString(3, produto.getNome());
            stmt.setDouble(4, produto.getValor());
            stmt.setString(5, produto.getDescricao());
            stmt.setString(6, produto.getFoto());

            stmt.executeUpdate();

            produto.setId(this.find());
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void alterar(Produto produto) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("UPDATE `produto` SET `id_secao` = ?,`nome` = ?, `valor` = ?, "
                    + "`descricao` = ?, `foto` = ? WHERE `id_produto` = ?");
            stmt.setInt(1, produto.getSecao().getId());
            stmt.setString(2, produto.getNome());
            stmt.setDouble(3, produto.getValor());
            stmt.setString(4, produto.getDescricao());
            stmt.setString(5, produto.getFoto());
            stmt.setInt(6, produto.getId());
            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void excluir(Produto produto) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        try {
            stmt = conexao.prepareStatement("DELETE FROM PRODUTO WHERE ID_PRODUTO = ?");
            stmt.setInt(1, produto.getId());
            stmt.executeUpdate();
        } finally {
            Conexao.fecharConexao(conexao, stmt);
        }
    }

    @Override
    public void buscar(Produto produto) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = conexao.prepareStatement("SELECT `id_secao`, `nome`, `valor`, `descricao`, `foto`"
                    + " FROM `produto` WHERE `id_produto` = ?");
            stmt.setInt(1, produto.getId());
            result = stmt.executeQuery();

            while (result.next()) {
                produto.setNome(result.getString("nome"));
                produto.setValor(result.getDouble("valor"));
                produto.setDescricao(result.getString("descricao"));
                produto.setFoto(result.getString("foto"));

                Secao secao = new Secao();
                secao.buscar(result.getInt("id_secao"));
                produto.setSecao(secao);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
        }
    }

    public ArrayList<Produto> buscaTodos(Secao secao) throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<Produto> produtos = new ArrayList<>();
        try {
            stmt = conexao.prepareStatement("SELECT `id_produto`, `id_secao`, `nome`, `valor`,"
                    + " `descricao`, `foto`  FROM `produto` where id_secao = ?");
            stmt.setInt(1, secao.getId());
            result = stmt.executeQuery();

            while (result.next()) {
                Produto produto = new Produto();
                produto.setId(result.getInt("id_produto"));
                produto.setNome(result.getString("nome"));
                produto.setValor(result.getDouble("valor"));
                produto.setDescricao(result.getString("descricao"));
                produto.setFoto(result.getString("foto"));

                SecaoDAO.getInstancia().buscar(secao);

                produto.setSecao(secao);
                produtos.add(produto);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return produtos;
        }
    }

    @Override
    public ArrayList<Produto> buscarTodos() throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<Produto> produtos = new ArrayList<>();
        try {
            stmt = conexao.prepareStatement("SELECT `id_produto`, `id_secao`, `nome`, `valor`,"
                    + " `descricao`, `foto`  FROM `produto` ");
            result = stmt.executeQuery();

            while (result.next()) {
                Produto produto = new Produto();
                produto.setId(result.getInt("id_produto"));
                produto.setNome(result.getString("nome"));
                produto.setValor(result.getDouble("valor"));
                produto.setDescricao(result.getString("descricao"));
                produto.setFoto(result.getString("foto"));

                Secao secao = new Secao();
                secao.buscar(result.getInt("id_secao"));

                produto.setSecao(secao);
                produtos.add(produto);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return produtos;
        }
    }

    public ArrayList<Produto> buscaTodosPorNome(String nome) {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        ArrayList<Produto> produtos = new ArrayList<>();
        try {
            stmt = conexao.prepareStatement("SELECT * FROM PRODUTO where nome like ? ORDER BY ID_PRODUTO");
            stmt.setString(1, "%" + nome + "%");
            result = stmt.executeQuery();

            while (result.next()) {
                Produto produto = new Produto();
                produto.setId(result.getInt("id_produto"));
                produto.setNome(result.getString("nome"));
                produto.setValor(result.getDouble("valor"));
                produto.setDescricao(result.getString("descricao"));
                produto.setFoto(result.getString("foto"));

                Secao secao = new Secao();
                secao.buscar(result.getInt("id_secao"));

                produto.setSecao(secao);
                produtos.add(produto);
            }
        } finally {
            Conexao.fecharConexao(conexao, stmt, result);
            return produtos;
        }

    }

    private int find() throws SQLException, ClassNotFoundException {
        Connection conexao = dao.getConexao();
        PreparedStatement stmt = null;
        ResultSet result = null;
        int resultado = 0;

        try {
            stmt = conexao.prepareStatement("SELECT AUTO_INCREMENT as id FROM "
                    + "information_schema.tables WHERE table_name = 'produto' AND table_schema = 'banco_restaurante'");
            result = stmt.executeQuery();

            while (result.next()) {
                resultado = result.getInt("id");
            }

        } finally {
            Conexao.fecharConexao(conexao, stmt);
            return resultado - 1;
        }
    }

}
