package Controllers;

import Models.Configuracao;
import Models.Mesa;
import Views.FrmMesas;
import java.awt.Color;
import java.awt.Component;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class FrmMesasController extends Controller {

    public FrmMesasController(FrmMesas view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;
            this.model.incluir(this);
        }
    }

    @Override
    public void evento(String evento) {
        if (evento.equals("FormClose")) {
            this.model.excluir(this);

            this.evento("btnBuscarMesas");
        }

        if (evento.equals("FormShow")) {

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            this.getView().getEdtDataIni().setText(sdf.format(Calendar.getInstance().getTime()));
            this.getView().getEdtDataFin().setText(sdf.format(Calendar.getInstance().getTime()));

            this.evento("btnBuscarMesas");
        }

        if (evento.equals("btnNovaMesa")) {
            try {
                this.model.abreTela("FrmAbreMesa");
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                this.getView().mostraMensagem("Não foi possível abrir a tela de abertura de mesas. Mensagem retornada: " + ex.getMessage());
            }

            this.evento("btnBuscarMesas");
        }

        if (evento.equals("btnBuscarMesas")) {
            try {
                this.model.buscarMesas(
                        this.getView().getCheckAbertas().isSelected(),
                        this.getView().getCheckCanceladas().isSelected(),
                        this.getView().getCheckFechadas().isSelected(), this.getView().getEdtDataIni().getText(), this.getView().getEdtDataFin().getText());
            } catch (SQLException | ClassNotFoundException ex) {
                this.getView().mostraMensagem("Não foi possível buscar as mesas. Mensagem retornada: " + ex.getMessage());
            }

            this.model.avisarObservers();
        }

        if (evento.equals("btnCancelarMesa")) {

            int linhaRegisto = this.getView().getTableMesas().getSelectedRow();

            try {

                if (this.getView().getTableMesas().getValueAt(linhaRegisto, 6).toString().equals("") && this.getView().getTableMesas().getValueAt(linhaRegisto, 7).toString().equals("")) {
                    this.model.cancelarMesa(Integer.parseInt(this.getView().getTableMesas().getValueAt(linhaRegisto, 0).toString()));
                    this.evento("btnBuscarMesas");
                    return;
                } else {
                    if (!this.getView().getTableMesas().getValueAt(linhaRegisto, 6).toString().equals("")) {
                        this.getView().mostraMensagem("Essa mesa já foi fechada.");
                    } else {
                        this.getView().mostraMensagem("Essa mesa já foi cancelada.");
                    }
                }

            } catch (ClassNotFoundException | SQLException ex) {
                this.getView().mostraMensagem("Não foi possível cancelar a mesa. Mensagem retornada: " + ex.getMessage());
            }

            this.evento("btnBuscarMesas");
        }

        if (evento.equals("btnImprimirComprovanteVenda")) {

            int linhaRegisto = this.getView().getTableMesas().getSelectedRow();

            try {

                if (!this.getView().getTableMesas().getValueAt(linhaRegisto, 6).toString().equals("")) {
                    this.model.buscaVenda(Integer.parseInt(this.getView().getTableMesas().getValueAt(linhaRegisto, 0).toString()));
                    this.model.imprimirVenda();
                } else {
                    if (this.getView().getTableMesas().getValueAt(linhaRegisto, 6).toString().equals("")) {
                        this.getView().mostraMensagem("Essa mesa não foi fechada.");
                    } else if (!this.getView().getTableMesas().getValueAt(linhaRegisto, 7).toString().equals("")) {
                        this.getView().mostraMensagem("Essa mesa foi cancelada.");
                    }
                }

            } catch (Exception ex) {
                this.getView().mostraMensagem("Não foi possível imprimir o comprovante. Mensagem retornada: " + ex.getMessage());
            }

            this.evento("btnBuscarMesas");
        }

        if (evento.equals("btnGerenciarMesa")) {

            int linhaRegisto = this.getView().getTableMesas().getSelectedRow();

            try {
                if (this.getView().getTableMesas().getValueAt(linhaRegisto, 6).toString().equals("") && this.getView().getTableMesas().getValueAt(linhaRegisto, 7).toString().equals("")) {
                    this.model.setMesaSelecionada(this.model.buscaMesa(Integer.parseInt(this.getView().getTableMesas().getValueAt(linhaRegisto, 1).toString())));
                    this.model.abreTela("FrmGerenciamentoMesa");
                } else {
                    if (!this.getView().getTableMesas().getValueAt(linhaRegisto, 6).toString().equals("")) {
                        this.getView().mostraMensagem("Essa mesa já foi fechada.");
                    } else {
                        this.getView().mostraMensagem("Essa mesa já foi cancelada.");
                    }
                }
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException | SQLException | ClassNotFoundException ex) {
                this.getView().mostraMensagem("Não foi possível abrir a tela de gerenciamento de mesa. Mensagem retornada: " + ex.getMessage());
            }

            this.evento("btnBuscarMesas");
        }

        if (evento.equals("btnConcluirMesa")) {
            int linhaRegisto = this.getView().getTableMesas().getSelectedRow();

            try {

                if (this.getView().getTableMesas().getValueAt(linhaRegisto, 6).toString().equals("") && this.getView().getTableMesas().getValueAt(linhaRegisto, 7).toString().equals("")) {
                    this.model.setMesaSelecionada(this.model.buscaMesa(Integer.parseInt(this.getView().getTableMesas().getValueAt(linhaRegisto, 1).toString())));
                    if (this.model.getMesaSelecionada().total() > 0) {
                        this.model.abreTela("FrmConcluir");
                    } else {
                        this.model.setMesaSelecionada(null);
                        this.getView().mostraMensagem("Essa mesa não possui nenhum item lançado.");
                    }
                } else {
                    if (!this.getView().getTableMesas().getValueAt(linhaRegisto, 6).toString().equals("")) {
                        this.getView().mostraMensagem("Essa mesa já foi fechada.");
                    } else {
                        this.getView().mostraMensagem("Essa mesa já foi cancelada.");
                    }
                }

            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException | SQLException | ClassNotFoundException ex) {
                this.getView().mostraMensagem("Não foi possível abrir a tela de gerenciamento de mesa. Mensagem retornada: " + ex.getMessage());
            }

            this.evento("btnBuscarMesas");
        }

    }

    @Override
    public void alterar() {
        ArrayList<Mesa> mesas = this.model.getMesas();
        this.getView().limpaTableMesas();
        if (mesas != null) {

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

            String dataAbertura = "";
            String dataFechamento = "";
            String dataCancelamento = "";

            this.getView().getTableMesas().setDefaultRenderer(Object.class,
                    new DefaultTableCellRenderer() {
                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                    if (table.getValueAt(row, 7) != "") {
                        c.setForeground(Color.red);
                    } else if (table.getValueAt(row, 6) != "") {
                        c.setForeground(Color.lightGray);
                    } else if (table.getValueAt(row, 7) == "" && table.getValueAt(row, 6) == "") {
                        c.setForeground(Color.black);
                    }
                    return c;
                }
            });

            for (Mesa mesa : mesas) {

                if (mesa.getDataHoraAbertura() != null) {
                    dataAbertura = sdf.format(mesa.getDataHoraAbertura().getTime());
                } else {
                    dataAbertura = "";
                }

                if (mesa.getDataHoraFechamento() != null) {
                    dataFechamento = sdf.format(mesa.getDataHoraFechamento().getTime());
                } else {
                    dataFechamento = "";
                }

                if (mesa.getDataHoraCancelamento() != null) {
                    dataCancelamento = sdf.format(mesa.getDataHoraCancelamento().getTime());
                } else {
                    dataCancelamento = "";
                }

                String[] novaLinha = {
                    String.valueOf(mesa.getId()),
                    String.valueOf(mesa.getNumero()),
                    String.valueOf(mesa.total()),
                    mesa.getDescricao(),
                    String.valueOf(mesa.getTotalPessoas()),
                    dataAbertura,
                    dataFechamento,
                    dataCancelamento
                };
                ((DefaultTableModel) this.getView().getTableMesas().getModel()).addRow(novaLinha);

            }
        }

        this.model.setMesas(
                new ArrayList<>());
    }

    public FrmMesas getView() {
        return (FrmMesas) this.view;
    }

}
