package Controllers;

import Models.Configuracao;
import Models.InterfaceObserver;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public abstract class Controller implements InterfaceObserver {

    public Configuracao model;
    public Object view;

    abstract void evento(String evento);

}
