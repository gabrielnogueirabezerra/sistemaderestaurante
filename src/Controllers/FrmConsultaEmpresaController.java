package Controllers;

import Models.Configuracao;
import Models.Empresa;
import Views.FrmConsultaEmpresa;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Beatriz Oliveiira
 */
public class FrmConsultaEmpresaController extends Controller {

    public FrmConsultaEmpresaController(FrmConsultaEmpresa view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;
            this.model.incluir(this);
        }
    }

    public void evento(KeyEvent evt) {
        if (this.getView().getCbPesquisa().getSelectedIndex() == 0) {
            String caracteres = "0987654321";
            if (!caracteres.contains(evt.getKeyChar() + "")) {
                evt.consume();
            }
        }
    }

    public FrmConsultaEmpresa getView() {
        return (FrmConsultaEmpresa) this.view;
    }

    @Override
    public void alterar() {
        ArrayList<Empresa> empresas = this.model.getEmpresas();
        this.getView().limpaTableEmpresas();
        if (empresas != null) {

            for (Empresa empresa : empresas) {
                String[] novaLinha = {
                    String.valueOf(empresa.getId()),
                    empresa.getNomeFantasia(),
                    empresa.getCNPJ()};
                ((DefaultTableModel) this.getView().getTableEmpresa().getModel()).addRow(novaLinha);
            }
        }

        this.model.setEmpresas(new ArrayList<Empresa>());
    }

    @Override
    public void evento(String evento) {
        if (evento.equals("MudarPesquisa")) {

            this.getView().getEdtPesquisa().setText("");

            switch (this.getView().getCbPesquisa().getSelectedIndex()) {
                case 0:
                    this.getView().getLblPesquisa().setText("Pesquisar por Código");
                    break;
                case 1:
                    this.getView().getLblPesquisa().setText("Pesquisar por Nome fantasia");
                    break;
                default:
                    this.getView().getLblPesquisa().setText("Pesquisar por Código");
                    break;
            }
        }

        if (evento.equals("btnSair")) {
            this.model.excluir(this);
            this.getView().dispose();
        }

        if (evento.equals("FormClose")) {
            this.model.excluir(this);
        }

        if (evento.equals("btnPesquisar")) {
            try {
                if (this.getView().getEdtPesquisa().getText().equals("")) {
                    this.model.buscarTodosEmpresa();
                } else {
                    switch (this.getView().getCbPesquisa().getSelectedIndex()) {
                        case 0:
                            this.model.buscarEmpresa(Integer.parseInt(this.getView().getEdtPesquisa().getText().trim()));
                            break;
                        case 1:
                            this.model.buscarEmpresa(this.getView().getEdtPesquisa().getText().trim());
                            break;
                        default:
                            this.getView().getLblPesquisa().setText("Pesquisar por Código");
                            break;
                    }
                }
            } catch (SQLException | ClassNotFoundException ex) {
                this.getView().mostraMensagem("Não foi possível listar as empresas. "
                        + "Mensagem retornada: " + ex.getMessage());
            }
        }

        if (evento.equals("btnSelecionar")) {

            if (this.getView().getTableEmpresa().getRowCount() <= 0) {
                return;
            }

            int linhaRegistro = this.getView().getTableEmpresa().getSelectedRow();
            try {
                this.model.setEmpresaSelecionada(this.model.buscarEmpresa(Integer.parseInt(this.getView().getTableEmpresa().getValueAt(linhaRegistro, 0).toString())));
                if (this.model.getObservers().size() == 1) {
                    this.model.abreTela("FrmCadastroEmpresa");
                }
                this.getView().dispose();
            } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                this.getView().mostraMensagem("Não foi possível selecionar a empresa."
                        + " Mensagem retornada: " + ex.getMessage());
            }
        }
        this.model.avisarObservers();
    }
}
