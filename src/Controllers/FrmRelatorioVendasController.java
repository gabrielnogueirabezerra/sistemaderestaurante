package Controllers;

import Models.Configuracao;
import Views.FrmRelatorioVendas;
import com.itextpdf.text.DocumentException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class FrmRelatorioVendasController extends Controller {

    public FrmRelatorioVendasController(FrmRelatorioVendas view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;
            this.model.incluir(this);
        }
    }

    @Override
    public void evento(String evento) {
        if (evento.equals("FormShow")) {

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            this.getView().getEdtDataInicial().setText(sdf.format(Calendar.getInstance().getTime()));
            this.getView().getEdtDataFinal().setText(sdf.format(Calendar.getInstance().getTime()));
        }

        if (evento.equals("FormClose")) {
            this.model.excluir(this);
        }

        if (evento.equals("btnSair")) {
            this.getView().dispose();
        }

        if (evento.equals("btnGerarRelatorio")) {
            if (!this.getView().getEdtDataFinal().getText().trim().equals("") && !this.getView().getEdtDataInicial().getText().trim().equals("")) {
                try {
                    this.model.geraRelatorioVendas(this.getView().getEdtDataInicial().getText().trim(), this.getView().getEdtDataFinal().getText().trim());
                } catch (ClassNotFoundException | SQLException | DocumentException | IOException ex) {
                    this.getView().mostraMensagem("Não foi possível gerar o relatório de vendas. Mensagem retornada: " + ex.getMessage());
                }
            }
        }
    }

    @Override
    public void alterar() {

    }

    public FrmRelatorioVendas getView() {
        return (FrmRelatorioVendas) this.view;
    }

}
