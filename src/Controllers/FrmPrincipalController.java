package Controllers;

import Models.Configuracao;
import Views.FrmPrincipal;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class FrmPrincipalController extends Controller {

    public FrmPrincipalController(FrmPrincipal view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;

            this.model.setDesktop(this.getView().getDpPanel());

            this.centralizaTela();
        }
    }

    @Override
    public void evento(String evento) {
        try {
            if (evento.equals("CadastroGarcom")) {
                this.model.abreTela("FrmCadastroGarcom");
            }

            if (evento.equals("ConsultaGarcom")) {
                this.model.abreTela("FrmConsultaGarcom");
            }

            if (evento.equals("CadastroUsuario")) {
                this.model.abreTela("FrmCadastroUsuario");
            }

            if (evento.equals("ConsultaUsuario")) {
                this.model.abreTela("FrmConsultaUsuario");
            }

            if (evento.equals("CadastroSecao")) {
                this.model.abreTela("FrmCadastroSecao");
            }

            if (evento.equals("ConsultaSecao")) {
                this.model.abreTela("FrmConsultaSecao");
            }

            if (evento.equals("CadastroProduto")) {
                this.model.abreTela("FrmCadastroProduto");
            }

            if (evento.equals("ConsultaProduto")) {
                this.model.abreTela("FrmConsultaProduto");
            }

            if (evento.equals("AbreMesa")) {
                this.model.abreTela("FrmAbreMesa");
            }

            if (evento.equals("AbreGerenciamentoMesas")) {
                this.model.abreTela("FrmMesas");
            }
            if (evento.equals("AbreGerenciamentoMesa")) {
                this.model.abreTela("FrmGerenciamentoMesa");
            }

            if (evento.equals("CadastroFormaPagamento")) {
                this.model.abreTela("FrmCadastroFormaPagamento");
            }

            if (evento.equals("ConsultaFormaPagamento")) {
                this.model.abreTela("FrmConsultaFormaPagamento");
            }

            if (evento.equals("RelatorioVendas")) {
                this.model.abreTela("FrmRelatorioVendas");
            }

            if (evento.equals("RelatorioMesas")) {
                this.model.abreTela("FrmRelatorioMesas");
            }

            if (evento.equals("Logout")) {
                System.exit(0);
            }
            
            if (evento.equals("CadastroEmpresa")) {
                this.model.abreTela("FrmCadastroEmpresa");
            }
            
            if (evento.equals("ConsultaEmpresa")) {
                this.model.abreTela("FrmConsultaEmpresa");
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
            this.getView().mostraMensagem("Não foi possivel abrir esta tela. Mensagem retornada: " + ex.getMessage());
        }
    }

    @Override
    public void alterar() {

    }

    public void centralizaTela() {
        Insets in = Toolkit.getDefaultToolkit().getScreenInsets(this.getView().getGraphicsConfiguration());
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();

        int width = d.width - (in.left + in.top);
        int height = d.height - (in.top + in.bottom);
        this.getView().setSize(width, height);
        this.getView().setLocation(in.left, in.top);
        this.getView().setResizable(false);
    }

    public FrmPrincipal getView() {
        return (FrmPrincipal) this.view;
    }

}
