package Controllers;

import Models.Configuracao;
import Models.Secao;
import Views.FrmCadastroProduto;
import java.awt.Image;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class FrmCadastroProdutoController extends Controller {

    public FrmCadastroProdutoController(FrmCadastroProduto view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;

            this.model.incluir(this);
        }
    }

    public void evento(KeyEvent evento) {

        String caracteres = "0987654321,";
        if (!caracteres.contains(evento.getKeyChar() + "")) {
            evento.consume();
            return;
        }

        if (this.getView().getValor().getText().contains(",")) {
            String decimais[] = this.getView().getValor().getText().split(",");
            if (decimais.length > 1) {
                if (decimais[1].length() > 1) {
                    evento.consume();
                }
            }
        }
    }

    @Override
    public void evento(String evento) {

        if (evento.equals("btnCadastrarSecao")) {
            try {
                this.model.abreTela("FrmCadastroSecao");
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                this.getView().mostraMensagem("Não foi possível abrir a tela de cadastro de seção. Mensagem retornada: " + ex.getMessage());
            }
        }

        if (evento.equals("btnPesquisarSecao")) {
            try {
                this.model.abreTela("FrmConsultaSecao");
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                this.getView().mostraMensagem("Não foi possível abrir a tela de consulta de seção. Mensagem retornada: " + ex.getMessage());
            }
        }

        if (evento.equals("btnGravar")) {
            if (this.getView().validaCampos()) {
                try {
                    if (this.getView().getId().getText().trim().equals("")) {

                        this.getView().getId().setText(String.valueOf(this.model.inserirProduto(
                                this.getView().getNome().getText().trim(),
                                Double.parseDouble(this.getView().getValor().getText().trim().replace(',', '.')),
                                this.model.getCaminhoFoto(),
                                Integer.parseInt(this.getView().getCodigoSecao().getText().trim()),
                                this.getView().getDescricao().getText().trim()).getId()));

                        this.getView().botoes('G');
                    } else {
                        this.model.alterarProduto(Integer.parseInt(this.getView().getId().getText().trim()), this.getView().getNome().getText().trim(),
                                Double.parseDouble(this.getView().getValor().getText().trim().replace(',', '.')),
                                this.model.getCaminhoFoto(),
                                Integer.parseInt(this.getView().getCodigoSecao().getText().trim()),
                                this.getView().getDescricao().getText().trim());
                        this.getView().botoes('G');
                    }

                    this.model.setCaminhoFoto("");
                } catch (ClassNotFoundException | SQLException ex) {
                    this.getView().mostraMensagem("Não foi possível inserir o produto. Mensagem retornada: " + ex.getMessage());
                }
            }
        }

        if (evento.equals("btnSair")) {
            this.getView().dispose();
        }

        if (evento.equals("btnNovo")) {
            this.getView().botoes('N');
            this.getView().limpaCampos();
        }

        if (evento.equals("FormClose")) {
            this.model.excluir(this);
        }

        if (evento.equals("btnExcluir")) {
            if (!this.getView().getId().getText().trim().equals("")) {
                if (this.getView().mensagemVerificacao("Deseja excluir esse registro?")) {
                    try {
                        this.model.excluirProduto(Integer.parseInt(this.getView().getId().getText().trim()));
                        this.getView().botoes('E');
                        this.getView().limpaCampos();
                    } catch (ClassNotFoundException | SQLException ex) {
                        this.getView().mostraMensagem("Não foi possível excluir a seção. Mensagem retornada: " + ex.getMessage());
                    }
                }
            }
        }

        if (evento.equals("btnAlterar")) {
            this.getView().botoes('A');
        }

        if (evento.equals("btnCancelar")) {
            this.getView().botoes('C');
            this.getView().limpaCampos();
        }

        if (evento.equals("btnSelecionarFoto")) {
            JFileChooser arquivo = new JFileChooser();
            arquivo.setFileFilter(new FileNameExtensionFilter("Arquivo de Imagem", "jpg", "jpeg", "png", "bpm"));
            int resposta = arquivo.showOpenDialog(null);
            if (resposta == JFileChooser.APPROVE_OPTION) {

                this.model.setCaminhoFoto(arquivo.getSelectedFile().getPath());

                ImageIcon imageIcon = new ImageIcon(arquivo.getSelectedFile().getPath()); // load the image to a imageIcon
                Image image = imageIcon.getImage(); // transform it 
                Image newimg = image.getScaledInstance(120, 120, java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
                imageIcon = new ImageIcon(newimg);

                this.getView().getCaminhoFoto().setText("");
                this.getView().getCaminhoFoto().setIcon(imageIcon);
            }
        }

        if (evento.equals("btnConsultar")) {
            try {
                this.model.excluir(this);
                this.getView().dispose();
                this.model.abreTela("FrmConsultaProduto");
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                this.getView().mostraMensagem("Não foi possível abrir a tela de consulta de produto. Mensagem retornada: " + ex.getMessage());
            }
        }

        if (evento.equals("FormShow")) {
            this.getView().botoes('N');
        }

        if (evento.equals("EdtCodigoSecaoSaida")) {
            if (!this.getView().getCodigoSecao().getText().equals("")) {
                try {
                    this.model.setSecaoSelecionada(this.model.buscarSecao(Integer.parseInt(this.getView().getCodigoSecao().getText().trim())));
                    this.model.avisarObservers();
                } catch (ClassNotFoundException | SQLException ex) {
                    this.getView().mostraMensagem("Não foi possível buscar a seção. Mensagem retornada: " + ex.getMessage());
                }
            }
        }
    }

    @Override
    public void alterar() {
        if (this.model.getSecaoSelecionada() != null) {
            this.getView().getCodigoSecao().setText(String.valueOf(this.model.getSecaoSelecionada().getId()));
            this.getView().getDescricaoSecao().setText(this.model.getSecaoSelecionada().getDescricao());

            this.model.setSecoes(new ArrayList<Secao>());
        }

        if (this.model.getProdutoSelecionado() != null) {
            this.getView().getId().setText(String.valueOf(this.model.getProdutoSelecionado().getId()));
            this.getView().getNome().setText(this.model.getProdutoSelecionado().getNome());
            this.getView().getValor().setText(String.valueOf(this.model.getProdutoSelecionado().getValor()).replace('.', ','));
            this.model.setCaminhoFoto(this.model.getProdutoSelecionado().getFoto());

            if (this.model.getProdutoSelecionado().getFoto() != null) {

                ImageIcon imageIcon = new ImageIcon(this.model.getProdutoSelecionado().getFoto()); // load the image to a imageIcon
                Image image = imageIcon.getImage(); // transform it 
                Image newimg = image.getScaledInstance(120, 120, java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
                imageIcon = new ImageIcon(newimg);

                this.getView().getCaminhoFoto().setText("");
                this.getView().getCaminhoFoto().setIcon(imageIcon);

            }

            this.getView().getCodigoSecao().setText(String.valueOf(this.model.getProdutoSelecionado().getSecao().getId()));
            this.getView().getDescricaoSecao().setText(this.model.getProdutoSelecionado().getSecao().getDescricao());
            this.getView().getDescricao().setText(this.model.getProdutoSelecionado().getDescricao());
            this.getView().botoes('G');
            this.model.setProdutos(new ArrayList<>());
            this.model.setProdutoSelecionado(null);
        }
    }

    public FrmCadastroProduto getView() {
        return (FrmCadastroProduto) this.view;
    }

}
