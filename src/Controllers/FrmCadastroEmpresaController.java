/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Configuracao;
import Models.Empresa;
import Views.FrmCadastroEmpresa;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Beatriz Oliveiira
 */
public class FrmCadastroEmpresaController extends Controller {

    public FrmCadastroEmpresaController(FrmCadastroEmpresa view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;

            this.model.incluir(this);
        }
    }

    public FrmCadastroEmpresa getView() {
        return (FrmCadastroEmpresa) this.view;
    }

    @Override
    public void alterar() {
        if (this.model.getEmpresaSelecionada() != null) {
            this.getView().getId().setText(String.valueOf(this.model.getEmpresaSelecionada().getId()));
            this.getView().getCNPJ().setText((this.model.getEmpresaSelecionada().getCNPJ()));
            this.getView().getNomeFantasia().setText(this.model.getEmpresaSelecionada().getNomeFantasia());
            this.getView().getRazaoSocial().setText(this.model.getEmpresaSelecionada().getRazaoSocial());
            this.getView().getEndereco().setText(this.model.getEmpresaSelecionada().getEndereco());
            this.getView().getNumero().setText((this.model.getEmpresaSelecionada().getNumero()));
            this.getView().getTelefone().setText(this.model.getEmpresaSelecionada().getTelefone());
            this.getView().getEmail().setText(this.model.getEmpresaSelecionada().getEmail());
            this.getView().botoes('G');

            this.model.setEmpresas(new ArrayList<Empresa>());
            this.model.setEmpresaSelecionada(null);
        }
    }

    @Override
    public void evento(String evento) {

        if (evento.equals("btnGravar")) {
            if (this.getView().validaCampos()) {
                try {
                    if (this.getView().getId().getText().trim().equals("")) {
                        this.getView().getId().setText(String.valueOf(this.model.inserirEmpresa(this.getView().getRazaoSocial().getText().trim(),
                                this.getView().getNomeFantasia().getText().trim(), this.getView().getCNPJ().getText().trim(),
                                this.getView().getEndereco().getText().trim(), this.getView().getNumero().getText().trim(),
                                this.getView().getEmail().getText().trim(), this.getView().getTelefone().getText().trim()).getId()));
                        this.getView().botoes('G');
                    } else {
                        this.model.alterarEmpresa(Integer.parseInt(this.getView().getId().getText().trim()), this.getView().getRazaoSocial().getText().trim(),
                                this.getView().getNomeFantasia().getText().trim(), this.getView().getCNPJ().getText().trim(),
                                this.getView().getEndereco().getText().trim(), this.getView().getNumero().getText().trim(),
                                this.getView().getEmail().getText().trim(), this.getView().getTelefone().getText().trim());
                        this.getView().botoes('G');
                    }
                } catch (ClassNotFoundException | SQLException ex) {
                    this.getView().mostraMensagem("Não foi possível inserir a empresa. Mensagem retornada: " + ex.getMessage());
                }
            }
        }

        if (evento.equals("btnSair")) {
            this.getView().dispose();
        }

        if (evento.equals("btnNovo")) {
            this.getView().botoes('N');
            this.getView().limpaCampos();
        }

        if (evento.equals("FormClose")) {
            this.model.excluir(this);
        }

        if (evento.equals("btnExcluir")) {
            if (!this.getView().getId().getText().trim().equals("")) {
                if (this.getView().mensagemVerificacao("Deseja excluir esse registro?")) {
                    try {
                        this.model.excluirEmpresa(Integer.parseInt(this.getView().getId().getText()));
                        this.getView().botoes('E');
                        this.getView().limpaCampos();
                    } catch (ClassNotFoundException | SQLException ex) {
                        this.getView().mostraMensagem("Não foi possível excluir a empresa. Mensagem retornada: " + ex.getMessage());
                    }
                }
            }
        }

        if (evento.equals("btnAlterar")) {
            this.getView().botoes('A');
        }

        if (evento.equals("btnCancelar")) {
            this.getView().botoes('C');
            this.getView().limpaCampos();
        }

        if (evento.equals("btnConsultar")) {
            try {
                this.model.excluir(this);

                this.getView().dispose();

                this.model.abreTela("FrmConsultaEmpresa");
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                this.getView().mostraMensagem("Não foi possível abrir a tela de consulta de empresa. Mensagem retornada: " + ex.getMessage());
            }
        }

        if (evento.equals("FormShow")) {
            this.getView().botoes('N');
        }
    }

}
