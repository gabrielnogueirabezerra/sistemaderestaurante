package Controllers;

import Models.Configuracao;
import Models.Garcom;
import Views.FrmConsultaGarcom;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class FrmConsultaGarcomController extends Controller {

    public FrmConsultaGarcomController(FrmConsultaGarcom view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;
            this.model.incluir(this);
        }
    }

    @Override
    public void evento(String evento) {
        if (evento.equals("MudarPesquisa")) {

            this.getView().getEdtPesquisa().setText("");

            switch (this.getView().getCbPesquisa().getSelectedIndex()) {
                case 0:
                    this.getView().getLblPesquisa().setText("Pesquisar por Código");
                    break;
                case 1:
                    this.getView().getLblPesquisa().setText("Pesquisar por Nome");
                    break;
                default:
                    this.getView().getLblPesquisa().setText("Pesquisar por Código");
                    break;
            }
        }

        if (evento.equals("btnSair")) {
            this.model.excluir(this);
            this.getView().dispose();
        }

        if (evento.equals("FormClose")) {
            this.model.excluir(this);
        }

        if (evento.equals("btnPesquisar")) {
            try {
                if (this.getView().getEdtPesquisa().getText().equals("")) {
                    this.model.buscarTodosGarcons();
                } else {
                    switch (this.getView().getCbPesquisa().getSelectedIndex()) {
                        case 0:
                            this.model.buscarGarcom(Integer.parseInt(this.getView().getEdtPesquisa().getText().trim()));
                            break;
                        case 1:
                            this.model.buscarGarcom(this.getView().getEdtPesquisa().getText().trim());
                            break;
                        default:
                            this.getView().getLblPesquisa().setText("Pesquisar por Código");
                            break;
                    }
                }
            } catch (SQLException | ClassNotFoundException ex) {
                this.getView().mostraMensagem("Não foi possível listar os garçons. "
                        + "Mensagem retornada: " + ex.getMessage());
            }
        }

        if (evento.equals("btnSelecionar")) {

            if (this.getView().getTableGarcons().getRowCount() <= 0) {
                return;
            }

            int linhaRegisto = this.getView().getTableGarcons().getSelectedRow();
            try {
                this.model.setGarcomSelecionado(this.model.buscarGarcom(Integer.parseInt(this.getView().getTableGarcons().getValueAt(linhaRegisto, 0).toString())));
                if (this.model.getObservers().size() == 1) {
                    this.model.abreTela("FrmCadastroGarcom");
                }
                this.getView().dispose();
            } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                this.getView().mostraMensagem("Não foi possível selecionar o garçom."
                        + " Mensagem retornada: " + ex.getMessage());
            }
        }
        this.model.avisarObservers();
    }

    public void evento(KeyEvent evt) {
        if (this.getView().getCbPesquisa().getSelectedIndex() == 0) {
            String caracteres = "0987654321";
            if (!caracteres.contains(evt.getKeyChar() + "")) {
                evt.consume();
            }
        }
    }

    @Override
    public void alterar() {
        ArrayList<Garcom> garcons = this.model.getGarcons();
        this.getView().limpaTableGarcons();
        if (garcons != null) {

            for (Garcom garcom : garcons) {
                String[] novaLinha = {String.valueOf(garcom.getId()), garcom.getNome()};
                ((DefaultTableModel) this.getView().getTableGarcons().getModel()).addRow(novaLinha);
            }
        }

        this.model.setGarcons(new ArrayList<Garcom>());
    }

    public FrmConsultaGarcom getView() {
        return (FrmConsultaGarcom) this.view;
    }
}
