package Controllers;

import Models.Configuracao;
import Views.FrmAbreMesa;
import java.awt.event.KeyEvent;
import java.sql.SQLException;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class FrmAbreMesaController extends Controller {
    
    public FrmAbreMesaController(FrmAbreMesa view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;
            this.model.incluir(this);
        }
    }
    
    public void evento(KeyEvent evento) {
        
        String caracteres = "0987654321";
        if (!caracteres.contains(evento.getKeyChar() + "")) {
            evento.consume();
            return;
        }
        
    }
    
    @Override
    public void evento(String evento) {
        if (evento.equals("btnSair")) {
            this.getView().dispose();
        }
        
        if (evento.equals("FormClose")) {
            this.model.excluir(this);
        }
        
        if (evento.equals("btnAbrir")) {
            if (this.getView().validaCampos()) {
                
                try {
                    if (!this.model.existeMesaAberta(Integer.parseInt(this.getView().getNumeroMesa().getText().trim()))) {
                        this.model.abreMesa(Integer.parseInt(this.getView().getNumeroMesa().getText().trim()));
                        this.getView().dispose();
                    } else {
                        this.getView().mostraMensagem("Essa mesa já encontra-se em aberto.");
                    }
                } catch (ClassNotFoundException | SQLException ex) {
                    this.getView().mostraMensagem("Não foi possível abrir a mesa. Mensagem retornada: " + ex.getMessage());
                }
            }
        }
        
        this.model.avisarObservers();
    }
    
    @Override
    public void alterar() {
        if (this.model.getNumeroMesa() > 0) {
            this.getView().getNumeroMesa().setText(String.valueOf(this.model.getNumeroMesa()));
            
            this.model.setNumeroMesa(0);
        }
    }
    
    public FrmAbreMesa getView() {
        return (FrmAbreMesa) this.view;
    }
}
