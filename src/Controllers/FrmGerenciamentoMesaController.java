package Controllers;

import Models.Configuracao;
import Models.ItemMesa;
import Views.FrmGerenciamentoMesa;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class FrmGerenciamentoMesaController extends Controller {

    public FrmGerenciamentoMesaController(FrmGerenciamentoMesa view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;
            this.model.incluir(this);
        }
    }

    public void evento(String evt, KeyEvent evento, JTextField edt) {

        if (evt.equals("EdtDouble")) {
            String caracteres = ".0987654321";
            if (!caracteres.contains(evento.getKeyChar() + "")) {
                evento.consume();
                return;
            }

            if (edt.getText().contains(".")) {
                String decimais[] = edt.getText().split(".");
                if (decimais.length > 1) {
                    if (decimais[1].length() > 1) {
                        evento.consume();
                    }
                }
            }
        }

        if (evt.equals("EdtInteger")) {
            String caracteres = "0987654321";
            if (!caracteres.contains(evento.getKeyChar() + "")) {
                evento.consume();
            }
        }
    }

    @Override
    public void evento(String evento) {
        if (evento.equals("FormClose")) {
            this.model.excluir(this);
        }

        if (evento.equals("FormShow")) {
            if (this.model.getMesaSelecionada() != null) {
                this.getView().getEdtNumeroMesa().setText(String.valueOf(this.model.getMesaSelecionada().getNumero()));
                this.getView().getEdtCodigoProduto().requestFocus();
            }
        }

        if (evento.equals("EdtNumeroMesaExit")) {
            if (!this.getView().getEdtNumeroMesa().getText().equals("")) {
                try {
                    if (this.model.existeMesaAberta(Integer.parseInt(this.getView().getEdtNumeroMesa().getText().trim()))) {
                        this.model.setMesaSelecionada(this.model.buscaMesa(Integer.parseInt(this.getView().getEdtNumeroMesa().getText().trim())));
                    } else {
                        if (this.getView().mensagemVerificacao("Essa mesa não está aberta, deseja abrir?")) {
                            this.getView().limpaTableMesa();
                            this.model.setNumeroMesa(Integer.parseInt(this.getView().getEdtNumeroMesa().getText()));
                            this.model.abreTela("FrmAbreMesa");
                        }
                    }
                } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                    this.getView().mostraMensagem("Não foi possível selecionar essa mesa. Mensagem retornada: " + ex.getMessage());
                }
            }
        }

        // Comandos quando a mesa está selecionada
        if (this.model.getMesaSelecionada() != null) {
            if (evento.equals("btnPesquisarProduto")) {
                try {
                    this.model.abreTela("FrmConsultaProduto");
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                    this.getView().mostraMensagem("Não foi possível abrir a tela de consulta de produto. Mensagem retornada: " + ex.getMessage());
                }
            }

            if (evento.equals("btnCadastrarProduto")) {
                try {
                    this.model.abreTela("FrmCadastroProduto");
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                    this.getView().mostraMensagem("Não foi possível abrir a tela de cadastro de produto. Mensagem retornada: " + ex.getMessage());
                }
            }

            if (evento.equals("EdtCodigoProdutoSaida")) {
                if (!this.getView().getEdtCodigoProduto().getText().equals("")) {
                    try {
                        this.model.setProdutoSelecionado(this.model.buscarProduto(Integer.parseInt(this.getView().getEdtCodigoProduto().getText())));
                    } catch (ClassNotFoundException | SQLException ex) {
                        this.getView().mostraMensagem("Não foi possível buscar o produto. Mensagem retornada: " + ex.getMessage());
                    }
                }
            }

            if (evento.equals("btnPesquisarGarcom")) {
                try {
                    this.model.abreTela("FrmConsultaGarcom");
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                    this.getView().mostraMensagem("Não foi possível abrir a tela de consulta de garçom. Mensagem retornada: " + ex.getMessage());
                }
            }

            if (evento.equals("btnCadastrarGarcom")) {
                try {
                    this.model.abreTela("FrmCadastroGarcom");
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                    this.getView().mostraMensagem("Não foi possível abrir a tela de cadastro de garçom. Mensagem retornada: " + ex.getMessage());
                }
            }

            if (evento.equals("EdtCodigoGarcomExit")) {
                if (!this.getView().getEdtCodigoGarcom().getText().equals("")) {
                    try {
                        this.model.setGarcomSelecionado(this.model.buscarGarcom(Integer.parseInt(this.getView().getEdtCodigoGarcom().getText())));
                    } catch (ClassNotFoundException | SQLException ex) {
                        this.getView().mostraMensagem("Não foi possível buscar o garçom. Mensagem retornada: " + ex.getMessage());
                    }
                }
            }

            if (evento.equals("EdtQuantidadeExit")) {
                if (!this.getView().getEdtCodigoProduto().equals("") && !this.getView().getEdtQuantidade().getText().equals("") && !this.getView().getEdtValor().getText().equals("")) {
                    this.getView().getEdtTotal().setText(String.valueOf(
                            Double.parseDouble(this.getView().getEdtQuantidade().getText()) * Double.parseDouble(this.getView().getEdtValor().getText())
                    ));
                }
            }

            if (evento.equals("btnGravar")) {
                if (this.getView().validaCampos()) {
                    try {
                        this.model.inserirItemMesa(
                                Integer.parseInt(this.getView().getEdtCodigoProduto().getText()),
                                Integer.parseInt(this.getView().getEdtCodigoGarcom().getText()), Double.parseDouble(this.getView().getEdtQuantidade().getText()),
                                Double.parseDouble(this.getView().getEdtValor().getText())
                        );
                        this.getView().limpaCamposProduto();
                        this.model.setGarcons(new ArrayList<>());
                        this.model.setProdutos(new ArrayList<>());
                        this.getView().getEdtCodigoProduto().requestFocus();
                    } catch (ClassNotFoundException | SQLException ex) {
                        this.getView().mostraMensagem("Não foi possível inserir o item na mesa. Mensagem retornada: " + ex.getMessage());
                    }
                }
            }

            if (evento.equals("btnCancelarItem")) {
                int linhaRegisto = this.getView().getTableMesa().getSelectedRow();

                try {
                    this.model.cancelaItemMesa(Integer.parseInt(this.getView().getTableMesa().getValueAt(linhaRegisto, 0).toString()), Integer.parseInt(this.getView().getTableMesa().getValueAt(linhaRegisto, 1).toString()));
                    this.model.setMesaSelecionada(this.model.buscaMesa(this.model.getMesaSelecionada().getNumero()));
                } catch (ClassNotFoundException | SQLException ex) {
                    this.getView().mostraMensagem("Não foi possível cancelar a mesa. Mensagem retornada: " + ex.getMessage());
                }
            }
        }

        this.model.avisarObservers();
    }

    @Override
    public void alterar() {
        // buscar informações do produto
        if (this.model.getProdutoSelecionado() != null) {

            this.getView().getEdtCodigoProduto().setText(String.valueOf(this.model.getProdutoSelecionado().getId()));
            this.getView().getEdtDescricaoProduto().setText(this.model.getProdutoSelecionado().getNome());
            this.getView().getEdtQuantidade().setText("1");

            this.getView().getEdtValor().setText(String.valueOf(this.model.getProdutoSelecionado().getValor()));

            this.getView().getEdtTotal().setText(String.valueOf(this.model.getProdutoSelecionado().getValor()));

            this.model.setProdutoSelecionado(null);
            this.model.setProdutos(new ArrayList<>());
        }

        if (this.model.getGarcomSelecionado() != null) {

            this.getView().getEdtCodigoGarcom().setText(String.valueOf(this.model.getGarcomSelecionado().getId()));
            this.getView().getEdtDescricaoGarcom().setText(this.model.getGarcomSelecionado().getNome());

            this.model.setGarcomSelecionado(null);
            this.model.setGarcons(new ArrayList<>());
        }

        this.getView().limpaTableMesa();

        if (this.model.getMesaSelecionada() != null) {
            ArrayList<ItemMesa> itensMesa = this.model.getMesaSelecionada().getItensMesa();

            this.getView().getEdtTotalMesa().setText(String.valueOf(this.model.getMesaSelecionada().total()));

            if (itensMesa != null) {
                this.getView().getTableMesa().setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
                    @Override
                    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                        final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                        if (table.getValueAt(row, 7) == "CANCELADO") {
                            c.setForeground(Color.red);
                        } else {
                            c.setForeground(Color.BLACK);
                        }
                        return c;
                    }
                });

                String status = "";
                for (ItemMesa itemMesa : itensMesa) {

                    if (itemMesa.getCancelado() == 0) {
                        status = "";
                    } else {
                        status = "CANCELADO";
                    }

                    String[] novaLinha = {
                        String.valueOf(itemMesa.getId()),
                        String.valueOf(itemMesa.getOrdem()),
                        itemMesa.getGarcom().getNome(),
                        itemMesa.getProduto().getNome(),
                        String.valueOf(itemMesa.getQuantidade()),
                        String.valueOf(itemMesa.getValor()),
                        String.valueOf(itemMesa.total()),
                        status};
                    ((DefaultTableModel) this.getView().getTableMesa().getModel()).addRow(novaLinha);

                }

            }
        } else {
            this.getView().getEdtTotalMesa().setText("0");
        }
    }

    public FrmGerenciamentoMesa getView() {
        return (FrmGerenciamentoMesa) this.view;
    }

}
