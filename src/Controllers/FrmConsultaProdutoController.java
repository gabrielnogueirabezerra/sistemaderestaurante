package Controllers;

import Models.Configuracao;
import Models.Garcom;
import Models.Produto;
import Models.Secao;
import Views.FrmConsultaProduto;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class FrmConsultaProdutoController extends Controller {

    public FrmConsultaProdutoController(FrmConsultaProduto view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;
            this.model.incluir(this);
        }
    }

    public void evento(KeyEvent evt) {
        if (this.getView().getCbPesquisa().getSelectedIndex() == 0) {
            String caracteres = "0987654321";
            if (!caracteres.contains(evt.getKeyChar() + "")) {
                evt.consume();
            }
        }
    }

    @Override
    public void evento(String evento) {
        if (evento.equals("MudarPesquisa")) {

            this.getView().getEdtPesquisa().setText("");

            switch (this.getView().getCbPesquisa().getSelectedIndex()) {
                case 0:
                    this.getView().getLblPesquisa().setText("Pesquisar por Código");
                    break;
                case 1:
                    this.getView().getLblPesquisa().setText("Pesquisar por Nome");
                    break;
                case 2:
                    this.getView().getLblPesquisa().setText("Pesquisar por Seção");
                default:
                    this.getView().getLblPesquisa().setText("Pesquisar por Código");
                    break;
            }
        }

        if (evento.equals("btnSair")) {
            this.model.excluir(this);
            this.getView().dispose();
        }

        if (evento.equals("FormClose")) {
            this.model.excluir(this);
        }

        if (evento.equals("btnPesquisar")) {
            try {
                if (this.getView().getEdtPesquisa().getText().equals("")) {
                    this.model.buscarTodosProdutos();
                } else {
                    switch (this.getView().getCbPesquisa().getSelectedIndex()) {
                        case 0:
                            this.model.buscarProduto(Integer.parseInt(this.getView().getEdtPesquisa().getText().trim()));
                            break;
                        case 1:
                            this.model.buscarProduto(this.getView().getEdtPesquisa().getText().trim());
                            break;
                        case 2:
                            this.model.buscarProduto(new Secao(Integer.parseInt(this.getView().getEdtPesquisa().getText().trim())));
                            break;
                        default:
                            this.getView().getLblPesquisa().setText("Pesquisar por Código");
                            break;
                    }
                }
            } catch (SQLException | ClassNotFoundException ex) {
                this.getView().mostraMensagem("Não foi possível listar os produtos. "
                        + "Mensagem retornada: " + ex.getMessage());
            }
        }

        if (evento.equals("btnSelecionar")) {

            if (this.getView().getTableProdutos().getRowCount() <= 0) {
                return;
            }

            int linhaRegisto = this.getView().getTableProdutos().getSelectedRow();
            try {
                this.model.setProdutoSelecionado(this.model.buscarProduto(Integer.parseInt(this.getView().getTableProdutos().getValueAt(linhaRegisto, 0).toString())));

                this.getView().dispose();
            } catch (ClassNotFoundException | SQLException ex) {
                this.getView().mostraMensagem("Não foi possível selecionar o produto."
                        + " Mensagem retornada: " + ex.getMessage());
            }
        }
        this.model.avisarObservers();
    }

    @Override
    public void alterar() {
        ArrayList<Produto> produtos = this.model.getProdutos();
        this.getView().limpaTableProdutos();
        if (produtos != null) {

            for (Produto produto : produtos) {
                String[] novaLinha = {
                    String.valueOf(produto.getId()),
                    produto.getNome(),
                    produto.getSecao().getDescricao(),
                    String.valueOf(produto.getValor()).replace('.', ',')};
                ((DefaultTableModel) this.getView().getTableProdutos().getModel()).addRow(novaLinha);
            }
        }

        this.model.setProdutos(new ArrayList<Produto>());
    }

    public FrmConsultaProduto getView() {
        return (FrmConsultaProduto) this.view;
    }

}
