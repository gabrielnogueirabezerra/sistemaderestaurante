package Controllers;

import Models.Configuracao;
import Views.FrmCadastroGarcom;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class FrmCadastroGarcomController extends Controller {

    public FrmCadastroGarcomController(FrmCadastroGarcom view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;
            this.model.incluir(this);
        }
    }

    @Override
    public void alterar() {
        if (this.model.getGarcomSelecionado() != null) {
            this.getView().getId().setText(String.valueOf(this.model.getGarcomSelecionado().getId()));
            this.getView().getNome().setText(this.model.getGarcomSelecionado().getNome());
            this.getView().botoes('G');
        }

        this.model.setGarcomSelecionado(null);
    }

    public FrmCadastroGarcom getView() {
        return (FrmCadastroGarcom) this.view;
    }

    @Override
    public void evento(String evento) {

        if (evento.equals("btnGravar")) {
            if (this.getView().validaCampos()) {
                try {
                    if (this.getView().getId().getText().trim().equals("")) {
                        this.getView().getId().setText(String.valueOf(this.model.inserirGarcom(this.getView().getNome().getText().trim()).getId()));
                        this.getView().botoes('G');
                    } else {
                        this.model.alterarGarcom(Integer.parseInt(this.getView().getId().getText().trim()), this.getView().getNome().getText().trim());
                        this.getView().botoes('G');
                    }
                } catch (ClassNotFoundException | SQLException ex) {
                    this.getView().mostraMensagem("Não foi possível inserir o garçom. Mensagem retornada: " + ex.getMessage());
                }
            }
        }

        if (evento.equals("btnSair")) {
            this.getView().dispose();
        }

        if (evento.equals("btnNovo")) {
            this.getView().botoes('N');
            this.getView().limpaCampos();
        }

        if (evento.equals("FormClose")) {
            this.model.excluir(this);
        }

        if (evento.equals("btnExcluir")) {
            if (!this.getView().getId().getText().trim().equals("")) {
                if (this.getView().mensagemVerificacao("Deseja excluir esse registro?")) {
                    try {
                        this.model.excluirGarcom(Integer.parseInt(this.getView().getId().getText()));
                        this.getView().botoes('E');
                        this.getView().limpaCampos();
                    } catch (ClassNotFoundException | SQLException ex) {
                        this.getView().mostraMensagem("Não foi possível excluir o garçom. Mensagem retornada: " + ex.getMessage());
                    }
                }
            }
        }

        if (evento.equals("btnAlterar")) {
            this.getView().botoes('A');
        }

        if (evento.equals("btnCancelar")) {
            this.getView().botoes('C');
            this.getView().limpaCampos();
        }

        if (evento.equals("btnConsultar")) {
            try {
                this.model.excluir(this);
                this.getView().dispose();
                this.model.abreTela("FrmConsultaGarcom");
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                this.getView().mostraMensagem("Não foi possível abrir a tela de consulta de garçom. Mensagem retornada: " + ex.getMessage());
            }
        }

        if (evento.equals("FormShow")) {
            if (this.model.getGarcomSelecionado() == null) {
                this.getView().botoes('N');
            } else {
                this.getView().botoes('G');
            }
        }
    }

}
