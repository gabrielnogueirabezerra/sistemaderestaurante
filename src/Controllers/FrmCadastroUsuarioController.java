package Controllers;

import Models.Configuracao;
import Views.FrmCadastroUsuario;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class FrmCadastroUsuarioController extends Controller {
    
    public FrmCadastroUsuarioController(FrmCadastroUsuario view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;
            this.model.incluir(this);
        }
    }
    
    @Override
    public void evento(String evento) {
        if (evento.equals("FormShow")) {
            if (this.model.getUsuarioSelecionado() == null) {
                this.getView().botoes('N');
            } else {
                this.getView().botoes('G');
            }
        }
        
        if (evento.equals("FormClose")) {
            this.model.excluir(this);
        }
        
        if (evento.equals("btnSair")) {
            this.getView().dispose();
        }
        
        if (evento.equals("btnCancelar")) {
            this.getView().botoes('C');
        }
        
        if (evento.equals("btnNovo")) {
            this.getView().botoes('N');
            this.getView().limpaCampos();
        }
        
        if (evento.equals("btnAlterar")) {
            this.getView().botoes('A');
        }
        
        if (evento.equals("btnGravar")) {
            if (this.getView().validaCampos()) {
                try {
                    if (this.getView().getId().getText().trim().equals("")) {
                        this.getView().getId().setText(String.valueOf(this.model.
                                inserirUsuario(this.getView().getNome().getText().trim(), this.getView().getSenha().getText().trim()).getId()));
                        this.getView().botoes('G');
                    } else {
                        this.model.alterarUsuario(Integer.parseInt(this.getView().getId().getText().trim()),
                                this.getView().getNome().getText().trim(), this.getView().getSenha().getText().trim());
                        this.getView().botoes('G');
                    }
                } catch (Exception ex) {
                    this.getView().mostraMensagem("Não foi possível inserir o usuário. Mensagem retornada: " + ex.getMessage());
                }
            }
        }
        
        if (evento.equals("btnExcluir")) {
            if (!this.getView().getId().getText().trim().equals("")) {
                if (this.getView().mensagemVerificacao("Deseja excluir esse registro?")) {
                    try {
                        this.model.excluirUsuario(Integer.parseInt(this.getView().getId().getText()));
                        this.getView().botoes('E');
                        this.getView().limpaCampos();
                    } catch (ClassNotFoundException | SQLException ex) {
                        this.getView().mostraMensagem("Não foi possível excluir o usuário. Mensagem retornada: " + ex.getMessage());
                    }
                }
            }
        }
        
        if (evento.equals("btnConsultar")) {
            try {
                this.model.excluir(this);
                this.getView().dispose();
                this.model.abreTela("FrmConsultaUsuario");
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                this.getView().mostraMensagem("Não foi possível abrir a tela de consulta de usuário. Mensagem retornada: " + ex.getMessage());
            }
        }
    }
    
    @Override
    public void alterar() {
        
        if (this.model.getUsuarioSelecionado() != null) {
            this.getView().getId().setText(String.valueOf(this.model.getUsuarioSelecionado().getId()));
            this.getView().getNome().setText(this.model.getUsuarioSelecionado().getLogin());
            this.getView().getSenha().setText(this.model.getUsuarioSelecionado().getSenha());
            this.getView().getConfirmarSenha().setText(this.model.getUsuarioSelecionado().getSenha());
            this.getView().botoes('G');
        }
        
        this.model.setUsuarioSelecionado(null);
    }
    
    public FrmCadastroUsuario getView() {
        return (FrmCadastroUsuario) this.view;
    }
}
