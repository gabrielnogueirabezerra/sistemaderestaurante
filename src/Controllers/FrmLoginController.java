package Controllers;

import Models.Configuracao;
import Views.FrmLogin;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class FrmLoginController extends Controller {

    public FrmLoginController(FrmLogin view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;
            this.model.incluir(this);

            this.centralizaTela();
        }
    }

    @Override
    public void evento(String evento) {
        if (evento.equals("btnSair")) {
            System.exit(0);
        }

        if (evento.equals("btnEntrar")) {

            if (this.getView().validaCampos()) {

                try {
                    this.model.logar(this.getView().getUsuario().getText().trim(),
                            this.getView().getSenha().getText().trim());

                    this.model.avisarObservers();
                } catch (Exception ex) {
                    Logger.getLogger(FrmLoginController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
    }

    @Override
    public void alterar() {
        if (this.model.getUsuario() != null) {
            this.getView().dispose();

            try {
                Thread.sleep(1200);
            } catch (InterruptedException ex) {
                Logger.getLogger(FrmLoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
            this.model.abreTelaPrincipal();
        } else {
            this.getView().mostraMensagem("Usuário ou Senha incorretos");
        }
    }

    public void centralizaTela() {
        this.getView().setLocationRelativeTo(null);
    }

    public FrmLogin getView() {
        return (FrmLogin) this.view;
    }

}
