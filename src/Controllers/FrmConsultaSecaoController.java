/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Configuracao;
import Models.Secao;
import Views.FrmConsultaSecao;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Beatriz Oliveiira
 */
public class FrmConsultaSecaoController extends Controller {

    public FrmConsultaSecaoController(FrmConsultaSecao view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;
            this.model.incluir(this);
        }
    }

    public FrmConsultaSecao getView() {
        return (FrmConsultaSecao) this.view;
    }

    @Override
    public void evento(String evento) {
        if (evento.equals("MudarPesquisa")) {

            this.getView().getEdtPesquisa().setText("");

            switch (this.getView().getCbPesquisa().getSelectedIndex()) {
                case 0:
                    this.getView().getLblPesquisa().setText("Pesquisar por Código");
                    break;
                case 1:
                    this.getView().getLblPesquisa().setText("Pesquisar por Descrição");
                    break;
                default:
                    this.getView().getLblPesquisa().setText("Pesquisar por Código");
                    break;
            }
        }

        if (evento.equals("btnSair")) {
            this.model.excluir(this);
            this.getView().dispose();
        }

        if (evento.equals("FormClose")) {
            this.model.excluir(this);
        }

        if (evento.equals("btnPesquisar")) {
            try {
                if (this.getView().getEdtPesquisa().getText().equals("")) {
                    this.model.buscarTodosSecao();
                } else {
                    switch (this.getView().getCbPesquisa().getSelectedIndex()) {
                        case 0:
                            this.model.buscarSecao(Integer.parseInt(this.getView().getEdtPesquisa().getText().trim()));
                            break;
                        case 1:
                            this.model.buscarSecao(this.getView().getEdtPesquisa().getText().trim());
                            break;
                        default:
                            this.getView().getLblPesquisa().setText("Pesquisar por Código");
                            break;
                    }
                }
            } catch (SQLException | ClassNotFoundException ex) {
                this.getView().mostraMensagem("Não foi possível listar as seções. "
                        + "Mensagem retornada: " + ex.getMessage());
            }
        }

        if (evento.equals("btnSelecionar")) {

            if (this.getView().getTableSecoes().getRowCount() <= 0) {
                return;
            }

            int linhaRegistro = this.getView().getTableSecoes().getSelectedRow();
            try {
                this.model.setSecaoSelecionada(this.model.buscarSecao(Integer.parseInt(this.getView().getTableSecoes().getValueAt(linhaRegistro, 0).toString())));
                if (this.model.getObservers().size() == 1) {
                    this.model.abreTela("FrmCadastroSecao");
                }
                this.getView().dispose();
            } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                this.getView().mostraMensagem("Não foi possível selecionar a seção."
                        + " Mensagem retornada: " + ex.getMessage());
            }
        }
        this.model.avisarObservers();
    }

    public void evento(KeyEvent evt) {
        if (this.getView().getCbPesquisa().getSelectedIndex() == 0) {
            String caracteres = "0987654321";
            if (!caracteres.contains(evt.getKeyChar() + "")) {
                evt.consume();
            }
        }
    }

    @Override
    public void alterar() {
        ArrayList<Secao> secoes = this.model.getSecoes();
        this.getView().limpaTableSecoes();
        if (secoes != null) {
            for (Secao secao : secoes) {
                String[] novaLinha = {String.valueOf(secao.getId()), secao.getDescricao()};
                ((DefaultTableModel) this.getView().getTableSecoes().getModel()).addRow(novaLinha);
            }
        }

        this.model.setSecoes(new ArrayList<Secao>());
    }
}
