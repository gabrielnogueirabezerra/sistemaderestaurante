package Controllers;

import Models.Configuracao;
import Models.Usuario;
import Views.FrmConsultaUsuario;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class FrmConsultaUsuarioController extends Controller {

    public FrmConsultaUsuarioController(FrmConsultaUsuario view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;
            this.model.incluir(this);
        }
    }

    @Override
    public void evento(String evento) {
        if (evento.equals("MudarPesquisa")) {

            this.getView().getEdtPesquisa().setText("");

            switch (this.getView().getCbPesquisa().getSelectedIndex()) {
                case 0:
                    this.getView().getLblPesquisa().setText("Pesquisar por Código");
                    break;
                case 1:
                    this.getView().getLblPesquisa().setText("Pesquisar por Nome");
                    break;
                default:
                    this.getView().getLblPesquisa().setText("Pesquisar por Código");
                    break;
            }
        }

        if (evento.equals("btnSair")) {
            this.model.excluir(this);
            this.getView().dispose();
        }

        if (evento.equals("FormClose")) {
            this.model.excluir(this);
        }

        if (evento.equals("btnPesquisar")) {
            try {
                if (this.getView().getEdtPesquisa().getText().equals("")) {
                    this.model.buscarTodosUsuarios();
                } else {
                    switch (this.getView().getCbPesquisa().getSelectedIndex()) {
                        case 0:
                            this.model.buscarUsuario(Integer.parseInt(this.getView().getEdtPesquisa().getText().trim()));
                            break;
                        case 1:
                            this.model.buscarUsuario(this.getView().getEdtPesquisa().getText().trim());
                            break;
                        default:
                            this.getView().getLblPesquisa().setText("Pesquisar por Código");
                            break;
                    }
                }
            } catch (SQLException | ClassNotFoundException ex) {
                this.getView().mostraMensagem("Não foi possível listar os usuários. "
                        + "Mensagem retornada: " + ex.getMessage());
            } catch (Exception ex) {

                this.getView().mostraMensagem("Não foi possível listar os usuários. "
                        + "Mensagem retornada: " + ex.getMessage());
            }
        }

        if (evento.equals("btnSelecionar")) {

            if (this.getView().getTableUsuarios().getRowCount() <= 0) {
                return;
            }

            int linhaRegisto = this.getView().getTableUsuarios().getSelectedRow();
            try {
                this.model.setUsuarioSelecionado(this.model.buscarUsuario(Integer.parseInt(this.getView().getTableUsuarios().getValueAt(linhaRegisto, 0).toString())));

                this.getView().dispose();
                if (this.model.getObservers().size() == 1) {
                    this.model.abreTela("FrmCadastroUsuario");
                }
                this.evento("FormClose");

            } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                this.getView().mostraMensagem("Não foi possível selecionar o garçom."
                        + " Mensagem retornada: " + ex.getMessage());
            }
        }
        this.model.avisarObservers();
    }

    public void evento(KeyEvent evt) {
        if (this.getView().getCbPesquisa().getSelectedIndex() == 0) {
            String caracteres = "0987654321";
            if (!caracteres.contains(evt.getKeyChar() + "")) {
                evt.consume();
            }
        }
    }

    @Override
    public void alterar() {
        ArrayList<Usuario> usuarios = this.model.getUsuarios();
        this.getView().limpaTableUsuarios();
        if (usuarios != null) {

            for (Usuario usuario : usuarios) {
                String[] novaLinha = {String.valueOf(usuario.getId()), usuario.getLogin()};
                ((DefaultTableModel) this.getView().getTableUsuarios().getModel()).addRow(novaLinha);
            }
        }

        this.model.setUsuarios(new ArrayList<Usuario>());
    }

    public FrmConsultaUsuario getView() {
        return (FrmConsultaUsuario) this.view;
    }

}
