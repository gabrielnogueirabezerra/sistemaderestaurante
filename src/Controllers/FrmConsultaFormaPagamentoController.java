/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Configuracao;
import Models.FormaPagamento;
import Views.FrmConsultaFormaPagamento;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Beatriz Oliveiira
 */
public class FrmConsultaFormaPagamentoController extends Controller {

    public FrmConsultaFormaPagamentoController(FrmConsultaFormaPagamento view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;
            this.model.incluir(this);
        }
    }

    public FrmConsultaFormaPagamento getView() {
        return (FrmConsultaFormaPagamento) this.view;
    }

    @Override
    public void evento(String evento) {
        if (evento.equals("MudarPesquisa")) {

            this.getView().getEdtPesquisa().setText("");

            switch (this.getView().getCbPesquisa().getSelectedIndex()) {
                case 0:
                    this.getView().getLblPesquisa().setText("Pesquisar por Código");
                    break;
                case 1:
                    this.getView().getLblPesquisa().setText("Pesquisar por Descrição");
                    break;
                default:
                    this.getView().getLblPesquisa().setText("Pesquisar por Código");
                    break;
            }
        }

        if (evento.equals("btnSair")) {
            this.model.excluir(this);
            this.getView().dispose();
        }

        if (evento.equals("FormClose")) {
            this.model.excluir(this);
        }

        if (evento.equals("btnPesquisar")) {
            try {
                if (this.getView().getEdtPesquisa().getText().equals("")) {
                    this.model.buscarTodasFormasPagamento();
                } else {
                    switch (this.getView().getCbPesquisa().getSelectedIndex()) {
                        case 0:
                            this.model.buscarFormaPagamento(Integer.parseInt(this.getView().getEdtPesquisa().getText().trim()));
                            break;
                        case 1:
                            this.model.buscarFormaPagamento(this.getView().getEdtPesquisa().getText().trim());
                            break;
                        default:
                            this.getView().getLblPesquisa().setText("Pesquisar por Código");
                            break;
                    }
                }
            } catch (SQLException | ClassNotFoundException ex) {
                this.getView().mostraMensagem("Não foi possível listar as seções. "
                        + "Mensagem retornada: " + ex.getMessage());
            }
        }

        if (evento.equals("btnSelecionar")) {

            if (this.getView().getTableFormasPagamento().getRowCount() <= 0) {
                return;
            }

            int linhaRegistro = this.getView().getTableFormasPagamento().getSelectedRow();
            try {
                this.model.setFormaPagamentoSelecionada(this.model.buscarFormaPagamento(Integer.parseInt(this.getView().getTableFormasPagamento().getValueAt(linhaRegistro, 0).toString())));

                if (this.model.getObservers().size() == 1) {
                    this.model.abreTela("FrmCadastroFormaPagamento");
                }
                this.getView().dispose();
            } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                this.getView().mostraMensagem("Não foi possível selecionar a forma de pagamento."
                        + " Mensagem retornada: " + ex.getMessage());
            }
        }
        this.model.avisarObservers();
    }

    public void evento(KeyEvent evt) {
        if (this.getView().getCbPesquisa().getSelectedIndex() == 0) {
            String caracteres = "0987654321";
            if (!caracteres.contains(evt.getKeyChar() + "")) {
                evt.consume();
            }
        }
    }

    @Override
    public void alterar() {
        ArrayList<FormaPagamento> formas = this.model.getFormas();
        this.getView().limpaTableFormasPagamento();
        if (formas != null) {

            for (FormaPagamento formaPagamento : formas) {
                String[] novaLinha = {String.valueOf(formaPagamento.getId()), formaPagamento.getDescricao()};
                ((DefaultTableModel) this.getView().getTableFormasPagamento().getModel()).addRow(novaLinha);
            }
        }

        this.model.setFormas(new ArrayList<FormaPagamento>());
    }

}
