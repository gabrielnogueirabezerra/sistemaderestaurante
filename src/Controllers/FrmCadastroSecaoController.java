package Controllers;

import Models.Configuracao;
import Models.Secao;
import Views.FrmCadastroSecao;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Beatriz Oliveiira
 */
public class FrmCadastroSecaoController extends Controller {

    public FrmCadastroSecaoController(FrmCadastroSecao view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;
            this.model.incluir(this);
        }
    }

    public FrmCadastroSecao getView() {
        return (FrmCadastroSecao) this.view;
    }

    @Override
    public void alterar() {
        if (this.model.getSecaoSelecionada() != null) {
            this.getView().getId().setText(String.valueOf(this.model.getSecaoSelecionada().getId()));
            this.getView().getDescricao().setText(this.model.getSecaoSelecionada().getDescricao());
            this.getView().botoes('G');

            this.model.setSecoes(new ArrayList<Secao>());
            this.model.setSecaoSelecionada(null);
        }
    }

    @Override
    public void evento(String evento) {
        if (evento.equals("btnGravar")) {
            if (this.getView().validaCampos()) {
                try {
                    if (this.getView().getId().getText().trim().equals("")) {
                        this.getView().getId().setText(String.valueOf(this.model.inserirSecao(this.getView().getDescricao().getText().trim()).getId()));
                        this.getView().botoes('G');
                    } else {
                        this.model.alterarSecao(Integer.parseInt(this.getView().getId().getText().trim()), this.getView().getDescricao().getText().trim());
                        this.getView().botoes('G');
                    }
                } catch (ClassNotFoundException | SQLException ex) {
                    this.getView().mostraMensagem("Não foi possível inserir a seção. Mensagem retornada: " + ex.getMessage());
                }
            }
        }

        if (evento.equals("btnSair")) {
            this.getView().dispose();
        }

        if (evento.equals("btnNovo")) {
            this.getView().botoes('N');
            this.getView().limpaCampos();
        }

        if (evento.equals("FormClose")) {
            this.model.excluir(this);
        }

        if (evento.equals("btnExcluir")) {
            if (!this.getView().getId().getText().trim().equals("")) {
                if (this.getView().mensagemVerificacao("Deseja excluir esse registro?")) {
                    try {
                        this.model.excluirGarcom(Integer.parseInt(this.getView().getId().getText()));
                        this.getView().botoes('E');
                        this.getView().limpaCampos();
                    } catch (ClassNotFoundException | SQLException ex) {
                        this.getView().mostraMensagem("Não foi possível excluir a seção. Mensagem retornada: " + ex.getMessage());
                    }
                }
            }
        }

        if (evento.equals("btnAlterar")) {
            this.getView().botoes('A');
        }

        if (evento.equals("btnCancelar")) {
            this.getView().botoes('C');
            this.getView().limpaCampos();
        }

        if (evento.equals("btnConsultar")) {
            try {
                this.model.excluir(this);
                this.getView().dispose();
                this.model.abreTela("FrmConsultaSecao");
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                this.getView().mostraMensagem("Não foi possível abrir a tela de consulta de seção. Mensagem retornada: " + ex.getMessage());
            }
        }

        if (evento.equals("FormShow")) {
            this.getView().botoes('N');
        }
    }

}
