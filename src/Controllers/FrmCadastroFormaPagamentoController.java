/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Configuracao;
import Views.FrmCadastroFormaPagamento;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

/**
 *
 * @author Beatriz Oliveiira
 */
public class FrmCadastroFormaPagamentoController extends Controller {

    public FrmCadastroFormaPagamentoController(FrmCadastroFormaPagamento view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;
            this.model.incluir(this);
        }
    }

    @Override
    public void alterar() {
        if (this.model.getFormaPagamentoSelecionada() != null) {
            this.getView().getId().setText(String.valueOf(this.model.getFormaPagamentoSelecionada().getId()));
            this.getView().getDescricao().setText(this.model.getFormaPagamentoSelecionada().getDescricao());
            this.getView().botoes('G');
        }

        this.model.setFormaPagamentoSelecionada(null);
    }

    public FrmCadastroFormaPagamento getView() {
        return (FrmCadastroFormaPagamento) this.view;
    }

    @Override
    public void evento(String evento) {

        if (evento.equals("btnGravar")) {
            if (this.getView().validaCampos()) {
                try {
                    if (this.getView().getId().getText().trim().equals("")) {
                        this.getView().getId().setText(String.valueOf(this.model.inserirFormaPagamento(this.getView().getDescricao().getText().trim()).getId()));
                        this.getView().botoes('G');
                    } else {
                        this.model.alterarFormaPagamento(Integer.parseInt(this.getView().getId().getText().trim()), this.getView().getDescricao().getText().trim());
                        this.getView().botoes('G');
                    }
                } catch (ClassNotFoundException | SQLException ex) {
                    this.getView().mostraMensagem("Não foi possível inserir a forma de pagamento. Mensagem retornada: " + ex.getMessage());
                }
            }
        }

        if (evento.equals("btnSair")) {
            this.getView().dispose();
        }

        if (evento.equals("btnNovo")) {
            this.getView().botoes('N');
            this.getView().limpaCampos();
        }

        if (evento.equals("FormClose")) {
            this.model.excluir(this);
        }

        if (evento.equals("btnExcluir")) {
            if (!this.getView().getId().getText().trim().equals("")) {
                if (this.getView().mensagemVerificacao("Deseja excluir esse registro?")) {
                    try {
                        this.model.excluirFormaPagamento(Integer.parseInt(this.getView().getId().getText()));
                        this.getView().botoes('E');
                        this.getView().limpaCampos();
                    } catch (ClassNotFoundException | SQLException ex) {
                        this.getView().mostraMensagem("Não foi possível excluir a forma de pagamento. Mensagem retornada: " + ex.getMessage());
                    }
                }
            }
        }

        if (evento.equals("btnAlterar")) {
            this.getView().botoes('A');
        }

        if (evento.equals("btnCancelar")) {
            this.getView().botoes('C');
            this.getView().limpaCampos();
        }

        if (evento.equals("btnConsultar")) {
            try {
                this.model.excluir(this);

                this.getView().dispose();

                this.model.abreTela("FrmConsultaFormaPagamento");
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                this.getView().mostraMensagem("Não foi possível abrir a tela de consulta de forma de pagamento. Mensagem retornada: " + ex.getMessage());
            }
        }

        if (evento.equals("FormShow")) {
            this.getView().botoes('N');
        }
    }

}
