package Controllers;

import Models.Configuracao;
import Models.VendaFormaPagamento;
import Views.FrmConcluir;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBException;

/**
 *
 * @author Gabriel Nogueira Bezerra
 */
public class FrmConcluirController extends Controller {

    public FrmConcluirController(FrmConcluir view, Configuracao model) {
        if (view != null && model != null) {
            this.view = view;
            this.model = model;
            this.model.incluir(this);
        }
    }

    public void evento(String evt, KeyEvent evento, JTextField edt) {

        if (evt.equals("EdtDouble")) {
            String caracteres = ".0987654321";
            if (!caracteres.contains(evento.getKeyChar() + "")) {
                evento.consume();
                return;
            }

            if (edt.getText().contains(".")) {
                String decimais[] = edt.getText().split(".");
                if (decimais.length > 1) {
                    if (decimais[1].length() > 1) {
                        evento.consume();
                    }
                }
            }
        }

        if (evt.equals("EdtInteger")) {
            String caracteres = "0987654321";
            if (!caracteres.contains(evento.getKeyChar() + "")) {
                evento.consume();
            }
        }
    }

    @Override
    public void evento(String evento) {
        if (evento.equals("FormShow")) {
            if (this.model.getMesaSelecionada() != null) {
                try {
                    this.model.criaNovaVenda();
                    this.getView().getEdtNomeCliente().requestFocus();
                } catch (Exception ex) {
                    this.getView().mostraMensagem("Não foi possível concluir a mesa. Mensagem retornada:" + ex.getMessage());
                }
            }
        }
        if (evento.equals("EdtDescontoLost")) {
            if (!this.getView().getEdtDesconto().getText().equals("")) {
                if (Double.parseDouble(this.getView().getEdtDesconto().getText()) > this.model.getMesaSelecionada().total()) {
                    this.getView().mostraMensagem("Desconto maior que o permitido.");
                    this.getView().getEdtDesconto().setText("0");
                }
            }
        }

        if (evento.equals("FormClose")) {
            this.model.excluir(this);
        }

        if (evento.equals("btnSair")) {
            if (this.model.getVendaCriada().total() == 0) {
                try {
                    this.model.gravaVenda(Double.parseDouble(this.getView().getEdtAcrescimo().getText()), Double.parseDouble(this.getView().getEdtDesconto().getText()), this.getView().getEdtNomeCliente().getText());
                    this.model.concluiMesaSelecionada();
                    this.model.imprimirVenda();
                } catch (Exception ex) {
                    this.getView().mostraMensagem("Não foi possível concluir a venda. Mensagem retornada:" + ex.getMessage());
                }
            } else {
                this.model.setVendaCriada(null);
            }

            this.getView().dispose();
        }

        if (evento.equals("btnGravar")) {
            if (this.getView().validaCampos()) {
                try {
                    this.model.adicionaFormaPagamentoVenda(Integer.parseInt(this.getView().getEdtCodigoFormaPagamento().getText()), Double.parseDouble(this.getView().getEdtValor().getText()));
                    this.getView().limpaCampos();
                } catch (ClassNotFoundException | SQLException ex) {
                    this.getView().mostraMensagem("Não foi possível adicionar forma de pagamento a mesa. Mensagem retornada:" + ex.getMessage());
                }
            }
        }

        if (evento.equals("btnExcluir")) {
            try {
                this.model.excluirFormaPagamentoVenda(this.getView().getTableFormasPagamento().getSelectedRow());
            } catch (Exception ex) {
                this.getView().mostraMensagem("Não foi possível adicionar forma de pagamento a mesa. Mensagem retornada:" + ex.getMessage());
            }

        }

        if (evento.equals("btnPesquisarFormaPagamento")) {
            try {
                this.model.abreTela("FrmConsultaFormaPagamento");
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                this.getView().mostraMensagem("Não foi possível abrir a tela de consulta de pagamento. Mensagem retornada: " + ex.getMessage());
            }
        }

        if (evento.equals("btnCadastrarFormaPagamento")) {
            try {
                this.model.abreTela("FrmCadastroFormaPagamento");
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException ex) {
                this.getView().mostraMensagem("Não foi possível abrir a tela de cadastro de forma de pagamento. Mensagem retornada: " + ex.getMessage());
            }
        }

        if (evento.equals("EdtCodigoFormaPagamento")) {
            if (!this.getView().getEdtCodigoFormaPagamento().getText().equals("")) {
                try {
                    this.model.setFormaPagamentoSelecionada(this.model.buscarFormaPagamento(Integer.parseInt(this.getView().getEdtCodigoFormaPagamento().getText())));
                } catch (ClassNotFoundException | SQLException ex) {
                    this.getView().mostraMensagem("Não foi possível buscar o garçom. Mensagem retornada: " + ex.getMessage());
                }
            }
        }

        this.model.avisarObservers();
    }

    @Override
    public void alterar() {

        if (!this.getView().getEdtDesconto().getText().equals("")) {
            this.model.getVendaCriada().setDesconto(Double.parseDouble(this.getView().getEdtDesconto().getText()));
        } else {
            this.model.getVendaCriada().setDesconto(0);
        }
        if (!this.getView().getEdtAcrescimo().getText().equals("")) {
            this.model.getVendaCriada().setAcrescimo(Double.parseDouble(this.getView().getEdtAcrescimo().getText()));
        } else {
            this.model.getVendaCriada().setAcrescimo(0);
        }

        this.getView().getEdtTotalVenda().setText(String.valueOf(this.model.getVendaCriada().total()));
        this.getView().getEdtValor().setText(String.valueOf(this.model.getVendaCriada().total()));

        ArrayList<VendaFormaPagamento> vendasFormasPagamento = this.model.getVendaCriada().getVendaFormasPagamento();
        this.getView().limpaTableFormasPagamento();
        if (vendasFormasPagamento != null) {

            for (VendaFormaPagamento vendaFormaPagamento : vendasFormasPagamento) {
                String[] novaLinha = {String.valueOf(vendaFormaPagamento.getId()), vendaFormaPagamento.getFormaPagamento().getDescricao(), String.valueOf(vendaFormaPagamento.getValor())};
                ((DefaultTableModel) this.getView().getTableFormasPagamento().getModel()).addRow(novaLinha);
            }
        }
        if (this.model.getFormaPagamentoSelecionada() != null) {
            this.getView().getEdtCodigoFormaPagamento().setText(String.valueOf(this.model.getFormaPagamentoSelecionada().getId()));
            this.getView().getEdtDescricaoFormaPagamento().setText(this.model.getFormaPagamentoSelecionada().getDescricao());

            this.model.setFormaPagamentoSelecionada(null);
        }

    }

    public FrmConcluir getView() {
        return (FrmConcluir) this.view;
    }

    public void evento(String evento, KeyEvent evt) {

        if (evento.equals("EdtDesconto")) {
            this.evento("EdtDouble", evt, this.getView().getEdtDesconto());
            this.getView().getEdtAcrescimo().setText("0");
            if (!this.getView().getEdtDesconto().getText().equals("")) {
                if (Double.parseDouble(this.getView().getEdtDesconto().getText()) > this.model.getMesaSelecionada().total()) {
                    this.getView().mostraMensagem("Desconto maior que o permitido.");
                    this.getView().getEdtDesconto().setText("0");

                }
            }
        }

        if (evento.equals("EdtAcrescimo")) {
            this.evento("EdtDouble", evt, this.getView().getEdtAcrescimo());
            this.getView().getEdtDesconto().setText("0");

        }
    }

}
